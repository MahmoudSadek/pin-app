
class SendAUnFollowRequestResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static SendAUnFollowRequestResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SendAUnFollowRequestResponse sendAUnFollowRequestResponseBean = SendAUnFollowRequestResponse();
    sendAUnFollowRequestResponseBean.status = map['status'];
    sendAUnFollowRequestResponseBean.msg = map['msg'];
    sendAUnFollowRequestResponseBean.data = DataBean.fromMap(map['data']);
    sendAUnFollowRequestResponseBean.errors = map['errors'];
    return sendAUnFollowRequestResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  dynamic removed;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.removed = map['removed'];
    return dataBean;
  }

  Map toJson() => {
    "removed": removed,
  };
}