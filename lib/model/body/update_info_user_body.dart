

class UpdateInfoUserBody {
  String notification;
  String eventsPublic;
  String friendsPublic;

  static UpdateInfoUserBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpdateInfoUserBody updateInfoUserBodyBean = UpdateInfoUserBody();
    updateInfoUserBodyBean.notification = map['notification'];
    updateInfoUserBodyBean.eventsPublic = map['events_public'];
    updateInfoUserBodyBean.friendsPublic = map['friends_public'];
    return updateInfoUserBodyBean;
  }

  Map toJson() => {
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
  };
}