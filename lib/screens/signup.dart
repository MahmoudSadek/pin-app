import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../language/translation_strings.dart';
import 'signin.dart';
import 'video.dart';

class Signup extends StatefulWidget {
  Signup({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          height: MediaQuery.of(context).size.height * 1,
          width: MediaQuery.of(context).size.width * 1,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("asset/start.png"), fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                //margin: EdgeInsets.only(left: 80, top:160),
                child: Image.asset(
                  "asset/logo_app.png",
                  scale: 3,
                ),
              ),
              Container(
                // margin: EdgeInsets.only(left:85),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
//                    Image.asset(
//                      "asset/pin_logo.png",
//                      scale: 5,
//                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .035,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * .01,
              ),
              _button(1),
              SizedBox(
                height: MediaQuery.of(context).size.height * .025,
              ),
              _button(2),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _button(int id) {
    return GestureDetector(
      onTap: () {
        id == 1
            ? Navigator.push(
                context, MaterialPageRoute(builder: (context) => Signin()))
            : Navigator.push(
                context, MaterialPageRoute(builder: (context) => Video()));
      },
      child: Container(
        // margin: EdgeInsets.only(left:80),

        decoration: id == 1
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(5), color: Colors.white)
            : BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.transparent,
                border: Border.all(color: Colors.white)),
        height: MediaQuery.of(context).size.height * .07,
        width: MediaQuery.of(context).size.height * .22,

        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              id == 1 ? Translations.of(context).SIGNIN : Translations.of(context).SIGNUP,
              style: TextStyle(
                  fontSize: 18, color: id == 1 ? Colors.black : Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
