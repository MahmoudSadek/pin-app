class SendMessageResponse {
  bool status;
  String msg;
  Data data;

  SendMessageResponse({this.status, this.msg, this.data});

  SendMessageResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Created created;

  Data({this.created});

  Data.fromJson(Map<String, dynamic> json) {
    created =
    json['created'] != null ? new Created.fromJson(json['created']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.created != null) {
      data['created'] = this.created.toJson();
    }
    return data;
  }
}

class Created {
  int conversationId;
  int senderId;
  String message;
  int isSeen;
  int deletedFromSender;
  int deletedFromReceiver;
  String updatedAt;
  String createdAt;
  int id;

  Created(
      {this.conversationId,
        this.senderId,
        this.message,
        this.isSeen,
        this.deletedFromSender,
        this.deletedFromReceiver,
        this.updatedAt,
        this.createdAt,
        this.id});

  Created.fromJson(Map<String, dynamic> json) {
    conversationId = json['conversation_id'];
    senderId = json['sender_id'];
    message = json['message'];
    isSeen = json['is_seen'];
    deletedFromSender = json['deleted_from_sender'];
    deletedFromReceiver = json['deleted_from_receiver'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['conversation_id'] = this.conversationId;
    data['sender_id'] = this.senderId;
    data['message'] = this.message;
    data['is_seen'] = this.isSeen;
    data['deleted_from_sender'] = this.deletedFromSender;
    data['deleted_from_receiver'] = this.deletedFromReceiver;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    return data;
  }
}
