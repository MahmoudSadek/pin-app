
class SendAUnFollowRequestBody {
  String followedId;

  static SendAUnFollowRequestBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SendAUnFollowRequestBody sendAUnFollowRequestBodyBean = SendAUnFollowRequestBody();
    sendAUnFollowRequestBodyBean.followedId = map['followed_id'];
    return sendAUnFollowRequestBodyBean;
  }

  Map toJson() => {
    "followed_id": followedId,
  };
}