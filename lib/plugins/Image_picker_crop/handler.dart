import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pin/plugins/Image_picker_crop/cropper.dart';
import 'package:pin/plugins/Image_picker_crop/image_picker.dart';
import 'package:pin/plugins/Image_picker_crop/picker_dialog.dart';

class ImagePickerHandler {
  ImagePickerDialog imagePicker;
  AnimationController _controller;
  ImagePickerListener _listener;
  bool _isCropRequired;

  ImagePickerHandler(this._listener, this._controller);

  openCamera() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    if (_isCropRequired) {
      cropImage(image);
    } else {
      _listener.userImage(image);
    }
  }

  openGallery() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (_isCropRequired) {
      cropImage(image);
    } else {
      _listener.userImage(image);
    }
  }

  void build(int bgColor, int labelColor, bool isCropRequired) {
    _isCropRequired = isCropRequired;
    imagePicker = new ImagePickerDialog(this, _controller, bgColor, labelColor);
    imagePicker.initState();
  }

  Future cropImage(File image) async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      ratioX: 1.0,
      ratioY: 1.0,
      maxWidth: 512,
      maxHeight: 512,
    );
    _listener.userImage(croppedFile);
  }

  showDialog(BuildContext context) {
    imagePicker.getImage(context);
  }
}

abstract class ImagePickerListener {
  userImage(File _image);
}
