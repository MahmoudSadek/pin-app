import 'dart:io';

class UpdateImageBody {
  File image;

  static UpdateImageBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpdateImageBody updateImageBodyBean = UpdateImageBody();
    updateImageBodyBean.image = map['image'];
    return updateImageBodyBean;
  }

  Map toJson() => {
    "image": image,
  };
}