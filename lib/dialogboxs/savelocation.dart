import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/API/user_add_social_api.dart';
import 'package:pin/screens/Dashboard.dart';
import 'package:pin/constents/context.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/screens/maps.dart';
import 'package:pin/model/body/user_add_social_body.dart';
import 'package:pin/model/response/user_add_social_response.dart';
import 'package:pin/screens/myinformation.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;

class Saveloc extends StatefulWidget {
  @override
  _SavelocState createState() => _SavelocState();
}

class _SavelocState extends State<Saveloc> {
  var facebookLogin = FacebookLogin();

  String facebookName;

  String facebookEmail;

  String facebookImageUrl;

  String facebookPhoneNumber;

  String facebookUserId;

  signUpWithFacebook() async {
    var facebookLoginResult =
    await facebookLogin.logInWithReadPermissions(['email']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        break;
      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.loggedIn:
        var graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult
                .accessToken.token}');

        var profile = json.decode(graphResponse.body);
        print(profile.toString());
        facebookName = profile["name"];
        facebookEmail = profile["email"];
        facebookImageUrl = profile["picture"]['data']['url'];
        facebookPhoneNumber = profile["phone"];
        facebookUserId = profile["id"];
        print(facebookImageUrl);
        break;
    }
  }

  _logout() async {
    await facebookLogin.logOut();
    print("Logged out");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 40,
                bottom: 30,
                left: 20,
                right: 20,
              ),
              margin: EdgeInsets.all(30),
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min, // To make the card compact
                children: <Widget>[
                  Container(
                    child: Text(
                      Translations.of(context).ConnectPintoyourFacebook,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18, fontFamily: 'quicksand'),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  GestureDetector(
                    onTap: () {
//                      Navigator.push(context,
//                          MaterialPageRoute(builder: (context) => Dashboard()));
                      signUpWithFacebook().whenComplete(() {
                        _sendUserSocialPress();
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * .12,
                          right: MediaQuery.of(context).size.width * .12,
                          top: 20),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * .06,
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: Color(0xff054084)),
                        child: Text(
    Translations.of(context).Connect,
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'quicksand',
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      _navPaymentSuccessful();
//                      Navigator.push(context,
//                          MaterialPageRoute(builder: (context) => Dashboard()));
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Material(
                          child: Text(
    Translations.of(context).NoThanks,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      )),
                    ),
                  )
                ],
              ),
            ),

            //...bottom card part,
            //...top circlular image part,
          ],
        ),
      ),
    );
  }

  Future _sendUserSocialPress() async {
    UserAddSocialBody body = UserAddSocialBody();
    body.providerId = facebookUserId;

    UserAddSocialResponse result;
    try {
      result = await sendUserAddSocialResponse(body);
//      await storeUser(result);
      _navPaymentSuccessful();
//      Navigator.push(context,MaterialPageRoute(builder: (context) => Dashboard()));


      Fluttertoast.showToast(
          msg: "done",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
  _navPaymentSuccessful(){
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (context) => Dashboard()
        ),
        ModalRoute.withName("/Dashboard")
    );
  }
}
