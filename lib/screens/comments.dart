import 'package:flutter/material.dart';

class Comments extends StatefulWidget {
  @override
  _CommentsState createState() => _CommentsState();
}

class _CommentsState extends State<Comments> {
  int reply;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: SingleChildScrollView(
        child: Container(
          color: Colors.transparent,
          child: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .27),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * .08,
                    )),
                    _comment(context, 1),
                    _comment(context, 2),
                    _comment(context, 3),
                    _comment(context, 4),
                    _comment(context, 5),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .15),
                height: 100,
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(bottom: 0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey, blurRadius: 3, spreadRadius: 2)
                      ]),
                  width: 70,
                  height: 70,
                  child: Image.asset(
                    "asset/heartlike.png",
                    scale: 3.5,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .38),
                alignment: Alignment.topCenter,
                child: Material(
                    color: Colors.transparent,
                    child: Text(
                      "Comments",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'quicksand'),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _comment(BuildContext context, int id) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * .02),
      width: MediaQuery.of(context).size.width,
      //     height: MediaQuery.of(context).size.height*.19,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.height * .15,
                    // height: MediaQuery.of(context).size.height * .15,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * .05),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.height * .06,
                          height: MediaQuery.of(context).size.height * .06,
                          margin: EdgeInsets.only(
                              bottom: MediaQuery.of(context).size.width * .09),
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                              color: Colors.grey, shape: BoxShape.circle),
                          child: CircleAvatar(
                            backgroundImage: ExactAssetImage(id == 1
                                ? "asset/logo.png"
                                : id == 4 ? "asset/logo.png" : "asset/logo.png"),
                            radius: MediaQuery.of(context).size.width * .06,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Material(
                            color: Colors.transparent,
                            child: Text(
                              "John Doe",
                              style: TextStyle(
                                  fontFamily: 'probapro',
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              color: Color(0xffF5F5F5),
                              width: MediaQuery.of(context).size.width * .72,
                              //height: 60,
                              margin: EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * .07,
                                  top: MediaQuery.of(context).size.height *
                                      .004),
                              child: Material(
                                color: Colors.transparent,
                                child: Text(
                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ",
                                  style: TextStyle(
                                      fontSize: 13, fontFamily: 'cavior'),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * .08,
                left: MediaQuery.of(context).size.width * .2),
            alignment: Alignment.centerRight,
            child: Material(
                color: Colors.transparent,
                child: reply == id
                    ? Container(
                        // height: MediaQuery.of(context).size.width * .12,
                        padding: EdgeInsets.only(left: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.grey),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 6,
                              child: TextField(
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Type a message.',
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    reply = 0;
                                  });
                                },
                                child: Image.asset(
                                  "asset/sent.png",
                                  color: Colors.blue,
                                  scale: 4,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                reply = id;
                              });
                            },
                            child: Container(
                              child: Text(
                                "Reply",
                                style:
                                    TextStyle(color: Colors.blue, fontSize: 14),
                              ),
                            ),
                          ),
                          Text(
                            "10-01-2020 11-00-AM",
                            style: TextStyle(color: Colors.grey, fontSize: 12),
                          ),
                        ],
                      )),
          ),
        ],
      ),
    );
  }
}
