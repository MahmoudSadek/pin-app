

class DenyAFriendRequestResponse {
  bool status;
  String msg;
  Dataset data;
  List<dynamic> errors;

  static DenyAFriendRequestResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DenyAFriendRequestResponse denyAFriendRequestResponseBean = DenyAFriendRequestResponse();
    denyAFriendRequestResponseBean.status = map['status'];
    denyAFriendRequestResponseBean.msg = map['msg'];
    denyAFriendRequestResponseBean.data = Dataset.fromMap(map['data']);
    denyAFriendRequestResponseBean.errors = map['errors'];
    return denyAFriendRequestResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class Dataset {
  int accepted;

  static Dataset fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Dataset dataBean = Dataset();
    dataBean.accepted = map['accepted'];
    return dataBean;
  }

  Map toJson() => {
    "accepted": accepted,
  };
}