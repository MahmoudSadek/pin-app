class constant {
  static const String del = 'DeletePin';
  static const String edit = 'EditPin';
  static const String report = 'ReportPin';
  static const String follow = 'Follow';

  static const List<String> choice = <String>[
    edit,
    del,
  ];
  static const List<String> choice2 = <String>[
    follow,
    report,
  ];
  static const int ch = 0;
}
