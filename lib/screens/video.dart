import 'package:flutter/material.dart';
import 'package:pin/screens/Dashboard.dart';
import 'package:pin/screens/signup.dart';
import 'package:pin/screens/signup_2.dart';

import '../language/translation_strings.dart';
import 'signin.dart';

class Video extends StatefulWidget {
  Video({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _VideoState createState() => _VideoState();
}

class _VideoState extends State<Video> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 5,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(horizontal: 14),
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        "asset/addback.png",
                        scale: 4,
                        color: Color(0xff949494),
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * .3,
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(70.0)),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 15, left: 15, right: 15),
                    height: MediaQuery.of(context).size.height * .75,
                    width: MediaQuery.of(context).size.width,

                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                            image: AssetImage("asset/video.png"),
                            fit: BoxFit.cover)),
                    // width: MediaQuery.of(context).size.width
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      _play(1),
                      _play(2),
                      _play(3),
                    ],
                  ),
                ],
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Signup2()));
                },
                child: Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * .03,
                      left: MediaQuery.of(context).size.width * .65),
                  height: MediaQuery.of(context).size.height * .05,
                  width: MediaQuery.of(context).size.width * .25,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.2), blurRadius: 3)
                    ],
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
    Translations.of(context).SKIP,
                        style: TextStyle(color: Color(0xff404040)),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Image.asset(
                        "asset/yellow_next.png",
                        scale: 5,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ));
  }

  Widget _play(int id) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * .6),
      alignment: Alignment.center,
      child: id == 1
          ? CircleAvatar(
              radius: 30,
              backgroundColor: Colors.white,
              child: Image.asset(
                "asset/playback.png",
                color: Colors.grey,
                scale: 4,
              ),
            )
          : id == 2
              ? CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.play_arrow,
                    size: 55,
                    color: Colors.grey,
                  ))
              : CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.play_arrow,
                    size: 45,
                    color: Colors.grey,
                  )),
    );
  }
}
