import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/body/report_event_body.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/utils/common.dart';
import 'package:progress_dialog/progress_dialog.dart';

class Spam extends StatefulWidget {
  DataEvent item;
  Spam(this.item);

  @override
  _SpamState createState() => _SpamState(item);
}

class _SpamState extends State<Spam> {
  var spam = [
    "Nudity",
    "Violance",
    "Harassment",
    "Sucide Or Self-Injury",
    "Unauthorised Sales",
    "Hate Speeech",
    "Hate Speeech",
    "Terrorism",
  ];
  var problem = [
    "Adult Nudity",
    "Sexual Activity",
    "Sexual Services ",
    "Sexually Exploitation",
    "Sexually Suggestive",
    "Involve A Child",
    "Sharing Private Images"
  ];
  var selected2;
  var selected;
  bool search = false;
  bool falseloc = false;
  DataEvent item;
  String reason;
  TextEditingController searchControllerm= TextEditingController();
  _SpamState(this.item);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Container(
            color: Colors.transparent,
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * .25),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * .04,
                          right: MediaQuery.of(context).size.width * .04,
                          top: MediaQuery.of(context).size.width * .17,
                        ),
                        width: MediaQuery.of(context).size.width,
                        // height: MediaQuery.of(context).size.height * .05,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .02,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    _spam(0),
                                    _spam(1),
                                    _spam(2)
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[_spam(3), _spam(4)],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    _spam(5),
                                    _spam(6),
                                    _spam(7)
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                              ],
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  _false(context, 1),
                                  _spam4(context, 1)
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              // margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*.09),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * .07,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  border: !search
                                      ? null
                                      : Border.all(
                                          width: 1,
                                          color: Color(0xffE3E3E3),
                                        )),
                              child: Container(
                                margin: EdgeInsets.only(left: 30, right: 20),
                                alignment: Alignment.center,
                                color: Colors.transparent,
                                child: !search
                                    ? Container()
                                    : TextField(
                                  controller: searchControllerm,
                                        keyboardType: TextInputType.multiline,
                                        style: TextStyle(fontSize: 20),
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                        ),
                                      ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * .04,
                            right: MediaQuery.of(context).size.width * .04,
                            top: 20),
                        alignment: Alignment.centerLeft,
                        child: Material(
                            color: Colors.transparent,
                            child: Text(
                              "Help us Understand the problem",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'quicksand'),
                            )),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * .04,
                          right: MediaQuery.of(context).size.width * .04,
                          top: MediaQuery.of(context).size.height * .01,
                        ),
                        width: MediaQuery.of(context).size.width,
                        // height: MediaQuery.of(context).size.height * .05,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .02,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    _spamproblem(0),
                                    _spamproblem(1),
                                    _spamproblem(2)
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    _spamproblem(3),
                                    _spamproblem(4)
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    _spamproblem(5),
                                    _spamproblem(6)
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * .06,
                            right: MediaQuery.of(context).size.width * .06,
                            top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width * .4,
                              height: MediaQuery.of(context).size.height * .06,
                              decoration: BoxDecoration(
                                  color: Color(0xffE3E3E3),
                                  borderRadius: BorderRadius.circular(4)),
                              child: Material(
                                  color: Colors.transparent,
                                  child: Text(
                                    "Cancel",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'quicksand',
                                        color: Colors.black),
                                  )),
                            ),
                            GestureDetector(
                              onTap: () {
                                reportEvent();
                            },
                              child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width * .4,
                                height: MediaQuery.of(context).size.height * .06,
                                decoration: BoxDecoration(
                                    color: Color(0xff59A0CE),
                                    borderRadius: BorderRadius.circular(4)),
                                child: Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      "Report",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'quicksand',
                                          color: Colors.white),
                                    )),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * .13),
                  height: 100,
                  alignment: Alignment.center,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 3,
                              spreadRadius: 2)
                        ]),
                    width: MediaQuery.of(context).size.width * .7,
                    height: MediaQuery.of(context).size.width * .2,
                    child: Image.asset(
                      "asset/spam.png",
                      scale: 3.8,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * .38),
                  alignment: Alignment.topCenter,
                  child: Material(
                      color: Colors.transparent,
                      child: Text(
                        "Spam",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'quicksand'),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _spam(int id) {
    return GestureDetector(
      onTap: () {
        setState(() {
          search = false;
          selected = id;
          falseloc = false;
          selected2 = false;
          reason = spam[id];
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.width * 0.03,
            horizontal: MediaQuery.of(context).size.width * 0.05),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: selected == id ? Colors.blue : Colors.grey.withOpacity(0.3),
        ),
        child: Center(
            child: Material(
                color: Colors.transparent,
                child: Text(
                  spam[id],
                  style: TextStyle(
                      color: selected == id ? Colors.white : Colors.black),
                ))),
      ),
    );
  }

  _spamproblem(int id) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected = false;
          search = false;
          falseloc = false;

          selected2 = id;

          reason = problem[id];
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.width * 0.03,
            horizontal: MediaQuery.of(context).size.width * 0.03),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: selected2 == id ? Colors.blue : Colors.grey.withOpacity(0.3),
        ),
        child: Center(
            child: Material(
                color: Colors.transparent,
                child: Text(
                  problem[id],
                  style: TextStyle(
                      color: selected2 == id ? Colors.white : Colors.black),
                ))),
      ),
    );
  }

  Widget _spam4(BuildContext context, int id) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected = null;
          selected2 = null;
          falseloc = false;
          search == false ? search = true : search = false;

          reason = searchControllerm.text;
        });
      },
      child: Container(
        margin: EdgeInsets.only(
          top: 10,
        ),
        width: MediaQuery.of(context).size.width * .45,
        height: MediaQuery.of(context).size.width * .1,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: search ? Color(0xff1778F9) : Color(0xffE3E3E3)),
        child: Container(
          margin: EdgeInsets.only(left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                "asset/search.png",
                scale: 6,
                color: search ? Colors.white : Colors.black,
              ),
              SizedBox(
                width: 10,
              ),
              Material(
                  color: Colors.transparent,
                  child: Text(
                    "Something Else",
                    style: TextStyle(
                        color: search ? Colors.white : Colors.black,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'quicksand'),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Widget _false(BuildContext context, int id) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected = null;
          selected2 = null;
          falseloc = false;
          search = false;

          falseloc == false ? falseloc = true : falseloc = false;
          reason = "False Location";
        });
      },
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(
          top: 10,
        ),
        width: MediaQuery.of(context).size.width * .45,
        height: MediaQuery.of(context).size.width * .1,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: falseloc ? Colors.blue : Color(0xffE3E3E3)),
        child: Text(
          "False Location",
          style: TextStyle(color: falseloc ? Colors.white : Colors.black),
        ),
      ),
    );
  }

  Future<void> reportEvent() async {
    if (selected!=null) {
      reason = spam[selected];
    }else  if (selected2!=null) {
      reason = problem[selected];
    }else  if (falseloc) {
      reason = "False Location";
    }else  if (search) {
      reason = searchControllerm.text;
    }else {
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: "Choose report type",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      ReportEventBody body = ReportEventBody();
      body.eventid = item.id.toString();
      body.reason = reason;
      var result = await reportEventResponse(body);
      pr.hide();
      Navigator.pop(context);
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
