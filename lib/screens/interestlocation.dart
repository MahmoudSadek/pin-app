import 'dart:async';

import 'package:async_loader/async_loader.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pin/screens/interests.dart';
import 'package:pin/states/app_state.dart';
import 'package:pin/utils/common.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:search_map_place/search_map_place.dart';

import '../API/get_user_interests_api.dart';
import '../API/set_user_interests_location_api.dart';
import '../dialogboxs/savelocation.dart';
import '../language/translation_strings.dart';
import '../model/body/set_user_interest_location_body.dart';
import '../model/response/get_user_interests_response.dart';
import '../model/response/set_user_interest_location_response.dart';
import 'Dashboard.dart';

class Interestloc extends StatefulWidget {
  @override
  _InterestlocState createState() => _InterestlocState();
}

final GlobalKey<AsyncLoaderState> _getUserInterestss =
    new GlobalKey<AsyncLoaderState>();

class _InterestlocState extends State<Interestloc> {
  int interestId;
  InterestsBean selectedInterest;
  List<InterestsBean> listInterestBean = [];

  static double _value = 50;
  var next = true;
  var selected = false;
  LatLng initGeolocation = AppState.initialPositions;

  Completer<GoogleMapController> _controller = Completer();
  static final CameraPosition lahore = CameraPosition(
    target: AppState.initialPositions,
    zoom: 4.4746,
  );
  Set<Circle> circles = Set.from([]);
  Place pp;
  List<Marker> mMarker = [];
  List<LocationBean> mMarkerData = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _getUserInterestss,
      initState: () async => await getGetUserInterestsResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    var size = MediaQuery.of(context).size;
    circles.clear();
    circles.add(Circle(
      circleId: CircleId("100"),
      center: initGeolocation,
      fillColor: Color.fromRGBO(171, 39, 133, 0.1),
      strokeColor: Color.fromRGBO(171, 39, 133, 0.5),
      radius: 100 + _value * 100,
    ));
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              height: 145,
              child: Container(
                margin: EdgeInsets.only(top: 10),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      // height: MediaQuery.of(context).size.height * 70,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      margin: EdgeInsets.only(top: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              Navigator.pop(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Interests()));
                            },
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.black,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              Translations.of(context).InterestsLocation,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'trebuchat'),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(width: 20,)
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 45, right: 45, top: 5),
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        Translations.of(context)
                            .Pleasechooseyourinterestslocation,
                        style: TextStyle(fontSize: 14, height: 1,fontFamily: 'trebuchat'),
                      ),
                    ),
                    _asyncLoader
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(150.0)),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              mapType: MapType.terrain,
              initialCameraPosition: lahore,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              markers: mMarker.toSet(),
              circles: circles,
            ),
            Positioned(
              top: 70,
              left: 30,
              right: 30,
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xff7B7B7B),
                          blurRadius: 2,
                        )
                      ]),
                  height: 40,
                  child: Row(
                    //  mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
//                      Image.asset(
//                        "asset/minus.png",
//                        scale: 3,
//                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * .7,
                        child: SliderTheme(
                          data: SliderThemeData(
                            activeTrackColor: Color(0xffFDAF17),
                            thumbColor: Colors.grey,
                            inactiveTrackColor: Color(0xffA5A5A5),
                            trackHeight: 3,
                            thumbShape:
                                RoundSliderThumbShape(enabledThumbRadius: 10),
                          ),
                          child: Slider(
                            max: 50,
                            min: 0,
                            value: _value,
                            onChanged: (val) {
                              setState(() {
//                                selected = false;
                                _value = val;
                              });
                            },
                          ),
                        ),
                      ),
                      Text(
                        "${_value.toInt()} Km",
                        style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).size.height * 0.018,
                            fontFamily: 'trebuchat'),
                      ),
//                      Image.asset(
//                        "asset/plus.png",
//                        scale: 3,
//                      ),
                    ],
                  )),
            ),
            Positioned(
                top: 5,
                left: MediaQuery.of(context).size.width * 0.05,
                child: SearchMapPlaceWidget(
                  apiKey: "AIzaSyDdKyw50BVZxuQnTLHEbMjJbI6Fc-crFn4",
                  language: Translations.of(context).LanguageCode,
                  onSelected: (place) async {
                    final geolocation = await place.geolocation;
                    final GoogleMapController controller =
                        await _controller.future;
                    pp = place;
                    initGeolocation = geolocation.coordinates;
                    setState(() {
                      next = true;
                      circles.clear();
                      circles.add(Circle(
                        circleId: CircleId(geolocation.toString()),
                        center: geolocation.coordinates,
                        fillColor: Color.fromRGBO(171, 39, 133, 0.1),
                        strokeColor: Color.fromRGBO(171, 39, 133, 0.5),
                        radius: 100 + _value * 100,
                      ));
                    });
                    controller.animateCamera(
                        CameraUpdate.newLatLng(geolocation.coordinates));
                    controller.animateCamera(
                        CameraUpdate.newLatLngBounds(geolocation.bounds, 0));
                  },
                )),
            mMarkerData.isNotEmpty
                ? Positioned(
                    top: 109,
                    left: -100,
                    child: Container(
                      color: Colors.transparent,
                      width: 220,
                      height: MediaQuery.of(context).size.height - 30,
                      child: setInterestsWidegtData(),
                    ),
                  )
                : Container(),
            Positioned(
              bottom: 10,
              left: 40,
              right: 40,
              child: Container(
                color: Colors.transparent,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .060,
                child: InkWell(
                  onTap: () {
//                    _sendInterestsLovationPress();
                    next ? addInterest() : _sendInterestsFinishPress();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Color(0xff086EBA),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xff000000).withOpacity(.42),
                            blurRadius: 2),
                      ],
                    ),
                    child: Text(
                      !next
                          ? Translations.of(context).Next
                          : Translations.of(context).Add,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'quicksand',
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  Future _sendInterestsLovationPress() async {
    if (interestId == null) {
      Fluttertoast.showToast(
          msg: Translations.of(context).youmustselectyourinteres,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    SetUserInterestLocationBody body = SetUserInterestLocationBody();
    body.locations = [];
    LocationBean locationsBean = new LocationBean();
    locationsBean.lat = initGeolocation.latitude.toDouble();
    locationsBean.lon = initGeolocation.latitude.toDouble();
    locationsBean.km = _value.toInt();
    body.locations.add(locationsBean);

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();

    SetUserInterestLocationResponse result;
    try {
      result = await sendSetUserInterestLocationResponse(body);
//      await storeUser(result);

      setState(() {
        next == false ? next = true : next = false;
        next == true
            ? Common.GOOGLE
                ? showDialog(
                    context: context,
                    builder: (BuildContext context) => Saveloc(),
                  )
                : _navPaymentSuccessful()
//            Navigator.push(context,
//            MaterialPageRoute(builder: (context) => Dashboard()))
            : print("add");
      });

      Fluttertoast.showToast(
          msg: Translations.of(context).Yourinterestsarelocated,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }

  Future _sendInterestsFinishPress() async {
    if (mMarkerData.isEmpty) {
      Fluttertoast.showToast(
          msg: Translations.of(context).youmustselectyourinteres,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    SetUserInterestLocationBody body = SetUserInterestLocationBody();
    body.locations = [];
    body.locations.addAll(mMarkerData);

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();

    SetUserInterestLocationResponse result;
    try {
      result = await sendSetUserInterestLocationResponse(body);
//      await storeUser(result);

      setState(() {
        Common.GOOGLE
            ? showDialog(
                context: context,
                builder: (BuildContext context) => Saveloc(),
              )
            : _navPaymentSuccessful();
      });

      Fluttertoast.showToast(
          msg: Translations.of(context).Yourinterestsarelocated,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }

  Widget getNoConnectionWidget() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          SizedBox(
//            height: 90.0,
//            child: new Container(
//              decoration: new BoxDecoration(
//                image: new DecorationImage(
//                  image: new AssetImage('assets/wifi.png'),
//                  fit: BoxFit.contain,
//                ),
//              ),
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.black),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _getUserInterestss.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtData(GetUserInterestsResponse data1) {
    listInterestBean.clear();
    listInterestBean.addAll(data1.data.interests);
    return Container(
      padding: EdgeInsets.only(left: 15),
      margin: EdgeInsets.only(left: 45, right: 45, top: 5),
      width: MediaQuery.of(context).size.width,
      height: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        border: Border.all(width: 1, color: Colors.grey),
      ),
      child: DropdownButton(
        value: selectedInterest,
        isExpanded: true,
        items: getDropDownIntersrsItems(),
        hint: new Text(
          Translations.of(context).SelectInterest,
          style: TextStyle(fontSize: 14, fontFamily: 'trebuchat'),
        ),
        onChanged: (InterestsBean newValueSelected) {
          setState(() {
            next = true;
            selectedInterest = newValueSelected;
          });
          interestId = selectedInterest.id;
        },
      ),
    );
  }

  List<DropdownMenuItem<InterestsBean>> getDropDownIntersrsItems() {
    List<DropdownMenuItem<InterestsBean>> items = new List();
    for (InterestsBean myFeeling in listInterestBean) {
      items.add(new DropdownMenuItem(
        value: myFeeling,
        child: Text(myFeeling.interest.title),
      ));
    }
    return items;
  }

  _navPaymentSuccessful() {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Dashboard()),
        ModalRoute.withName("/Dashboard"));
  }

  Future<void> addInterest() async {
    if (interestId == null) {
      Fluttertoast.showToast(
          msg: Translations.of(context).youmustselectyourinteres,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    for(int i=0;i<mMarkerData.length;i++){
      if(mMarkerData[i].locationName==pp.description ||
          (initGeolocation.latitude.toDouble() == mMarkerData[i].lat &&
              initGeolocation.longitude.toDouble() == mMarkerData[i].lon))
        if(selectedInterest.id==mMarkerData[i].interestid) {
          Fluttertoast.showToast(
              msg: Translations
                  .of(context)
                  .youmustselectyourinteres,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.blue,
              textColor: Colors.white,
              fontSize: 16.0);
          return;
        }
    }
    LocationBean locationsBean = new LocationBean();
    locationsBean.interestid = selectedInterest.id;
    locationsBean.interestName = selectedInterest.interest.image;
    locationsBean.lat = initGeolocation.latitude.toDouble();
    locationsBean.lon = initGeolocation.latitude.toDouble();
    locationsBean.km = _value.toInt();
    locationsBean.locationName = pp.description;
    locationsBean.marker = Marker(
      markerId: MarkerId((mMarker.length + 1).toString()),
      icon: BitmapDescriptor.defaultMarker,
      position: LatLng(initGeolocation.latitude.toDouble(),
          initGeolocation.longitude.toDouble()),
    );

    mMarkerData.add(locationsBean);

    setState(() {
      mMarker.add(locationsBean.marker);
      next = false;
    });
  }

  setInterestsWidegtData() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.only(left: 100, right: 100),
        child: Container(
          width: 100,
          height: MediaQuery.of(context).size.height - 30,
          child: GridView.builder(
              itemCount: mMarkerData.length,
              scrollDirection: Axis.vertical,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 1.3,
                  crossAxisSpacing: 25,
                  mainAxisSpacing: 25,
                  crossAxisCount: 1),
              itemBuilder: (BuildContext context, int index) {
                return _interest(mMarkerData[index]);
              }),
        ),
      ),
    );
  }

  Widget _interest(LocationBean item) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(right: 5, bottom: 5, top: 5, left: 5),
          height: 200,
          width: 200,
          decoration: new BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: new DecorationImage(
                  image: new NetworkImage(item.interestName),
                  fit: BoxFit.cover)),
          child: Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
//              color: Colors.black.withOpacity(0.4),
              borderRadius: BorderRadius.circular(10),
            ),
            padding: EdgeInsets.only(bottom: 10),
            child: Padding(
              padding: const EdgeInsets.only(top: 20, left: 8, right: 8),
              child: AutoSizeText(
                item.locationName,
                maxFontSize: 20,
                minFontSize: 8,
                maxLines: 4,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        Positioned(
          left: 0,
          top: 0,
          child: Container(
              height: 30,
              width: 30,
              child: InkWell(
                onTap: () {
                  setState(() {
                    mMarker.remove(item.marker);
                    mMarkerData.remove(item);
                  });
                },
                child: Icon(
                  Icons.clear,
                  color: Colors.white,
                ),
              )),
        ),
      ],
    );
  }
}
