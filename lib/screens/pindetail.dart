import 'package:async_loader/async_loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pin/model/response/event_details_response.dart';
import 'package:pin/screens/Dashboard.dart';
import 'package:pin/constents/menuitem.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/screens/view_map_mark.dart';
import 'package:pin/widgets/post_tile.dart';
import 'package:pin/widgets/post_tile2.dart';
import 'package:url_launcher/url_launcher.dart';

import 'comments.dart';
import 'customdialog.dart';
import '../dialogboxs/spam.dart';
import '../dialogboxs/viewpin.dart';
import '../language/translation_strings.dart';
import 'likes.dart';
import '../map/map_location.dart';
import 'timeline.dart';

class Pindetail extends StatefulWidget {
  String itemId;
  Pindetail(this.itemId);

  @override
  _PindetailState createState() => _PindetailState(itemId);
}

class _PindetailState extends State<Pindetail> {
  String itemId;
  _PindetailState(this.itemId);

  TextEditingController commentControllerList = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getEventDtailsResponse(itemId),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => getView(data),
    );
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .3,
                  ),
                  Text(
                    "Pin Details",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: _asyncLoader
    );
  }

  Widget _post(int id, DataEvent event ) {
    return PostTile2(event.id, event,commentControllerList, _asyncLoaderState,null);
  }



  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();

  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderState.currentState.reloadState())
        ],
      ),
    );
  }

  getView(EventDetailsResponse data) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            _post(1, data.data.event),
            Container(
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      // height: MediaQuery.of(context).size.height*.09,

                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.grey, blurRadius: 2)
                          ]),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Day",
                            style: TextStyle(
                                fontSize: 18,
                                fontFamily: 'quicksand',
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            children: <Widget>[
                              Image.asset(
                                "asset/date.png",
                                scale: 3,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                data.data.event.startDate,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'quicksand',
                                    color: Color(0xff858282)),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      //  height: MediaQuery.of(context).size.height*.09,

                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.grey, blurRadius: 2)
                          ]),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Time",
                            style: TextStyle(
                                fontSize: 18,
                                fontFamily: 'quicksand',
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            children: <Widget>[
                              Image.asset(
                                "asset/alaram.png",
                                scale: 3,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                data.data.event.startTime,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'quicksand',
                                    color: Color(0xff858282)),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(7),
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),

              width: MediaQuery.of(context).size.width,
              // height: MediaQuery.of(context).size.height*.09,

              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)]),

              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            "asset/pin.png",
                            scale: 3,
                          ),
                          Text(
                            data.data.event.address_name.contains("\n")?
                            data.data.event.address_name.replaceFirst("\n", " ,"):data.data.event.address_name,
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'quicksand',
                                color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(builder: (context) => ViewMapMark(LatLng(double.parse(data.data.event.lat),double.parse(data.data.event.lon) ))),
//                        );
                        openMap(double.parse(data.data.event.lat),double.parse(data.data.event.lon));
                      },
                      child: Container(
                        padding: EdgeInsets.all(5),
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * .2,
                        // height: MediaQuery.of(context).size.height*.03,
                        decoration: BoxDecoration(
                          color: Color(0xff2EC6FF),
                          borderRadius: BorderRadius.circular(2),
                        ),
                        child: Center(
                            child: Text(
                              "View Map",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'quicksand',
                                  color: Colors.white),
                            )),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * .04,
            )
          ],
        ),
      ),
    );
  }

  static Future<void> openMap(double latitude, double longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }
}
