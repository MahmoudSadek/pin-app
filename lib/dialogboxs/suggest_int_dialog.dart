import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pin/screens/Dashboard.dart';
import 'package:pin/constents/context.dart';
import 'package:pin/screens/interestlocation.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/screens/maps.dart';
import 'package:pin/screens/myinformation.dart';

class Suggest_intr_dialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: 40,
                  bottom: 30,
                  left: 20,
                  right: 20,
                ),
                margin: EdgeInsets.all(30),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 10.0,
                      offset: const Offset(0.0, 10.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min, // To make the card compact
                  children: <Widget>[
                    Container(
                      child: Text(
                    Translations.of(context).DONE,
                        style: TextStyle(
                            fontSize: 34,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'quicksand'),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Text(
                        Translations.of(context).Yoursuggestionswillbereviewedandapprovedifitmeetsourstandards,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, fontFamily: 'quicksand'),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      child: Text(
                          Translations.of(context).Thanks,
                        style: TextStyle(
                            fontSize: 34,
                            fontFamily: 'robo',
                            fontWeight: FontWeight.bold,
                            color: Color(0xffFDAF17)),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
//                        Navigator.push(
//                            context,
//                            MaterialPageRoute(
//                                builder: (context) => Interestloc()));
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * .12,
                            right: MediaQuery.of(context).size.width * .12,
                            top: 20),
                        width: MediaQuery.of(context).size.width * .3,
                        height: MediaQuery.of(context).size.height * .06,
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(color: Color(0xff086EBA)),
                              color: Colors.white),
                          child: Text(
                              Translations.of(context).Next,
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: 'quicksand',
                                color: Color(0xff086EBA)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),

              //...bottom card part,
              //...top circlular image part,
            ],
          ),
        ),
      ),
    );
  }
}
