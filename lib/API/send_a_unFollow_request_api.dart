

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/send_a_unFollow_request_body.dart';
import 'package:pin/model/response/send_a_unFollow_request_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';





Future<SendAUnFollowRequestResponse> sendSendAUnFollowRequestResponse (SendAUnFollowRequestBody sendAUnFollowRequestBody) async {

  var headers= await Common.getHeaders();

  var body = json.encode(sendAUnFollowRequestBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/follower/unfollow',body: body,headers: headers);

  if (response.statusCode == 200){
    return SendAUnFollowRequestResponse.fromMap(json.decode(response.body));
  } else {

    SendAUnFollowRequestResponse res = SendAUnFollowRequestResponse.fromMap(json.decode(response.body));
    throw Exception(res.msg);
  }
}




