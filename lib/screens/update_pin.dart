import 'dart:io';

import 'package:async_loader/async_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pin/API/all_interests_api.dart';
import 'package:pin/dialogboxs/addpinbox.dart';
import 'package:pin/model/body/update_pin_body.dart';
import 'package:pin/model/response/add_pin_response.dart';
import 'package:pin/model/response/all_interests_response.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/model/response/feelings_response.dart';
import 'package:pin/model/response/friends_response.dart';
import 'package:pin/repository/add_event_api.dart';
import 'package:pin/screens/selectfriend.dart';
import 'package:pin/screens/view_map.dart';
import 'package:pin/utils/common.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

import 'Dashboard.dart';
import '../dialogboxs/savedinfo.dart';
import '../language/translation_strings.dart';
import '../map/map.dart';
import '../model/body/add_pin_body.dart';
import '../plugins/Image_picker_crop/handler.dart';

class UpdatePin extends StatefulWidget {
  DataEvent item;
  UpdatePin(this.item);

  @override
  _UpdatePinState createState() => _UpdatePinState(item);
}

class _UpdatePinState extends State<UpdatePin>
    with TickerProviderStateMixin, ImagePickerListener {
  DateTime _date = new DateTime.now();
  TimeOfDay _time = new TimeOfDay.now();
  DataEvent item;
  _UpdatePinState(this.item);

  Future<Null> selecetDate(int type) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2020),
        lastDate: new DateTime(2025));

    if (picked != null ) {
      setState(() {
        _date = picked;

          if(type==0)
            _StartDate = '${_date.year}-'+(_date.month<10?"0":"")+'${_date.month}-'+(_date.day<10?"0":"")+'${_date.day}';
           else _EndDate = '${_date.year}-'+(_date.month<10?"0":"")+'${_date.month}-'+(_date.day<10?"0":"")+'${_date.day}';


      });
    }
  }

  Future<Null> selectTime(int type) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _time);
    if (picked != null) {
      setState(() {
        _time = picked;
        if(type==0)
          _StartTime =(_time.hour<10?"0":"")+'${_time.hour}:'+(_time.minute<10?"0":"")+'${_time.minute}';
        else _EndTime = (_time.hour<10?"0":"")+'${_time.hour}:'+(_time.minute<10?"0":"")+'${_time.minute}';


      });
    }
  }

  ////////////////////////////////// image crop and pick methods /////////////////////////////////
  String _StartDate ="",_EndDate ="",_StartTime ="",_EndTime ="";
  List<File> _image=[];
  List<File> _video=[];
  AnimationController _controller;
  ImagePickerHandler imagePicker;
  SharedPreferences pref ;
  String userImages = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserData();
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    imagePicker = new ImagePickerHandler(this, _controller);
    imagePicker.build(0xFFEE6969, 0xFFFFFFFF, false);

    setData();
  }

  List<Interest> selected = [];
  TextEditingController locationEditingController = TextEditingController();
  Feelings _currentFeeling;
  List<Feelings> feelingList = [];
  TextEditingController _ControlerPinName=new TextEditingController();
  TextEditingController _ControlerDetails=new TextEditingController();

  List<DropdownMenuItem<Feelings>> getDropDownFeelingItems() {
    List<DropdownMenuItem<Feelings>> items = new List();
    for (Feelings myFeeling in feelingList) {
      items.add(new DropdownMenuItem(value: myFeeling, child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Image.network(
          myFeeling.imageEn,
            fit: BoxFit.contain,
            scale: 4,
          ),
          SizedBox(
            width: 10,
          ),
          Text(myFeeling.titleEn)
        ],
      ),));
    }
    return items;
  }
  @override
  Widget build(BuildContext context) {

    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getFeelingsResponse(),
      renderLoad: () => Center(child: new CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setFeelingWidegtData(data),
    );
    var _asyncLoaderInterest = new AsyncLoader(
      key: _asyncLoaderInterestsState,
      initState: () async => await getAllInterestsResponse(),
      renderLoad: () => Center(child: new CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setInterestsWidegtData(data),
    );
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                //  height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(vertical: 14, horizontal: 14),
                //  margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * .3,
                    ),
                    Text(
                        Translations.of(context).AddPin,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'trebuchat'),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(60.0)),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 13,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(top: 3, left: 10, right: 10),
                    width: MediaQuery.of(context).size.width,
                    //height: MediaQuery.of(context).size.height * .2,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(7),
                        boxShadow: [
                          BoxShadow(color: Colors.grey, blurRadius: 2)
                        ]),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Row(
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * .3,
                                //height:MediaQuery.of(context).size.height * .02 ,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image.asset(
                                      "asset/pen.png",
                                      scale: 2.5,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                        Translations.of(context).AddPin,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'quicksand'),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height:
                                      MediaQuery.of(context).size.height * .06,
                                  decoration: BoxDecoration(
                                      color: Color(0xffEBEBEB),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(5),
                                          bottomLeft: Radius.circular(5))),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(left: 10, top: 10),
                          width: MediaQuery.of(context).size.width,
                          //height: MediaQuery.of(context).size.height * .06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                backgroundImage:
                                NetworkImage(userImages),
                                backgroundColor: Colors.transparent,
                                radius: 15,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              //Image.asset("asset/women.png"),
                              Expanded(
                                child: TextField(
                                  keyboardType: TextInputType.multiline,
                                  controller: _ControlerPinName,
                                  maxLines: null,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: Translations.of(context).YourPinName,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .02,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 1,
                          decoration: BoxDecoration(color: Colors.grey),
                        ),
                        _asyncLoader

                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * .03,
                      top: MediaQuery.of(context).size.height * .01),
                  child: Text(Translations.of(context).Description,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: 'quicksand',
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .15,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                      boxShadow: [BoxShadow(color: Colors.grey)]),
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    child: TextField(
                      keyboardType: TextInputType.multiline,
                      controller: _ControlerDetails,
                      maxLines: null,
                      style: TextStyle(fontSize: 16),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .03,
                            ),
                            child: Text(
                                Translations.of(context).StartDate,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'quicksand',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * .02,
                          ),
                          child: Text(
                              Translations.of(context).EndDate,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'quicksand',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10, top: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            selecetDate(0);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * .07,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(color: Colors.grey, blurRadius: 1)
                                ]),
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      _StartDate==""?Translations.of(context).StartDate:_StartDate,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'quicksand',
                                          color: Color(0xff858282)),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          .09,
                                    ),
                                    Image.asset(
                                      "asset/date.png",
                                      scale: 3,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * .04,
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            selecetDate(1);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * .07,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(color: Colors.grey, blurRadius: 1)
                                ]),
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      _EndDate==""?Translations.of(context).EndDate:_EndDate,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'quicksand',
                                          color: Color(0xff858282)),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          .09,
                                    ),
                                    Image.asset(
                                      "asset/date.png",
                                      scale: 3,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * .024,
                        top: MediaQuery.of(context).size.height * 0.01,
                        bottom: MediaQuery.of(context).size.height * 0.01),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                        Translations.of(context).StartTime,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'quicksand',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          child: Text(
                              Translations.of(context).EndTime,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'quicksand',
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    )),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          selectTime(0);
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width * .02,
                          ),
                          padding: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * .07,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.grey, blurRadius: 1)
                              ]),
                          alignment: Alignment.centerLeft,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    _StartTime==""?Translations.of(context).StartTime:_StartTime,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'quicksand',
                                        color: Color(0xff858282)),
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * .09,
                                  ),
                                  Image.asset(
                                    "asset/alaram.png",
                                    scale: 3,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          selectTime(1);
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width * .02,
                          ),
                          padding: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * .07,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.grey, blurRadius: 1)
                              ]),
                          alignment: Alignment.centerLeft,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    _EndTime==""?Translations.of(context).EndTime:_EndTime,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'quicksand',
                                        color: Color(0xff858282)),
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * .09,
                                  ),
                                  Image.asset(
                                    "asset/alaram.png",
                                    scale: 3,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * .03,
                      top: MediaQuery.of(context).size.height * .01),
                  child: Text( Translations.of(context).AddLocation,
                      style: TextStyle(

                          fontSize: 14,
                          fontFamily: 'quicksand',
                          fontWeight: FontWeight.bold)),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ViewMap(locationEditingController: locationEditingController)));
                  },
                  child: Container(
                    margin: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * .07,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                      boxShadow: [
                        BoxShadow(color: Colors.grey),
                      ],
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 4,
                          child: Container(
                            padding: EdgeInsets.all(4),
                            child: TextField(
                                controller: locationEditingController,
                                readOnly:true,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: Translations.of(context).AddLocation,
                                ),

                                style: TextStyle(

                                    fontSize: 14,
                                    fontFamily: 'quicksand',
                                    fontWeight: FontWeight.bold),
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => ViewMap(locationEditingController: locationEditingController)));
                              },),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Image.asset(
                            "asset/pin.png",
                            scale: 2,
                            color: Colors.grey,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * .03,
                      top: MediaQuery.of(context).size.height * .01),
                  child: Text(Translations.of(context).TagYourFriend,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: 'quicksand',
                          fontWeight: FontWeight.bold)),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Selectfriend()),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * .07,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                      boxShadow: [
                        BoxShadow(color: Colors.grey),
                      ],
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 4,
                          child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                                Translations.of(context).Selectyourfriend,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'quicksand',
                                  color: Colors.grey),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Image.asset(
                            "asset/leftarrow.png",
                            scale: 4,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * .03,
                      top: MediaQuery.of(context).size.height * .01),
                  child: Text(Translations.of(context).Categories,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: 'quicksand',
                          fontWeight: FontWeight.bold)),
                ),
                _asyncLoaderInterest
               ,
                GestureDetector(
                  child: Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * .03,
                        top: MediaQuery.of(context).size.height * .01),
                    child: Text(Translations.of(context).UploadPicture,
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'quicksand',
                            fontWeight: FontWeight.bold)),
                  ),
                ),
                Container(
                    margin: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * .2,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: [BoxShadow(color: Colors.grey)]),
                    child:
                    Column(
                      children: <Widget>[
                        setImagesWidegtData(),
                        Container(
                          margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .09,
                              right: MediaQuery.of(context).size.width * .09,
                              top: MediaQuery.of(context).size.height * .02),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * .07,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(4),
                            boxShadow: [
                              BoxShadow(color: Colors.grey),
                            ],
                          ),
                          child: GestureDetector(
                            onTap: () {
                              imagePicker.showDialog(context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  "asset/uploadimg.png",
                                  scale: 4,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(
                                    Translations.of(context).UploadPicture,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'quicksand',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )),
                Container(
                    margin: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * .2,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: [BoxShadow(color: Colors.grey)]),
                    child:
                    Column(
                      children: <Widget>[
                        setVideossWidegtData(),
                        Container(
                          margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .09,
                              right: MediaQuery.of(context).size.width * .09,
                              top: MediaQuery.of(context).size.height * .02),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * .07,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(4),
                            boxShadow: [
                              BoxShadow(color: Colors.grey),
                            ],
                          ),
                          child: GestureDetector(
                            onTap: () {
                              _pickVideo();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  "asset/uploadimg.png",
                                  scale: 4,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(
                                    Translations.of(context).UploadVideo,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'quicksand',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          //   Fluttertoast.showToast(msg: "Pin added to the drafts",gravity: ToastGravity.CENTER);
                          publishClick(Common.draft);
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .05,
                              right: MediaQuery.of(context).size.width * .03,
                              top: MediaQuery.of(context).size.height * .02),
                          height: MediaQuery.of(context).size.height * .06,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Color(0xff59A0CE)),
                          child: Text(
                              Translations.of(context).Draft,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'quicksand',
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          /*showDialog(
                            context: context,
                            builder: (BuildContext context) => Addpinbox(),
                          );*/
                          publishClick(Common.publish);
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .03,
                              right: MediaQuery.of(context).size.width * .05,
                              top: MediaQuery.of(context).size.height * .02),
                          height: MediaQuery.of(context).size.height * .06,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Color(0xff59A0CE)),
                          child: Text(
                              Translations.of(context).Publish,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'quicksand',
                                color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          //   Fluttertoast.showToast(msg: "Pin added to the drafts",gravity: ToastGravity.CENTER);
                          publishClick(Common.scheduled);
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .05,
                              right: MediaQuery.of(context).size.width * .03,
                              top: MediaQuery.of(context).size.height * .02),
                          height: MediaQuery.of(context).size.height * .06,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Color(0xff59A0CE)),
                          child: Text(
                              Translations.of(context).Schedule,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'quicksand',
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          /*showDialog(
                            context: context,
                            builder: (BuildContext context) => Addpinbox(),
                          );*/
                          publishClick(Common.scheduled_publish);
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .03,
                              right: MediaQuery.of(context).size.width * .05,
                              top: MediaQuery.of(context).size.height * .02),
                          height: MediaQuery.of(context).size.height * .06,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Color(0xff59A0CE)),
                          child: Text(
                              Translations.of(context).SchedulePublish,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'quicksand',
                                color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .04,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }


  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> _asyncLoaderInterestsState =
  new GlobalKey<AsyncLoaderState>();

  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.white),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderState.currentState.reloadState())
        ],
      ),
    );
  }

  Widget _interest(Interest id) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected.contains(id) ? selected.remove(id) : selected.add(id);
        });
      },
      child: Container(
        margin: EdgeInsets.only(
          left: 10,
          top: 5,
        ),
        alignment: Alignment.bottomCenter,
        width: MediaQuery.of(context).size.width * .16,
        height: MediaQuery.of(context).size.width * .05,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              width: 1,
              color: selected.contains(id) ? Color(0xff086EBA) : Colors.grey),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 30,
              height: 30,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: selected.contains(id) ? Color(0xff086EBA) : Colors.grey,
                shape: BoxShape.circle,
              ),
              child: Icon(
                Icons.done_all,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              id.title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            )
          ],
        ),
      ),
    );
  }

  @override
  userImage(File _Image) {
    // TODO: implement userImage
    setState(() {
      if(_Image!=null)
      this._image.add(_Image);

    });

  }
  // This funcion will helps you to pick a Video File
  _pickVideo() async {
    File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    setState(() {

      if(video!=null)
      _video.add( video);
    });

  }

  setFeelingWidegtData(FeelingsResponse data) {
    feelingList = data.data.feelings;
    _currentFeeling = feelingList.firstWhere((feel) =>feel.id==int.parse(item.feelingId));
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 5, left: 10),
      alignment: Alignment.centerLeft,
      width: MediaQuery.of(context).size.width * .35,
      height: MediaQuery.of(context).size.height * .035,
      decoration: BoxDecoration(
          color: Color(0xffEBEBEB),
          borderRadius: BorderRadius.circular(15)),
      child: DropdownButton(
        hint: Text(
            Translations.of(context).Feeling),
        value: _currentFeeling,
        items: getDropDownFeelingItems(),
        onChanged: (selected){
          setState(() {
            _currentFeeling = selected;
          });
        },
      ),
    );
  }

  setInterestsWidegtData(AllInterestsResponse data) {

    return  Container(
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.width * .222,
      child: GridView.builder(
          itemCount: data.data.interests.length,
          scrollDirection: Axis.horizontal,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1.0,
              crossAxisSpacing: 10,
              mainAxisSpacing: 12,
              crossAxisCount: 1),
          itemBuilder: (BuildContext context, int index) {
            return _interest(data.data.interests[index]);
          }),
    );
  }
  setImagesWidegtData() {
    return   Container(
        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
        alignment: Alignment.center,
        height: MediaQuery.of(context).size.width * .15,
    child:
    GridView.count(
      childAspectRatio: 1.0,
      crossAxisSpacing: 10,
      mainAxisSpacing: 12,
      crossAxisCount: 1,
      scrollDirection: Axis.horizontal,
      children: List.generate(_image.length, (index) {
        return Container(
              width: MediaQuery.of(context).size.width * .15,
              height:
              100,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      image:
                      ExactAssetImage(_image[index].path),fit: BoxFit.fill,)),
            );
      }),
    )
//    GridView.builder(
//          itemCount: _image.length,
//          scrollDirection: Axis.horizontal,
//          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//              childAspectRatio: 1.0,
//              crossAxisSpacing: 10,
//              mainAxisSpacing: 12,
//              crossAxisCount: 1),
//          itemBuilder: (BuildContext context, int index) {
//            return Container(
//              width: MediaQuery.of(context).size.width * .15,
//              height:
//              MediaQuery.of(context).size.height * .08,
//              decoration: BoxDecoration(
//                  borderRadius: BorderRadius.circular(5),
//                  image: DecorationImage(
//                      image:
//                      ExactAssetImage(_image[index].path),fit: BoxFit.cover,)),
//            );
//          },
//    )
    );
  }
  setVideossWidegtData() {

    return   Container(
        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
        alignment: Alignment.center,
        height: MediaQuery.of(context).size.width * .15,
    child:GridView.builder(
          itemCount: _video.length,
          scrollDirection: Axis.horizontal,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1.0,
              crossAxisSpacing: 10,
              mainAxisSpacing: 12,
              crossAxisCount: 1),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              width: MediaQuery.of(context).size.width * .15,
              height:
              MediaQuery.of(context).size.height * .08,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      image:
                      AssetImage("asset/backimage.png"),fit: BoxFit.cover,)),
            );
          },
    ));
  }

  Future<void> publishClick(String type) async {
    String error = "";
    if (_ControlerPinName.text.isEmpty) {
      error = Translations.of(context).YourPinName;
    }else if (_currentFeeling==null) {
      error = Translations.of(context).Feeling;
    }else if (_ControlerDetails.text.isEmpty) {
      error = Translations.of(context).Description;
    } else if (_StartDate.isEmpty) {
      error = Translations.of(context).StartDate;
    }  else if (_EndDate.isEmpty) {
      error = Translations.of(context).EndDate;
    }  else if (_StartTime.isEmpty) {
      error = Translations.of(context).StartTime;
    } else if (_EndTime.isEmpty) {
      error = Translations.of(context).EndTime;
    } else if (locationEditingController.text.isEmpty) {
      error = Translations.of(context).AddLocation;
    }else if (selected.isEmpty) {
      error = Translations.of(context).SelectInterest;
    }else if (_image.isEmpty) {
      error = Translations.of(context).UploadPicture;
    }

    if (error.isNotEmpty){
      Fluttertoast.showToast(
          msg:Translations.of(context).enter+" "+error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.amber,
          fontSize: 16.0);
      return;
    }

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    await pr.show();
    try {
      UpdatePinBody body = UpdatePinBody();
      body.method = "PUT";
      body.name = _ControlerPinName.text;
      body.description = _ControlerDetails.text;
      body.feelingId = _currentFeeling.id;
      body.startDate = _StartDate;
      body.endDate = _EndDate;
      body.startTime = _StartTime;
      body.endTime = _EndTime;
      body.lat = MapWidegt.currentLatLng.latitude.toString();
      body.lon = MapWidegt.currentLatLng.longitude.toString();
      body.type = type;
      body.categories = [];
      for(int i=0 ; i<selected.length;i++){
        body.categories.add( selected[i].id.toString());
      }
      body.friends = [];
      for(int i=0 ; i<Selectfriend.selected.length;i++){
        body.friends.add( Selectfriend.selected[i].id.toString());
      }
      body.images=[];
      body.videos=[];
      body.images.addAll(_image);
      body.videos.addAll(_video);
      AddPinResponse result = await sendUpdatePinRequest(body, item.id);

      pr.hide();
      showDialog(
        context: context,
        builder: (BuildContext context) => Addpinbox(),
      );

    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg:  Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  void setData() {
    _ControlerPinName.text = item.name;
     _ControlerDetails.text = item.description;
//     _currentFeeling.id;
     _StartDate= item.startDate;
    _EndDate = item.endDate;
    _StartTime = item.startTime;
    _EndTime = item.endTime;
    MapWidegt.currentLatLng = LatLng(double.parse(item.lat), double.parse(item.lon));
    locationEditingController.text="Selected";
   
  }
  Future<void> getUserData() async {
    pref = await SharedPreferences.getInstance();
    setState(() {

      userImages = pref.getString(Common.USER_IMAGE)??"";
    });
  }
}
