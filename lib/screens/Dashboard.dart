import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:pin/chat2/inbox.dart';
import 'package:pin/screens/friends.dart';
import 'package:pin/screens/maps.dart';
import 'package:pin/screens/myaccount.dart';
import 'package:pin/screens/timeline.dart';

import '../language/translation_strings.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final _pageController = PageController(initialPage: 2);
  int _page = 2;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Container(
          height: 75,
          decoration: BoxDecoration(
//              color: Colors.black.withOpacity(0.08),

              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 0.5,
                    offset: Offset(2, 0))
              ]),
          child: Stack(children: [
            CurvedNavigationBar(
              height: 50.0,
              animationCurve: Curves.easeOut,
              color: Colors.white70,
              index: _page,
              animationDuration: Duration(milliseconds: 700),
              backgroundColor: Colors.transparent,
              buttonBackgroundColor: Color(0xff59A0CE),
              items: <Widget>[
                Container(
                    width: 35,
                    height: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      "asset/timeline.png",
                      scale: 4.5,
                      color: _page != 0 ? Color(0xff707070) : null,
                    )),
                Container(
                    width: 35,
                    height: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      "asset/frinds.png",
                      scale: 4.5,
                      color: _page != 1 ? Color(0xff707070) : null,
                    )),
                Container(
                    width: 35,
                    height: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      "asset/map.png",
                      scale: 4.5,
                      color: _page != 2 ? Color(0xff707070) : null,
                    )),
                Container(
                    width: 35,
                    height: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      "asset/chats.png",
                      scale: 4.5,
                      color: _page != 3 ? Color(0xff707070) : null,
                    )),
                Container(
                    width: 35,
                    height: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      "asset/myaccount.png",
                      scale: 4.5,
                      color: _page != 4 ? Color(0xff707070) : null,
                    )),
              ],
              onTap: (index) {
                setState(() {
                  _page = index;
                  _pageController.jumpToPage(index);
                });
              },
            ),
            Container(
                padding: EdgeInsets.only(
                  top: 20,
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Center(
                            child: Text(
                      _page == 0 ? "" : Translations.of(context).Timeline,
                      style: TextStyle(fontSize: 12),
                    ))),
                    Expanded(
                        child: Center(
                            child: Text(
                      _page == 1 ? "" : Translations.of(context).Friends,
                      style: TextStyle(fontSize: 12),
                    ))),
                    Expanded(
                        child: Center(
                            child: Text(
                      _page == 2 ? "" : Translations.of(context).Maps,
                      style: TextStyle(fontSize: 12),
                    ))),
                    Expanded(
                        child: Center(
                            child: Text(
                      _page == 3 ? "" : Translations.of(context).Chats,
                      style: TextStyle(fontSize: 12),
                    ))),
                    Expanded(
                        child: Center(
                            child: Text(
                      _page == 4 ? "" : Translations.of(context).MyAccount,
                      style: TextStyle(fontSize: 12),
                    ))),
                  ],
                ))
          ]),
        ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Timeline(),
            Friends(),
            Maps(),
            Inbox(),
            Myaccout(),
          ],
//          onPageChanged: (index){
//            _pageController.jumpToPage(_page);
//          },
        ));
  }
}
