

import 'package:http/http.dart' as http ;
import 'package:pin/model/response/all_friends_of_friends_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';






Future<AllFriendsOfFriendsResponse> getAllFriendsOfFriendsResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/friends/paginate/getFriendsOfFriends',headers: headers);

  if (response.statusCode == 200){
    return AllFriendsOfFriendsResponse.fromJson(json.decode(response.body));
  } else {

    AllFriendsOfFriendsResponse res = AllFriendsOfFriendsResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}




