

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/user_add_social_body.dart';
import 'package:pin/model/response/user_add_social_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';









Future<UserAddSocialResponse> sendUserAddSocialResponse (UserAddSocialBody userAddSocialBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(userAddSocialBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/add/social/facebook',body: body,headers: headers);

  if (response.statusCode == 200){
    return UserAddSocialResponse.fromMap(json.decode(response.body));
  } else {

    UserAddSocialResponse res = UserAddSocialResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}



