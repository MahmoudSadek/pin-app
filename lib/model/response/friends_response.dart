class FriendsResponse {
  bool status;
  String msg;
  Data data;

  FriendsResponse({this.status, this.msg, this.data});

  FriendsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Friends> friends;

  Data({this.friends});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['friends'] != null) {
      friends = new List<Friends>();
      json['friends'].forEach((v) {
        friends.add(new Friends.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.friends != null) {
      data['friends'] = this.friends.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Friends {
  int id;
  String name;
  String email;
  String phone;
  String gender;
  String image;
  String cover;
  String aboutMe;
  String activities;
  String hobbies;
  String education;
  String work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  String deletedAt;
  String createdAt;
  String updatedAt;

  Friends(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.gender,
        this.image,
        this.cover,
        this.aboutMe,
        this.activities,
        this.hobbies,
        this.education,
        this.work,
        this.notification,
        this.eventsPublic,
        this.friendsPublic,
        this.gcm,
        this.deletedAt,
        this.createdAt,
        this.updatedAt});

  Friends.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    gender = json['gender'];
    image = json['image'];
    cover = json['cover'];
    aboutMe = json['about_me'];
    activities = json['activities'];
    hobbies = json['hobbies'];
    education = json['education'];
    work = json['work'];
    notification = json['notification'];
    eventsPublic = json['events_public'];
    friendsPublic = json['friends_public'];
    gcm = json['gcm'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['image'] = this.image;
    data['cover'] = this.cover;
    data['about_me'] = this.aboutMe;
    data['activities'] = this.activities;
    data['hobbies'] = this.hobbies;
    data['education'] = this.education;
    data['work'] = this.work;
    data['notification'] = this.notification;
    data['events_public'] = this.eventsPublic;
    data['friends_public'] = this.friendsPublic;
    data['gcm'] = this.gcm;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
