class FeelingsResponse {
  bool status;
  String msg;
  Data data;

  FeelingsResponse({this.status, this.msg, this.data});

  FeelingsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }

    return data;
  }
}

class Data {
  List<Feelings> feelings;

  Data({this.feelings});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['feelings'] != null) {
      feelings = new List<Feelings>();
      json['feelings'].forEach((v) {
        feelings.add(new Feelings.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.feelings != null) {
      data['feelings'] = this.feelings.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Feelings {
  int id;
  String createdAt;
  String updatedAt;
  String titleEn;
  String imageEn;

  Feelings(
      {this.id,
        this.createdAt,
        this.updatedAt,
        this.titleEn,
        this.imageEn,});

  Feelings.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    titleEn = json['title'];
    imageEn = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['title_en'] = this.titleEn;
    data['image_en'] = this.imageEn;
    return data;
  }
}
