

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/set_user_interests_body.dart';
import 'package:pin/model/response/set_user_interests_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';







Future<SetUserInterestsResponse> sendSetUserInterestsResponse (SetUserInterestsBody setUserInterestsBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(setUserInterestsBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/interests',body: body,headers: headers);

  if (response.statusCode == 200){
    return SetUserInterestsResponse.fromMap(json.decode(response.body));
  } else {

    SetUserInterestsResponse res = SetUserInterestsResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}




