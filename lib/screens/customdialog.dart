import 'package:flutter/material.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/utils/common.dart';
import 'package:share/share.dart';

import '../constents/context.dart';

class CustomDialog extends StatelessWidget {
  DataEvent item;
  CustomDialog(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Consts.padding),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: dialogContent(context),
      ),
    );
  }

  Widget dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: Consts.avatarRadius + Consts.padding,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          margin: EdgeInsets.only(top: Consts.avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              Text(
                "Share Link",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand',
                ),
              ),
              SizedBox(height: 16.0),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * .13,
                      height: MediaQuery.of(context).size.height * .06,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white),
                      child: Image.asset("asset/fb.png"),
                    ),
                    Container(
                      padding: EdgeInsets.all(7),
                      width: MediaQuery.of(context).size.width * .12,
                      height: MediaQuery.of(context).size.height * .06,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color(0xffF34A38),
                      ),
                      child: Image.asset("asset/google.png"),
                    ),
                    Container(
                      padding: EdgeInsets.all(7),
                      width: MediaQuery.of(context).size.width * .12,
                      height: MediaQuery.of(context).size.height * .06,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xffD32F2F)),
                      child: Image.asset("asset/pint.png"),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * .13,
                      height: MediaQuery.of(context).size.height * .06,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white),
                      child: Image.asset("asset/linkedin.png"),
                    )
                  ],
                ),
              ),
              SizedBox(height: 24.0),
              Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * .02,
                    right: MediaQuery.of(context).size.width * .02,
                    top: 20),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .06,
                child: InkWell(
                  onTap: () {
//                    Navigator.pop(context);
                    Share.share(Common.MAIN_API_URL+item.id.toString(), subject: 'PIN App');
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Color(0xff59A0CE)),
                    child: Text(
                      "Share",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'quicksand',
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 100,
          alignment: Alignment.center,
          child: Container(
            margin: EdgeInsets.only(top: 0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(color: Colors.grey, blurRadius: 3, spreadRadius: 2)
                ]),
            width: MediaQuery.of(context).size.width * .5,
            height: MediaQuery.of(context).size.height * .12,
            child: Image.asset(
              "asset/sharebox.png",
              scale: 4,
            ),
          ),
        ),
        //...bottom card part,
        //...top circlular image part,
      ],
    );
  }
}
