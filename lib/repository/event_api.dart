import 'package:http/http.dart' as http ;
import 'package:pin/model/body/add_comment_body.dart';
import 'package:pin/model/body/add_pin_body.dart';
import 'package:pin/model/body/report_event_body.dart';
import 'package:pin/model/body/update_pin_body.dart';
import 'package:pin/model/response/add_pin_response.dart';
import 'package:pin/model/response/error_response.dart';
import 'package:pin/model/response/event_details_response.dart';
import 'package:pin/model/response/events_maps_response.dart';
import 'package:pin/model/response/events_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/response/feelings_response.dart';
import 'package:pin/model/response/friends_response.dart';
import 'package:pin/model/response/interests_response.dart';
import 'package:pin/utils/common.dart';






Future<EventsResponse> getEventsResponse(String pagination  ) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(Common.MAIN_API_URL+"api/user/events?page=$pagination",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return EventsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}
Future<EventsMapsResponse> getMapEventsResponse(String lat, String long  ) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(Common.MAIN_API_URL+"api/user/map/events/$lat/$long/50",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return EventsMapsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}


Future<EventsResponse> getTimelineEventsResponse(String pagination, int filter , String searchText ) async {
  var headers= await Common.getHeaders();
  String filters = filter!=-1?"$filter":"";

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(Common.MAIN_API_URL+"api/user/timeline/$filters?page=$pagination&search=$searchText",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return EventsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}
Future<EventsResponse> getUserEventsResponse(String pagination , int userId ) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(Common.MAIN_API_URL+"api/user/events/$userId?page=$pagination",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return EventsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<EventDetailsResponse> getEventDtailsResponse(String id  ) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(Common.MAIN_API_URL+"api/events/$id",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return EventDetailsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}



Future<ErrorResponse> deleteEventResponse(String id , UpdatePinBody updatePinBody) async {
  var headers= await Common.getHeaders();

  var body = json.encode(updatePinBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+"api/events/$id",
      headers: headers,
      body: body
  );

  if (response.statusCode == 200) {
    return ErrorResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<EventsResponse> addCommentEventResponse(String id ,String type, AddCommnetBody addCommnetBody) async {
  var headers= await Common.getHeaders();

  var body = json.encode(addCommnetBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+"api/comments/$type/$id",
      headers: headers,
      body: body
  );

  if (response.statusCode == 200) {
    return EventsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<EventsResponse> reportEventResponse(ReportEventBody addCommnetBody) async {
  var headers= await Common.getHeaders();

  var body = json.encode(addCommnetBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+"api/user/event/report",
      headers: headers,
      body: body
  );

  if (response.statusCode == 200) {
    return EventsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<EventsResponse> addFavoriteEventResponse(String id , String type, String favorite ) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(addCommnetBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+"api/$favorite/$type/$id",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return EventsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}



Future<EventsResponse> addReplayEventResponse(String commentId , String eventId,String type, AddCommnetBody addCommnetBody) async {
  var headers= await Common.getHeaders();

  var body = json.encode(addCommnetBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+"api/comments/$type/$eventId/reply/$commentId",
      headers: headers,
      body: body
  );

  if (response.statusCode == 200) {
    return EventsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

