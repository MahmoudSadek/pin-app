

import 'package:http/http.dart' as http ;
import 'package:pin/model/response/all_followings_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';






Future<AllFollowingsResponse> getAllFollowingsResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/followers/followings',headers: headers);

  if (response.statusCode == 200){
    return AllFollowingsResponse.fromMap(json.decode(response.body));
  } else {

    AllFollowingsResponse res = AllFollowingsResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors[1]);
  }
}


