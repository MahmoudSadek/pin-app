
class SendAFollowRequestResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static SendAFollowRequestResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SendAFollowRequestResponse sendAFollowRequestResponseBean = SendAFollowRequestResponse();
    sendAFollowRequestResponseBean.status = map['status'];
    sendAFollowRequestResponseBean.msg = map['msg'];
    sendAFollowRequestResponseBean.data = DataBean.fromMap(map['data']);
    sendAFollowRequestResponseBean.errors = map['errors'];
    return sendAFollowRequestResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  CreatedBean created;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.created = CreatedBean.fromMap(map['created']);
    return dataBean;
  }

  Map toJson() => {
    "created": created,
  };
}

/// pending : true

class CreatedBean {
  bool pending;

  static CreatedBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CreatedBean createdBean = CreatedBean();
    createdBean.pending = map['pending'];
    return createdBean;
  }

  Map toJson() => {
    "pending": pending,
  };
}