import 'package:async_loader/async_loader.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:loadmore/loadmore.dart';
import 'package:pin/chat/perchat.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/response/chat_thread_response.dart';
import 'package:pin/repository/chat_api.dart';
import 'package:pin/utils/common.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Inbox extends StatefulWidget {
  @override
  _InboxState createState() => _InboxState();
}

class _InboxState extends State<Inbox> {
  var selected = false;
  bool blocked = false;
  bool isLoadingData = false;
  String title = "Filters";
  var filterText = ["All", "Unread", "Read"];
  List<int> starred = [];

  bool blockSelect = false;
  bool _unread = false;
  bool filter = false;
  bool unread = false;
  bool archived = false;
  bool all = true;
  String messegsType ="";
  bool blockFirebase = false;

  TextEditingController search_controller = TextEditingController();
  String  searchText ="";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {


    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        filter
                            ? "Filters"
                            : unread
                                ? "Unread"
                                : all
                                    ? "Chats"
                                    :   "Read" ,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'trebuchat'),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(child: _filter()),
                  )
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: SmartRefresher(
        enablePullDown: true,
        header: ClassicHeader(
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child:  SingleChildScrollView(
          child:  Container(
            child: filter == true
                ? _filters()
                : blocked == true
                    ? _blocked() : _chat(),
          ),
        ),
      ),
    );
  }

  _chat() {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getChatThreadsResponse(page.toString(),messegsType),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => getNoConnectionWidget(),
//      renderSuccess: ({data}) => getListView(data),
    );
    return Column(
      children: <Widget>[
        all == false
            ? Container(
                height: MediaQuery.of(context).size.height * .07,
              )
            : _searchBar(),
//        _asyncLoader,
        StreamBuilder<DocumentSnapshot>(
          stream: Firestore.instance.collection("users").document("${Common.USER_ID_}").snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return LinearProgressIndicator();
//            _asyncLoaderState.currentState.reloadState();
            if(!blockFirebase)

//            if(snapshot.data.data!=null)
              getData();
            return getListView();
          },
        )
//        _chatlist(),
      ],
    );
  }

  _searchBar() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      margin: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * .04,
          left: MediaQuery.of(context).size.height * .05,
          right: MediaQuery.of(context).size.height * .05,
          bottom: MediaQuery.of(context).size.height * .03),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 5)],
        borderRadius: BorderRadius.circular(4),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: TypeAheadField(
              textFieldConfiguration:
              TextFieldConfiguration(
                onSubmitted: (term) {

                  searchText = search_controller.text;
//                  _asyncLoaderState.currentState.reloadState();
//                  blocked = true;
//                  all = false;
//                  unread = false;
//                  archived = false;
                },
                controller: search_controller,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.search,
                //inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                decoration: new InputDecoration(
                  hintText: Translations.of(context)
                      .Searchhere,
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
//                                              print(search_controller.text);

                        searchText = search_controller.text;
//                        blocked = true;
//
//                         all = false;
//                        unread = false;
//                         archived = false;

//                        _asyncLoaderState.currentState.reloadState();
                      });
                    },

                    child:Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        child: Container(
                          padding: EdgeInsets.all(2),
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                              color: Color(0xffFDAF17), shape: BoxShape.circle),
                          child: Image.asset(
                            "asset/search.png",
                            scale: 5.5,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                  prefixText: ' ',
//                                        suffixStyle:
//                                        TextStyle(color: Colors.blue)
                ),
              ),

              suggestionsCallback: (pattern) async {
                List<Threads> suggistions = new List();
                if(itemss!=null)
                  itemss.forEach((row) =>
                  {
                    if (row.withUser.name.contains(pattern))
                      {suggistions.add(row)}
                  });
                return suggistions;
              },

              hideOnLoading: true,
              hideOnError: true,
              hideOnEmpty: true,
              getImmediateSuggestions: false,
              itemBuilder: (context,
                  suggestion) {
                return ListTile(
                  title: Text(suggestion.withUser.name),
                );
              },
              hideSuggestionsOnKeyboardHide: false,
              keepSuggestionsOnLoading: false,
              keepSuggestionsOnSuggestionSelected: false,
              onSuggestionSelected: ( Threads suggestion) async {
                searchText = suggestion.withUser.name;
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Perchat(suggestion)));
//                _asyncLoaderState.currentState.reloadState();
              },

            ),

          ),

        ],
      ),
    );
  }


  _blocked() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: GridView.count(
        crossAxisCount: 3,
        shrinkWrap: true,
        mainAxisSpacing: 0,
        crossAxisSpacing: 0,
        childAspectRatio: 0.8,
        // (MediaQuery.of(context).size.width*.3 / MediaQuery.of(context).size.height*.2),

        children: List.generate(12, (_) => _blockedPerson(_)),
      ),
    );
  }

  _blockedPerson(int id) {
    return InkWell(
      child: Stack(children: [
        Container(
          margin: EdgeInsets.all(5),
          //width: MediaQuery.of(context).size.width * .3,
          //height: MediaQuery.of(context).size.height * .2,
          padding: EdgeInsets.only(top: 15, bottom: 5, left: 15, right: 15),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(color: Colors.grey.withOpacity(0.5), blurRadius: 2)
            ],
            borderRadius: BorderRadius.circular(6),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CircleAvatar(
                radius: 35,
                backgroundColor: Colors.black,
                backgroundImage:
                    AssetImage(id.isEven ? "asset/logo.png" : "asset/logo.png"),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 6, top: 15),
                child: Text(
                  id.isEven ? "Willium" : "JOHN DOE",
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }



  _filter() {
    return GestureDetector(
      onTap: () {
        setState(() {
          filter = true;
        });
      },
      child: Container(
          margin: EdgeInsets.only(right: 15),
          height: 25,
          width: 25,
          child: Center(
            child: Image(
              image: AssetImage(
                "asset/side.png",
              ),
              color: Colors.black,
            ),
          )),
    );
  }

  _filters() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(children: List.generate(3, (_) => _buildFilter(_))),
    );
  }

  _buildFilter(int id) {
    return InkWell(
      onTap: () {
        setState(() {
          id == 0 ? all = true : all = false;
          id == 1 ? unread = true : unread = false;
          id == 2 ? archived = true : archived = false;
          id == 3 ? blocked = true : blocked = false;
          id == 0 ? messegsType="":
          id == 1 ? messegsType="unread":
          id == 2 ? messegsType="read":
          id == 3 ? messegsType="":messegsType="";
          filter = false;
        });
        getData();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        width: MediaQuery.of(context).size.width,
        child: Container(
          margin: EdgeInsets.only(left: 20),
          child: Text(
            "${filterText[id]}",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
                color: Color(0xff232D31)),
          ),
        ),
      ),
    );
  }

  Widget _chatThread(int index, Threads item, ) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
                context, MaterialPageRoute(builder: (context) => Perchat(item)));
      },
      child: Container(
        padding: EdgeInsets.symmetric(),
        margin: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * .01, left: 10, right: 10),
        width: MediaQuery.of(context).size.width,
        //    height: MediaQuery.of(context).size.height*.19,
        decoration: BoxDecoration(
          color: item.lastMessage.isSeen == "0"
              ? Color(0xffC4C4C4)
              : index == 4 ? Color(0xffC4C4C4) : Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              spreadRadius: 0.5,
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * .1, top: 5),
              alignment: Alignment.centerRight,
              child: Text(
                item.lastMessage.createdAt,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 10,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      width: MediaQuery.of(context).size.height * .15,
                      // height: MediaQuery.of(context).size.height * .15,
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * .05),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.height * .06,
                            height: MediaQuery.of(context).size.height * .06,
                            margin: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).size.width * .04),
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                color: Colors.grey, shape: BoxShape.circle),
                            child: CircleAvatar(
                              backgroundImage: NetworkImage(item.withUser.image),
                              radius: MediaQuery.of(context).size.width * .06,
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              item.withUser.name,
                              style: TextStyle(
                                  fontFamily: 'probapro',
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              item.lastMessage.isSeen == '0'
                                  ? Container(
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .004,
                                          right: 5),
                                      width: MediaQuery.of(context).size.width *
                                          .05,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              .022,
                                      decoration: BoxDecoration(
                                          color: Color(0xff2C4752),
                                          shape: BoxShape.circle),
                                      child: Text(
                                        "1",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                .015),
                                      ),
                                    )

                                      : Container(),
                              Container(
                                // color: Colors.blue,
                                width: MediaQuery.of(context).size.width * .56,
                                //height: 60,
                                margin: EdgeInsets.only(
                                    right:
                                        MediaQuery.of(context).size.width * .07,
                                    top: MediaQuery.of(context).size.height *
                                        .004),
                                child: Text(
                                  item.lastMessage.message,
                                  style: TextStyle(
                                      fontSize: 13, fontFamily: 'cavior'),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
//            Align(
//              alignment: Alignment.centerRight,
//              child: Container(
//                margin: EdgeInsets.only(
//                  right: 5,
//                  bottom: 2,
//                ),
//                child: InkWell(
//                  onTap: () {
//                    setState(() {
//                      starred.contains(index)
//                          ? starred.remove(index)
//                          : starred.add(index);
//                    });
//                  },
//                  child: Icon(
//                    starred.contains(index) ? Icons.star : Icons.star_border,
//                    size: 20,
//                    color:
//                        starred.contains(index) ? Colors.yellow : Colors.grey,
//                  ),
//                ),
//              ),
//            )
          ],
        ),
      ),
    );
  }


//swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();

  void _onRefresh() async {
    page = 20;
    // monitor network fetch
//    _asyncLoaderState.currentState.reloadState();
    await getData();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }  void _onLoading() async {
    // monitor network fetch
    await getData();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }
  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderState.currentState.reloadState())
        ],
      ),
    );
  }




  List<Threads> itemss = [];
  Widget getListView() {
//    if(page==20) {
//      itemss.clear();
//      itemss.addAll( data.data.threads);
//    }
    return  Stack(children:[itemss.length<=0?Container(
        child: Center(
          child: isLoadingData?Center(child: new CircularProgressIndicator()):Text(
            Translations.of(context).NoChatyet,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          ),
        )
    ):LoadMore(
      isFinish:  itemss.length < page,
      onLoadMore: _loadMore,
      child: new ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount:  itemss.length,
        controller: listScrollController,
        itemBuilder: (context, index) => _chatThread(index,itemss[index] ),
      ),
      whenEmptyLoad: false,
      delegate: DefaultLoadMoreDelegate(),
      textBuilder: DefaultLoadMoreTextBuilder.english,)
        ,Positioned(bottom: 0,
          left: 100,
          right: 100,
          child: isLoadingData ? Container(
//            width: 50, height: 40,
            child: Center(
              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),),),
            color: Colors.white.withOpacity(0.8),
          ):
          Container()),]);

  }
  int page = 20;
  final ScrollController listScrollController = new ScrollController();
  Future<bool> _loadMore() async {
    print("onLoadMore");
    page = page + 20;
    ChatThreadsResponse response =  await  getChatThreadsResponse(page.toString(),messegsType);
    itemss.clear();
    itemss.addAll(response.data.threads);
//    _onRefresh();
    print("load");
    setState(() {
//      itemss.addAll(response.data.events.data);
      print("data count = ${itemss.length}");
    });

    return true;
  }

  Future<void> getData()  async {
    blockFirebase = true;
    isLoadingData= true;
    print("onLoadMore");
    ChatThreadsResponse response =  await  getChatThreadsResponse(page.toString(),messegsType);
    print("blockFirebase = $blockFirebase");
    setState(() {
      itemss.clear();
      itemss.addAll(response.data.threads);
      print("data count = ${itemss.length}");
      isLoadingData= false;
    });
    await Future.delayed(Duration(seconds:  2));
    if(blockFirebase)
      blockFirebase = false;
  }


}
