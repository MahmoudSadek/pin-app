
class SetUserInterestsBody {
  List<int> interests;

  static SetUserInterestsBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SetUserInterestsBody setUserInterestsBodyBean = SetUserInterestsBody();
    setUserInterestsBodyBean.interests = List()..addAll(
      (map['interests'] as List ?? []).map((o) => int.tryParse(o.toString()))
    );
    return setUserInterestsBodyBean;
  }

  Map toJson() => {
    "interests": interests,
  };
}