
import 'package:http/http.dart' as http ;
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:http/http.dart';
import 'package:pin/model/body/update_cover_body.dart';
import 'package:pin/model/body/update_image_body.dart';
import 'package:pin/model/response/update_image_response.dart';
import 'package:pin/utils/common.dart';





Future<UpdateImageResponse> sendUpdateImageResponse(UpdateImageBody updateImageBody ) async {

//  var headers= await Common.getHeaders();
  var token = await Common.getTokengmail();

  var request = http.MultipartRequest("POST", Uri.parse(Common.MAIN_API_URL+'api/user/updateImage'));

  Map<String, String> params = {

  };

  request.headers['Authorization'] = 'Bearer '+token;
  List<MultipartFile> fileList = List();
  fileList.add(MultipartFile.fromBytes(
      'image', await updateImageBody.image.readAsBytes(),
      filename: "image.jpg"));

  if (params != null) request.fields.addAll(params);// add part params if not null
  if (fileList != null) request.files.addAll(fileList);//

  var response = await request.send();
  var responseData = await response.stream.toBytes();
  var responseString = String.fromCharCodes(responseData);
  print("responseBody " + responseString);


  if (response.statusCode == 200) {
    return UpdateImageResponse.fromJson(json.decode(responseString));
  } else {
    UpdateImageResponse res = UpdateImageResponse.fromJson(json.decode(responseString));
    throw Exception(res.msg[0]);
  }
}







Future<UpdateImageResponse> sendUpdateImageCover(UpdateCoverBody updateCoverBody ) async {

//  var headers= await Common.getHeaders();
  var token = await Common.getTokengmail();

  var request = http.MultipartRequest("POST", Uri.parse(Common.MAIN_API_URL+'api/user/updateCover'));
  Map<String, String> params = {

  };
  request.headers['Authorization'] = 'Bearer '+token;


  List<MultipartFile> fileList = List();
  fileList.add(MultipartFile.fromBytes(
      'cover', await updateCoverBody.cover.readAsBytes(),
      filename: "image.jpg"));

  if (params != null) request.fields.addAll(params);// add part params if not null
  if (fileList != null) request.files.addAll(fileList);//

  var response = await request.send();
  var responseData = await response.stream.toBytes();
  var responseString = String.fromCharCodes(responseData);

  print("responseBody " + responseString);


  if (response.statusCode == 200) {
    return UpdateImageResponse.fromJson(json.decode(responseString));
  } else {
    UpdateImageResponse res = UpdateImageResponse.fromJson(json.decode(responseString));
    throw Exception(res.msg[0]);
  }
}
//Future<UpdateImageResponse> sendUpdateImageCover(UpdateCoverBody body  ) async {
//  var headers= await Common.getHeaders();
//
//  var request = http.MultipartRequest('POST', Uri.parse(Common.MAIN_API_URL+"api/user/updateCover"));
//  request.fields['name'] = body.name;
//  request.fields['feeling_id'] = body.feelingId.toString();
//  request.fields['description'] = body.description;
//  request.fields['start_date'] = body.startDate;
//  request.fields['end_date'] = body.endDate;
//  request.fields['start_time'] = body.startTime;
//  request.fields['end_time'] = body.endTime;
//  request.fields['lat'] = body.lat;
//  request.fields['lon'] = body.lon;
//  request.fields['type'] = body.type;
//  for(int i=0 ; i<body.categories.length;i++){
//    request.fields['categories[$i]'] = body.categories[i];
//  }
//  for(int i=0 ; i<body.friends.length;i++){
//    request.fields['friends[$i]'] = body.friends[i];
//  }
//  for(int i=0 ; i<body.images.length;i++){
//    request.files.add(await http.MultipartFile.fromPath('files[]', body.images[i].path));
//  }
//  for(int i=0 ; i<body.videos.length;i++){
//    request.files.add(await http.MultipartFile.fromPath('files[]', body.videos[i].path));
//  }
//  request.headers['authorization'] = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzcyNmI2M2JlYjk0Yjk0OTdmMWU1MThiMmI3NjJjZWJlYWFkNjgyYzE5YWU0YzM2ZmM5M2VjZTBhYTFhZWJjNmZiYWM5MTEzMDI1NTNlOGYiLCJpYXQiOjE1ODk5NjExMzMsIm5iZiI6MTU4OTk2MTEzMywiZXhwIjoxNjIxNDk3MTMzLCJzdWIiOiI0NSIsInNjb3BlcyI6W119.iyJRqtAjOEJM4TBQqP8lvsrd3vzCJeABPvEhgSZyMlklC9qkKxzZfYlHFTQF3EYb7mcyXVR3rZt8GopmZznj7mx-gxySqyGyNqPDiWv6IrjM7WgYJoWyN94IWv41eEJVjEeXtZesgePkeGJVqGm_uPc8045A-6EKk6Vjp0bB3P8Xib-ZqOuw0m4megZx2hToSLrCrvnyupXglh-6s2ktaIxTjcr1FJcuKXsrWQsGjJOjlgEKxv9Mz12lTAC_M25uJ42Ixjbmlsa58yxWuhQMSJRERbrjsV7twPcskUc4FxZKT1BnzxBS7wqrJSoDjJb8nsYvJYeKmTGQAyPf3_-CASCSG2v0owBhvau0l_6fPehZNhWYCZSnmfJUFSonfntDp4VFoZ6SlZQoH3DV8-1lmPUAY0L_0f5GwKL_rfEQ57O2m1-iKYoxddoEH03yxS7GZQ6Cy28qD7uzY0TGKPmQVz1Gv_bQX130IS7hHK2dvDY-apfMYaXeqJvNLSBBhRY_qdrUcEbwNG6bWfQDuYy3ANt8V9yqfkfvc-oCM6jzX7vvn0pa4SOOF25XteRH-ib72RWc2-XUWvcaXKF9FogJRSLLP2z_xUiB2uQlW33_fZsoJzK8hnh7wpxwRb6V7jIFIWb2Oqzc03aL86jwqk2FtZ3MuvUlBuh2IWDNyoeg2tM';
//  var response = await request.send();
//
//
//
//  if (response.statusCode == 200) {
//    return AddPinResponse.fromJson(json.decode(response.toString()));
//  } else {
//    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
//  }
//}