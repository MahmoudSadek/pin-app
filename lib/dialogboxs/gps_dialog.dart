import 'dart:io';

import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/states/app_state.dart';
import 'package:pin/utils/common.dart';

class GPSEnableDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 40,
                bottom: 30,
                left: 20,
                right: 20,
              ),
              margin: EdgeInsets.all(30),
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min, // To make the card compact
                children: <Widget>[
                  Container(
                    height: 100,
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(top: 0),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xff00FF80),
                      ),
                      width: 80,
                      height: 80,
                      child: Image.asset(
                        "asset/pinlocation.png",
//                        scale: 8
                      ),
                    ),
                  ),
                  SizedBox(height: 14.0),
                  Container(
                    child: Text(
                      Translations.of(context).EnableGPS,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 24.0),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                            Common.CheckUser(context);
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.065,
                            decoration: BoxDecoration(
                                color: Color(0xffE1E1E1),
                                borderRadius: BorderRadius.circular(25)),
                            child: Text(
                              Translations.of(context).CANCEL,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'trebuchat',
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .03,
                        ),
                        InkWell(
                          onTap: () async {
                            AppState();
                            if (Platform.isAndroid) {
                              bool isLocationEnabled = await Geolocator()
                                  .isLocationServiceEnabled();
                              if (isLocationEnabled) {
                                Navigator.of(context).pop();
                                Common.CheckUser(context);
                              } else {
                                final AndroidIntent intent = new AndroidIntent(
                                  action:
                                  'android.settings.LOCATION_SOURCE_SETTINGS',
                                );
                                await intent.launch();
                              }
                            }else{
                              Navigator.of(context).pop();
                              Common.CheckUser(context);
                            }
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.065,
                            decoration: BoxDecoration(
                                color: Color(0xff59A0CE),
                                borderRadius: BorderRadius.circular(25)),
                            child: Text(
                              Translations.of(context).YES,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'trebuchat',
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            //...bottom card part,
            //...top circlular image part,
          ],
        ),
      ),
    );
  }
}
