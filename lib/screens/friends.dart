import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/API/all_friends_of_friends_api.dart';
import 'package:pin/API/send_a_follow_request_api.dart';
import 'package:pin/API/send_a_friend_request_api.dart';
import 'package:pin/API/send_a_unFollow_request_api.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/body/send_a_follow_request_body.dart';
import 'package:pin/model/body/send_a_friend_request_body.dart';
import 'package:pin/model/body/send_a_unFollow_request_body.dart';
import 'package:pin/model/response/all_friends_of_friends_response.dart';
import 'package:pin/model/response/send_a_follow_request_response.dart';
import 'package:pin/model/response/send_a_friend_request_response.dart';
import 'package:pin/model/response/send_a_unFollow_request_response.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Friends extends StatefulWidget {
  @override
  _FriendsState createState() => _FriendsState();
}

final GlobalKey<AsyncLoaderState> _asyncLoadergetFriendsOfFriends =
    new GlobalKey<AsyncLoaderState>();

class _FriendsState extends State<Friends> {
  @override
  Widget build(BuildContext context) {
    var _PendingFriendRequest = new AsyncLoader(
      key: _asyncLoadergetFriendsOfFriends,
      initState: () async => await getAllFriendsOfFriendsResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(horizontal: 14),
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      Translations.of(context).Suggestedfriends,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'trebuchat'),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(60.0)),
        body: SmartRefresher(
            enablePullDown: true,
            header: ClassicHeader(),
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: _PendingFriendRequest));
  }

  //swipe refresh
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    _asyncLoadergetFriendsOfFriends.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    _asyncLoadergetFriendsOfFriends.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  Widget _addfriend(Data model,index) {
    return
//      model.friends.data.elementAt(index).hasSentFriendRequestTo==true?Container():
      Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 15),
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 3,
            )
          ]),
      child: Row(
//
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Container(
              margin: EdgeInsets.only(left: 10, top: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage:
                        NetworkImage(model.friends.data.elementAt(index).image),
                    radius: 25,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 95,
                          child: Text(
                          model.friends.data.elementAt(index).name,
                            style: TextStyle(fontSize: 15),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
//                        Row(
//                          children: <Widget>[
//                            Image.asset(
//                              "asset/pin.png",
//                              scale: 3,
//                            ),
//                            SizedBox(
//                              width: 3,
//                            ),
//                            Text(model.friends.data.elementAt(0).phone,
////                              id == 1
////                                  ? "Cario"
////                                  : id == 2
////                                      ? "Benha"
////                                      : id == 3
////                                          ? "Naser City"
////                                          : id == 4
////                                              ? "Mansora"
////                                              : id == 5
////                                                  ? "Shebeen"
////                                                  : id == 6
////                                                      ? "Alex"
////                                                      : "pakistan",
//                              style: TextStyle(fontSize: 12),
//                            )
//                          ],
//                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .05, right: 5),
              padding: EdgeInsets.all(5),
              child: Row(
                children: <Widget>[
                  model.friends.data.elementAt(index).hasSentFriendRequestTo==true?Container():
                  Expanded(
                    flex: 5,
                    child: GestureDetector(
                      onTap: () {
                        _sendAAddFriendPress(
                            model.friends.data.elementAt(index).id);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .05,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xff0072BB),
                        ),
                        child: Text(
                            Translations.of(context).AddFriend,
                          style: TextStyle(fontSize: 13, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 7,
                  ),
                  model.friends.data.elementAt(index).hasRequestedToFollow==true ?
                  Expanded(
                    flex: 4,
                    child: GestureDetector(
                      onTap: () {
                        _sendAUnFollowRequestPress( model.friends.data.elementAt(index).id);

                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .05,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffFDAF17),
                        ),
                        child: Text(
                          Translations.of(context).Following,
                          style: TextStyle(fontSize: 13, color: Colors.white),
                        ),
                      ),
                    ),
                  )
                      :
                  Expanded(
                    flex: 4,
                    child: GestureDetector(
                      onTap: () {
                        _sendAFollowRequestPress(
                            model.friends.data.elementAt(index).id);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .05,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffFDAF17),
                        ),
                        child: Text(
                          Translations.of(context).Follow,
                          style: TextStyle(fontSize: 13, color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getNoConnectionWidget() {
    return Container(
      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/images/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () =>
                  _asyncLoadergetFriendsOfFriends.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtData(AllFriendsOfFriendsResponse data) {
    return data.data.friends.data.length <= 0
        ? Center(
            child: Container(
                height: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    Translations.of(context).Therearenofriendshipsuggestionsyet,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                )),
          )
        : ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: data.data.friends.data.length,
            itemBuilder: (context, index) {
              return _addfriend(data.data,index);
            });
  }

  Future _sendAFollowRequestPress(int id) async {
    SendAFollowRequestBody body = SendAFollowRequestBody();
    body.followedId = id.toString();

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    SendAFollowRequestResponse result;
    try {
      result = await sendSendAFollowRequestResponse(body);

      Navigator.pop(context);
      _asyncLoadergetFriendsOfFriends.currentState.reloadState();

      Fluttertoast.showToast(
          msg: Translations.of(context).Requestsent,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }

  Future _sendAAddFriendPress(int id) async {
    SendAFriendRequestBody body = SendAFriendRequestBody();
    body.friendId = id;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    SendAFriendRequestResponse result;
    try {
      result = await sendSendAFriendRequestResponse(body);

      Navigator.pop(context);
      _asyncLoadergetFriendsOfFriends.currentState.reloadState();


      Fluttertoast.showToast(
          msg: Translations.of(context).Requestsent,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }

  Future _sendAUnFollowRequestPress(int id) async {

    SendAUnFollowRequestBody body = SendAUnFollowRequestBody();
    body.followedId = id.toString() ;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
//    new Future.delayed(const Duration(seconds: 1), () => "1");
    SendAUnFollowRequestResponse result;
    try {
      result = await sendSendAUnFollowRequestResponse(body);
//      await storeUser(result);

      Navigator.pop(context);
      _asyncLoadergetFriendsOfFriends.currentState.reloadState();

      Fluttertoast.showToast(
          msg: Translations.of(context).Requestsent,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
}
