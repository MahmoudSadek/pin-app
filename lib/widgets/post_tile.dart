import 'package:async_loader/async_loader.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:pin/dialogboxs/spam.dart';
import 'package:pin/dialogboxs/viewpin.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/body/add_comment_body.dart';
import 'package:pin/model/body/update_pin_body.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/screens/update_pin.dart';
import 'package:pin/utils/common.dart';
import 'package:pin/constents/menuitem.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:video_player/video_player.dart';

import '../screens/comments_new.dart';
import '../screens/timeline.dart';
import '../screens/customdialog.dart';
import '../screens/likes_new.dart';
import '../screens/pindetail.dart';

class PostTile extends StatefulWidget {
  int id; DataEvent item;
  TextEditingController commentControllerList;
  GlobalKey<AsyncLoaderState> asyncLoaderState;
  restateInterface restat;
  PostTile(this.id, this.item , this.commentControllerList,  this.asyncLoaderState);
//  PostTile(this.id, this.item , this.commentControllerList,  this.asyncLoaderState, this.restat);

  @override
  _PostTileState createState() => _PostTileState(id, item,commentControllerList,asyncLoaderState);
}

class _PostTileState extends State<PostTile> {
  int id; DataEvent item;
  TextEditingController commentControllerList;
  GlobalKey<AsyncLoaderState> asyncLoaderState;
  bool isBlue = false;
  List<VideoPlayerController> _controllerList=[];

  _PostTileState(this.id, this.item ,this.commentControllerList ,this.asyncLoaderState);


  @override
  void initState() {
    super.initState();
    video();
  }
  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top: 10, left: 10, right: 10),
      width: MediaQuery.of(context).size.width,
      //height: MediaQuery.of(context).size.height * .56,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(7),
          boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)]),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
//                        CircleAvatar(
//                          backgroundImage: ExactAssetImage("asset/aqib.png"),
//                          backgroundColor: Colors.transparent,
//                          radius: 18,
//                        ),

              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(left: 10, top: 10),
                width: 60,
                //height: MediaQuery.of(context).size.height * .06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: NetworkImage(item.userBean.image),
                      backgroundColor: Colors.transparent,
                      radius: 22,
                    ),

                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                       item.userBean.name,
                      style: TextStyle(fontSize: 12),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      item.updatedAt,
                      style: TextStyle(fontSize: 10, color: Colors.grey),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.40,
              ),
              PopupMenuButton<String>(
                onSelected: item.isAuth ? choiceactionUser  : choiceaction,
                itemBuilder: (BuildContext contect) {
                  // id==1? ch =constant.choice:ch2 = constant.report;

                  return item.isAuth
                      ? constant.choice.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList()
                      : constant.choice2.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                },
              ),
            ],
          ),
          InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Pindetail(item.id.toString()),
                  ));
            },
            child: Container(
              margin: EdgeInsets.only(left: 10, right: 40),
              child: Text(
                item.description,
                style: TextStyle(fontSize: 12),
              ),
            ),
          ),
          item.files.length == 0
              ? Container()
              : Container(
            margin: EdgeInsets.only(top: 10),
            child: Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  Viewpin(item),
                            );
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 10, bottom: 0),
                            width: MediaQuery.of(context).size.width,
                            height: 150,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            item.files[0].name),
                                        fit: BoxFit.fill,
                                      ),
                                      borderRadius:
                                      BorderRadius.circular(10)),
                                  child: item.files[0]
                                      .mime.contains(
                                      "video")
                                      ?
                                  AspectRatio(
                                    aspectRatio: 2.0,
                                    child: Stack(
                                      alignment: Alignment.bottomCenter,
                                      children: <Widget>[
                                        VideoPlayer(_controllerList[0]),
                                        _PlayPauseOverlay(controller: _controllerList[0], item: item,),
                                        VideoProgressIndicator(_controllerList[0], allowScrubbing: true),
                                      ],
                                    ),
                                  )
                                      : Container(),
                                ),
                                Container(
                                  decoration: new BoxDecoration(
                                    borderRadius:
                                    new BorderRadius.circular(16.0),
                                    color: Colors.grey.withOpacity(.3),
                                  ),
                                  padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          item.name,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              item.address_name.contains("\n")?
                                        item.address_name.substring(0,item.address_name.indexOf("\n"))
                                              :"",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight:
                                                  FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Image.asset(
                                                  "asset/pinlocation.png",
                                                  scale: 2,
                                                ),
                                                Text(
                                                  item.address_name.contains("\n")?
                                                  item.address_name.substring(item.address_name.indexOf("\n")+1):
                                                  item.address_name,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      item.files.length == 1
                          ? Container()
                          : Expanded(
                        flex: 3,
                        child: Container(
                          margin:
                          EdgeInsets.only(right: 20, bottom: 0),
                          width: MediaQuery.of(context).size.width,
                          height: 150,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder:
                                          (BuildContext context) =>
                                          Viewpin(item),
                                    );
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                item.files[1].name),
                                            fit: BoxFit.fill)),
                                    child: item.files[1]
                                        .mime.contains(
                                        "video")
                                        ? AspectRatio(
                                      aspectRatio: 2.0,
                                      child: Stack(
                                        alignment: Alignment.bottomCenter,
                                        children: <Widget>[
                                          VideoPlayer(_controllerList[1]),
                                          _PlayPauseOverlay(controller: _controllerList[1], item: item,),
                                          VideoProgressIndicator(_controllerList[1], allowScrubbing: true),
                                        ],
                                      ),
                                    )
                                        : Container(),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              item.files.length == 2
                                  ? Container()
                                  : Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.circular(
                                        10),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder:
                                                      (context) =>
                                                      Pindetail(item.id.toString())));
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius
                                                  .circular(
                                                  10),
                                              image: DecorationImage(
                                                  image: NetworkImage(item
                                                      .files[
                                                  2]
                                                      .name),
                                                  fit: BoxFit
                                                      .fill)),
                                          child: item.files[2]
                                              .mime.contains(
                                              "video")
                                              ? AspectRatio(
                                            aspectRatio: 2.0,
                                            child: Stack(
                                              alignment: Alignment.bottomCenter,
                                              children: <Widget>[
                                                VideoPlayer(_controllerList[2]),
                                                _PlayPauseOverlay(controller: _controllerList[2], item: item,),
                                                VideoProgressIndicator(_controllerList[2], allowScrubbing: true),
                                              ],
                                            ),
                                          )
                                              : Container(),
                                        ),
                                      ),
                                      Center(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment
                                              .center,
                                          mainAxisAlignment:
                                          MainAxisAlignment
                                              .center,
                                          children: <Widget>[
                                            Text(
                                              item.files.length >
                                                  3
                                                  ? "+${item.files.length - 3}"
                                                  : "",
                                              style: TextStyle(
                                                  color: Colors
                                                      .white,
                                                  fontWeight:
                                                  FontWeight
                                                      .bold,
                                                  fontSize:
                                                  20),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ]),
          ),
          Container(
            margin:
            EdgeInsets.only(left: MediaQuery.of(context).size.width * .6),
            child: Row(
              children: <Widget>[
                _social(1, item),
                SizedBox(
                  width: 5,
                ),
                _social(2, item),
                SizedBox(
                  width: 5,
                ),
                _social(3, item),
              ],
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  child: Stack(
                    children: <Widget>[
                      item.favorites.length != 0
                          ? Container(
                        margin: EdgeInsets.only(left: 10),
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5,
                              )
                            ]),
                        child: Container(
                          padding: EdgeInsets.all(2),
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                item.favorites[0].user.image),
                            radius: 2,
                          ),
                        ),
                      )
                          : Container(),
                      item.favorites.length >= 2
                          ? Container(
                        margin: EdgeInsets.only(left: 25),
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5,
                              )
                            ]),
                        child: Container(
                          padding: EdgeInsets.all(2),
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                item.favorites[0].user.image),
                            radius: 2,
                          ),
                        ),
                      )
                          : Container(),
                      item.favorites.length >= 3
                          ? Container(
                        margin: EdgeInsets.only(left: 40),
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5,
                              )
                            ]),
                        child: Container(
                          padding: EdgeInsets.all(2),
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                item.favorites[0].user.image),
                            radius: 2,
                          ),
                        ),
                      )
                          : Container(),
                      item.favorites.length >= 4
                          ? Container(
                        margin: EdgeInsets.only(left: 55),
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5,
                              )
                            ]),
                        child: Container(
                          padding: EdgeInsets.all(2),
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                item.favorites[0].user.image),
                            radius: 2,
                          ),
                        ),
                      )
                          : Container(),
                    ],
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        item.favorites.length != 0
                            ? "${item.favorites[0].user.name}"
                            : "",
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                      Text(
                          item.favorites.length > 0
                              ? (item.favorites.length > 1
                              ? " ${item.favorites.length - 1} more"
                              : "") +
                              " like this"
                              : "",
                          style: TextStyle(
                            fontSize: 12,
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * .025,
                right: MediaQuery.of(context).size.width * .025,
                bottom: MediaQuery.of(context).size.height * .018),
            width: MediaQuery.of(context).size.width,
            //height: MediaQuery.of(context).size.height * .065,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    height: MediaQuery.of(context).size.width * .12,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1, color: Colors.grey),
                    ),
                    child: Image.asset(
                      "asset/mask.png",
                      scale: 3,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 6,
                  child: Container(
                    height: MediaQuery.of(context).size.width * .12,
                    padding: EdgeInsets.only(left: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1, color: Colors.grey),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 6,
                          child: TextField(
                            controller: commentControllerList,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: Translations.of(context).Typeacomment,
                            ),
                            onChanged: (val) {
                              setState(() {
                                commentControllerList.text.isNotEmpty
                                    ? isBlue = true
                                    : isBlue = false;
                              });
                            },
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {
                                commentEvent(item.id);
                              },
                              child: Image.asset(
                                "asset/sent.png",
                                scale: 4,
                                color: commentControllerList.text.isNotEmpty
                                    ? Colors.blue
                                    : null,
                              ),
                            ))
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _social(int id, DataEvent item) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: () {
            id == 1
                ? showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(item),
            )
                : id == 2
                ? showDialog(
                context: context,
                builder: (BuildContext context) =>
                    CommentsNew(item.comments, item.id, Common.EVENT))
                :favoriteEvent(item.id);
          },
          child: Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5,
                  )
                ]),
            child: Image.asset(
              id == 1
                  ? "asset/share.png"
                  : id == 2 ? "asset/comment.png" : "asset/heart.png",
              scale: 4,
              color: id==3&&item.isFavorited?Colors.red:Colors.grey,
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        InkWell(
          onTap: () {
            id == 1
                ? showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(item),
            )
                : id == 2
                ? showDialog(
                context: context,
                builder: (BuildContext context) =>
                    CommentsNew(item.comments, item.id, Common.EVENT))
                : showDialog(
                context: context,
                builder: (BuildContext context) =>
                    LikesNew(item.favorites));
          },
          child: Text(
            id == 3
                ? item.favorites.length.toString()
                : id == 2 ? item.comments.length.toString() : "",
            style: TextStyle(
                color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  void choiceaction(String choice) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Spam(item),
    );
  }
  void choiceactionUser(String choice) {
    if(choice== constant.del){
      deleteEvent(item.id);
    }
    else{
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => UpdatePin(item)));
    }
  }


  Future<void> commentEvent(int id) async {
    if (commentControllerList.text.isEmpty) {
      return;
    }
    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      AddCommnetBody body = AddCommnetBody();
      body.comment = commentControllerList.text;
      var result = await addCommentEventResponse(id.toString(),Common.EVENT, body);
      commentControllerList.text = "";
      pr.hide();
      asyncLoaderState.currentState.reloadState();
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
  Future<void> favoriteEvent(int id) async {

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      var result = await addFavoriteEventResponse(id.toString(),Common.EVENT, item.isFavorited?"unfavorite":"favorite");

      pr.hide();
      asyncLoaderState.currentState.reloadState();
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
  Future<void> deleteEvent(int id) async {

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      UpdatePinBody body = UpdatePinBody();
      body.method = "DELETE";
      var result = await deleteEventResponse(id.toString(), body);

      pr.hide();
      asyncLoaderState.currentState.reloadState();
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  @override
  void dispose() {
    for(int i=0;i<item.files.length;i++){
      if(item.files[i].mime.contains("video")) {
        _controllerList[i].dispose();
      }}
    super.dispose();
  }
  void video() {
    try {
      for (int i = 0; i < item.files.length; i++) {
        _controllerList.add(VideoPlayerController.network(item.files[1].name));
        if (item.files[i].mime.contains("video")) {
          _controllerList[i] = VideoPlayerController.network(item.files[i].name,
          );
          _controllerList[i].addListener(() {
            setState(() {});
          });
          _controllerList[i].setLooping(false);
          _controllerList[i].initialize().then((_) => setState(() {}));
          _controllerList[i].pause();
        }
      }
    }catch(e){

    }

  }

}


class _PlayPauseOverlay extends StatelessWidget {
  _PlayPauseOverlay({Key key, this.controller, this.item}) : super(key: key);

  final VideoPlayerController controller;
  DataEvent item;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
            color: Colors.black26,
            child: Center(
              child: Icon(
                Icons.play_arrow,
                color: Colors.white,
                size: 100.0,
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
//            controller.value.isPlaying ? controller.pause() : controller.play();
            showDialog(
              context: context,
              builder: (BuildContext context) =>
                  Viewpin(item),
            );
          },
        ),
      ],
    );
  }
}