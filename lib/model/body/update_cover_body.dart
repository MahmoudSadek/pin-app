import 'dart:io';

/// cover : "icone"

class UpdateCoverBody {
  File cover;

  static UpdateCoverBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpdateCoverBody updateCoverBodyBean = UpdateCoverBody();
    updateCoverBodyBean.cover = map['cover'];
    return updateCoverBodyBean;
  }

  Map toJson() => {
    "cover": cover,
  };
}