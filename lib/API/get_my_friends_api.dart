

import 'package:http/http.dart' as http ;
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/response/get_my_friends_response.dart';
import 'package:pin/utils/common.dart';






Future<GetMyFriendsResponse> getMyFriendsResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/friends/getFriends',headers: headers);

  if (response.statusCode == 200){
    return GetMyFriendsResponse.fromMap(json.decode(response.body));
  } else {

    GetMyFriendsResponse res = GetMyFriendsResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}


Future<GetMyFriendsResponse> getMyFriendsinfriendResponse (int id) async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/friends/getFriends/$id',headers: headers);

  if (response.statusCode == 200){
    return GetMyFriendsResponse.fromMap(json.decode(response.body));
  } else {

    GetMyFriendsResponse res = GetMyFriendsResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}
