
class UpdateInfoUserResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static UpdateInfoUserResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpdateInfoUserResponse updateInfoUserResponseBean = UpdateInfoUserResponse();
    updateInfoUserResponseBean.status = map['status'];
    updateInfoUserResponseBean.msg = map['msg'];
    updateInfoUserResponseBean.data = DataBean.fromMap(map['data']);
    updateInfoUserResponseBean.errors = map['errors'];
    return updateInfoUserResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}

class DataBean {
  bool updated;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.updated = map['updated'];
    return dataBean;
  }

  Map toJson() => {
    "updated": updated,
  };
}