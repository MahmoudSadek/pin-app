

class SendAFriendRequestResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static SendAFriendRequestResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SendAFriendRequestResponse sendAFriendRequestResponseBean = SendAFriendRequestResponse();
    sendAFriendRequestResponseBean.status = map['status'];
    sendAFriendRequestResponseBean.msg = map['msg'];
    sendAFriendRequestResponseBean.data = DataBean.fromMap(map['data']);
    sendAFriendRequestResponseBean.errors = map['errors'];
    return sendAFriendRequestResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}

class DataBean {
  CreatedBean created;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.created = CreatedBean.fromMap(map['created']);
    return dataBean;
  }

  Map toJson() => {
    "created": created,
  };
}


class CreatedBean {
  int recipientId;
  String recipientType;
  int status;
  int senderId;
  String senderType;
  String updatedAt;
  String createdAt;
  int id;

  static CreatedBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CreatedBean createdBean = CreatedBean();
    createdBean.recipientId = map['recipient_id'];
    createdBean.recipientType = map['recipient_type'];
    createdBean.status = map['status'];
    createdBean.senderId = map['sender_id'];
    createdBean.senderType = map['sender_type'];
    createdBean.updatedAt = map['updated_at'];
    createdBean.createdAt = map['created_at'];
    createdBean.id = map['id'];
    return createdBean;
  }

  Map toJson() => {
    "recipient_id": recipientId,
    "recipient_type": recipientType,
    "status": status,
    "sender_id": senderId,
    "sender_type": senderType,
    "updated_at": updatedAt,
    "created_at": createdAt,
    "id": id,
  };
}