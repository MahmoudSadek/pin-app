class AllInterestsResponse {
  bool status;
  String msg;
  InterestsBean data;

  AllInterestsResponse({this.status, this.msg, this.data});

  AllInterestsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new InterestsBean.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class InterestsBean {
  List<Interest> interests;

  InterestsBean({this.interests});

  InterestsBean.fromJson(Map<String, dynamic> json) {
    if (json['interests'] != null) {
      interests = new List<Interest>();
      json['interests'].forEach((v) {
        interests.add(new Interest.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.interests != null) {
      data['interests'] = this.interests.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Interest {
  int id;
  String createdAt;
  String updatedAt;
  String title;
  String image;

  Interest({this.id, this.createdAt, this.updatedAt, this.title, this.image});

  Interest.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    title = json['title'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['title'] = this.title;
    data['image'] = this.image;
    return data;
  }
}
