import 'package:flutter/material.dart';
import 'package:pin/screens/pindetail.dart';
import 'package:pin/screens/splach_page.dart';
import 'package:pin/utils/common.dart';
import 'package:provider/provider.dart';

import 'bloc.dart';

class PocWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DeepLinkBloc _bloc = Provider.of<DeepLinkBloc>(context);
    return StreamBuilder<String>(
      stream: _bloc.state,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return SplashPage();
        } else {
          String data = snapshot.data;
          String id = data.substring(data.lastIndexOf("/")+1);
          return Pindetail(id); /*Container(
              child: Center(
                  child: Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Text('Redirected: ${snapshot.data}',
                          style: Theme.of(context).textTheme.title))))*/;
        }
      },
    );
  }
}