import 'package:async_loader/async_loader.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:pin/screens/addpin.dart';
import 'package:pin/repository/add_event_api.dart';

import '../language/translation_strings.dart';
import '../model/response/friends_response.dart';

class Selectfriend extends StatefulWidget {
  static var selected= [];

  @override
  _SelectfriendState createState() => _SelectfriendState();
}

class _SelectfriendState extends State<Selectfriend> {


  @override
  Widget build(BuildContext context) {



    var _asyncLoaderFriends = new AsyncLoader(
      key: _asyncLoaderFriendsState,
      initState: () async => await getFriendsResponse(),
      renderLoad: () => Center(child: new CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setFriendsWidegtData(data),
    );


    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 6.5;
    final double itemWidth = size.width / 2;
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(horizontal: 14),
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Select Friends",
                      style: TextStyle(
                          fontFamily: 'trebuchat',
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(60.0)),
        body: _asyncLoaderFriends
      );
  }

  Widget _invite(Friends model) {
    return GestureDetector(
      onTap: () {
        setState(() {
          Selectfriend.selected.contains(model) ? Selectfriend.selected.remove(model) : Selectfriend.selected.add(model);
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: 5, right: 10, top: 15, bottom: 5),
        width: MediaQuery.of(context).size.width,
        height: 100,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 3,
              )
            ]),
        child: Column(
//
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .09,
                padding: EdgeInsets.all(5),
                margin: EdgeInsets.only(
                  left: 10,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: CircleAvatar(
                        backgroundImage:   NetworkImage(model.image),
                        radius: 20,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5,right: 5,
                      top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              child: AutoSizeText(
                                model.name,
                            maxLines: 2,
                            maxFontSize: 20,
//                            style: TextStyle(fontSize: 17),
                          )),
                          SizedBox(
                            height: 5,
                          ),
                         /* Row(
                            children: <Widget>[
                              Container(
                                  child: Image.asset(
                                "asset/pin.png",
                                scale: 3,
                              )),
                              SizedBox(
                                width: 3,
                              ),
                              Container(
                                  child: Text(
                                    "",
                                style: TextStyle(fontSize: 12),
                              ))
                            ],
                          )*/
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              width: MediaQuery.of(context).size.width,
              // height: MediaQuery.of(context).size.height*.04,
              child: Container(
                margin: EdgeInsets.only(right: 10, bottom: 10),
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                  border: Border.all(width: 1),
                  shape: BoxShape.circle,
                  color: Selectfriend.selected.contains(model) ? Colors.blue : Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  final GlobalKey<AsyncLoaderState> _asyncLoaderFriendsState =
  new GlobalKey<AsyncLoaderState>();

  Widget getNoConnectionWidget() {
    return Container(color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.white),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderFriendsState.currentState.reloadState())
        ],
      ),
    );
  }

  setFriendsWidegtData(FriendsResponse data) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(right: 10),
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 1.8,
                mainAxisSpacing: 0,
                crossAxisSpacing: 0,
                //childAspectRatio: MediaQuery.of(context).size.height / 4,
              ),
              itemBuilder: (context, index) => _invite(data.data.friends[index]),
              itemCount: data.data.friends.length,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.pop(
              context,
              MaterialPageRoute(builder: (context) => Addpin()),
            );
          },
          child: Container(
            height: MediaQuery.of(context).size.width * .12,
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * .09,
                right: MediaQuery.of(context).size.width * .09,
                top: MediaQuery.of(context).size.height * .02),
            width: MediaQuery.of(context).size.width,
            // height: MediaQuery.of(context).size.height*.07,

            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Color(0xff59A0CE)),
            child: Text(
              "Done",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand',
                  color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * .02,
        )
      ],
    );
  }
}
