import 'get_my_friend_nfo_response.dart';

class EventsResponse {
  bool status;
  String msg;
  Data data;

  EventsResponse({this.status, this.msg, this.data});

  EventsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Events events;

  Data({this.events});

  Data.fromJson(Map<String, dynamic> json) {
    events =
    json['events'] != null ? new Events.fromJson(json['events']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.events != null) {
      data['events'] = this.events.toJson();
    }
    return data;
  }
}

class Events {
  int currentPage;
  List<DataEvent> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  Events(
      {this.currentPage,
        this.data,
        this.firstPageUrl,
        this.from,
        this.lastPage,
        this.lastPageUrl,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  Events.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<DataEvent>();
      json['data'].forEach((v) {
        data.add(new DataEvent.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to']==null?0:json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class DataEvent {
  int id;
  String userId;
  String name;
  String feelingId;
  String description;
  String startDate;
  String endDate;
  String startTime;
  String endTime;
  String lat;
  String lon;
  String type;
  String createdAt;
  String updatedAt;
  String address_name;
  bool isFavorited;
  bool isAuth;
  UserBean userBean;
  List<Friends> friends;
  List<Files> files;
  List<Comment> comments;
  List<Favorites> favorites;
  Feeling feeling;
  List<Feeling> categories;

  DataEvent(
      {this.id,
        this.userId,
        this.name,
        this.feelingId,
        this.description,
        this.startDate,
        this.endDate,
        this.startTime,
        this.endTime,
        this.lat,
        this.lon,
        this.type,
        this.createdAt,
        this.updatedAt,
        this.address_name,
        this.isFavorited,
        this.isAuth,
        this.userBean,
        this.friends,
        this.files,
        this.comments,
        this.favorites,
        this.feeling,
        this.categories});

  DataEvent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    feelingId = json['feeling_id'];
    description = json['description'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    lat = json['lat'];
    lon = json['lon'];
    type = json['type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    address_name = json['address_name']==null?"":json['address_name'];
    isFavorited = json['isFavorited'];
    isAuth = json['isAuth'];
    userBean = UserBean.fromMap(json['user']);
    if (json['friends'] != null) {
      friends = new List<Friends>();
      json['friends'].forEach((v) {
        friends.add(new Friends.fromJson(v));
      });
    }
    if (json['files'] != null) {
      files = new List<Files>();
      json['files'].forEach((v) {
        files.add(new Files.fromJson(v));
      });
    }
    if (json['comments'] != null) {
      comments = new List<Comment>();
      json['comments'].forEach((v) {
        comments.add(new Comment.fromJson(v));
      });
    }
    if (json['favorites'] != null) {
      favorites = new List<Favorites>();
      json['favorites'].forEach((v) {
        favorites.add(new Favorites.fromJson(v));
      });
    }
    feeling =
    json['feeling'] != null ? new Feeling.fromJson(json['feeling']) : null;
    if (json['categories'] != null) {
      categories = new List<Feeling>();
      json['categories'].forEach((v) {
        categories.add(new Feeling.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['feeling_id'] = this.feelingId;
    data['description'] = this.description;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    data['type'] = this.type;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['address_name'] = this.address_name;
    data['isFavorited'] = this.isFavorited;
    data['isAuth'] = this.isAuth;
    data['user'] = this.userBean;
    if (this.friends != null) {
      data['friends'] = this.friends.map((v) => v.toJson()).toList();
    }
    if (this.files != null) {
      data['files'] = this.files.map((v) => v.toJson()).toList();
    }
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    if (this.favorites != null) {
      data['favorites'] = this.favorites.map((v) => v.toJson()).toList();
    }
    if (this.feeling != null) {
      data['feeling'] = this.feeling.toJson();
    }
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Friends {
  int id;
  String name;
  String email;
  String phone;
  String gender;
  String image;
  String cover;
  String aboutMe;
  String activities;
  String hobbies;
  String education;
  String work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  String deletedAt;
  String createdAt;
  String updatedAt;
  Pivot pivot;

  Friends(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.gender,
        this.image,
        this.cover,
        this.aboutMe,
        this.activities,
        this.hobbies,
        this.education,
        this.work,
        this.notification,
        this.eventsPublic,
        this.friendsPublic,
        this.gcm,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.pivot});

  Friends.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    gender = json['gender'];
    image = json['image'];
    cover = json['cover'];
    aboutMe = json['about_me'];
    activities = json['activities'];
    hobbies = json['hobbies'];
    education = json['education'];
    work = json['work'];
    notification = json['notification'];
    eventsPublic = json['events_public'];
    friendsPublic = json['friends_public'];
    gcm = json['gcm'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['image'] = this.image;
    data['cover'] = this.cover;
    data['about_me'] = this.aboutMe;
    data['activities'] = this.activities;
    data['hobbies'] = this.hobbies;
    data['education'] = this.education;
    data['work'] = this.work;
    data['notification'] = this.notification;
    data['events_public'] = this.eventsPublic;
    data['friends_public'] = this.friendsPublic;
    data['gcm'] = this.gcm;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class Pivot {
  String eventId;
  String userId;

  Pivot({this.eventId, this.userId});

  Pivot.fromJson(Map<String, dynamic> json) {
    eventId = json['event_id'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['event_id'] = this.eventId;
    data['user_id'] = this.userId;
    return data;
  }
}

class Files {
  int id;
  String filableId;
  String filableType;
  String name;
  String extension;
  String mime;
  String size;
  String deletedAt;
  String createdAt;
  String updatedAt;
  bool isFavorited;
  List<Comment> comments;
  List<Favorites> favorites;

  Files(
      {this.id,
        this.filableId,
        this.filableType,
        this.name,
        this.extension,
        this.mime,
        this.size,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.isFavorited,
        this.comments,
        this.favorites});

  Files.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    filableId = json['filable_id'];
    filableType = json['filable_type'];
    name = json['name'];
    extension = json['extension'];
    mime = json['mime'];
    size = json['size'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isFavorited = json['isFavorited'];
    if (json['comments'] != null) {
      comments = new List<Comment>();
      json['comments'].forEach((v) {
        comments.add(new Comment.fromJson(v));
      });
    }
    if (json['favorites'] != null) {
      favorites = new List<Favorites>();
      json['favorites'].forEach((v) {
        favorites.add(new Favorites.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['filable_id'] = this.filableId;
    data['filable_type'] = this.filableType;
    data['name'] = this.name;
    data['extension'] = this.extension;
    data['mime'] = this.mime;
    data['size'] = this.size;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isFavorited'] = this.isFavorited;
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    if (this.favorites != null) {
      data['favorites'] = this.favorites.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Comment {
  int id;
  String userId;
  String commentableType;
  String commentableId;
  String comment;
  String parentId;
  String deletedAt;
  String createdAt;
  String updatedAt;
  User user;
  List<Replys> replys;

  Comment(
      {this.id,
        this.userId,
        this.commentableType,
        this.commentableId,
        this.comment,
        this.parentId,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.user,
        this.replys});

  Comment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    commentableType = json['commentable_type'];
    commentableId = json['commentable_id'];
    comment = json['comment'];
    parentId = json['parent_id'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    if (json['replys'] != null) {
      replys = new List<Replys>();
      json['replys'].forEach((v) {
        replys.add(new Replys.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['commentable_type'] = this.commentableType;
    data['commentable_id'] = this.commentableId;
    data['comment'] = this.comment;
    data['parent_id'] = this.parentId;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.replys != null) {
      data['replys'] = this.replys.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class User {
  int id;
  String name;
  String email;
  String phone;
  String gender;
  String image;
  String cover;
  String aboutMe;
  String activities;
  String hobbies;
  String education;
  String work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  String deletedAt;
  String createdAt;
  String updatedAt;

  User(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.gender,
        this.image,
        this.cover,
        this.aboutMe,
        this.activities,
        this.hobbies,
        this.education,
        this.work,
        this.notification,
        this.eventsPublic,
        this.friendsPublic,
        this.gcm,
        this.deletedAt,
        this.createdAt,
        this.updatedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    gender = json['gender'];
    image = json['image'];
    cover = json['cover'];
    aboutMe = json['about_me'];
    activities = json['activities'];
    hobbies = json['hobbies'];
    education = json['education'];
    work = json['work'];
    notification = json['notification'];
    eventsPublic = json['events_public'];
    friendsPublic = json['friends_public'];
    gcm = json['gcm'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['image'] = this.image;
    data['cover'] = this.cover;
    data['about_me'] = this.aboutMe;
    data['activities'] = this.activities;
    data['hobbies'] = this.hobbies;
    data['education'] = this.education;
    data['work'] = this.work;
    data['notification'] = this.notification;
    data['events_public'] = this.eventsPublic;
    data['friends_public'] = this.friendsPublic;
    data['gcm'] = this.gcm;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Replys {
  int id;
  String userId;
  String commentableType;
  String commentableId;
  String comment;
  String parentId;
  String deletedAt;
  String createdAt;
  String updatedAt;
  User user;

  Replys(
      {this.id,
        this.userId,
        this.commentableType,
        this.commentableId,
        this.comment,
        this.parentId,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.user});

  Replys.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    commentableType = json['commentable_type'];
    commentableId = json['commentable_id'];
    comment = json['comment'];
    parentId = json['parent_id'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['commentable_type'] = this.commentableType;
    data['commentable_id'] = this.commentableId;
    data['comment'] = this.comment;
    data['parent_id'] = this.parentId;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class Favorites {
  String userId;
  String favoriteableType;
  String favoriteableId;
  String createdAt;
  String updatedAt;
  User user;

  Favorites(
      {this.userId,
        this.favoriteableType,
        this.favoriteableId,
        this.createdAt,
        this.updatedAt,
        this.user});

  Favorites.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    favoriteableType = json['favoriteable_type'];
    favoriteableId = json['favoriteable_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['favoriteable_type'] = this.favoriteableType;
    data['favoriteable_id'] = this.favoriteableId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}



class Feeling {
  int id;
  String createdAt;
  String updatedAt;
  String title;
  String image;

  Feeling({this.id, this.createdAt, this.updatedAt, this.title, this.image});

  Feeling.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    title = json['title'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['title'] = this.title;
    data['image'] = this.image;
    return data;
  }
}
