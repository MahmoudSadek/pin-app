
class UserSuggestIInterestResponse {
  bool status;
  String msg;
  List<dynamic> data;
  List<dynamic> errors;

  static UserSuggestIInterestResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserSuggestIInterestResponse userSuggestIInterestResponseBean = UserSuggestIInterestResponse();
    userSuggestIInterestResponseBean.status = map['status'];
    userSuggestIInterestResponseBean.msg = map['msg'];
    userSuggestIInterestResponseBean.data = map['data'];
    userSuggestIInterestResponseBean.errors = map['errors'];
    return userSuggestIInterestResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}