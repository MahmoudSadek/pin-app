import 'package:async_loader/async_loader.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loadmore/loadmore.dart';
import 'package:pin/API/get_user_info_api.dart';
import 'package:pin/dialogboxs/spam.dart';
import 'package:pin/model/body/add_comment_body.dart';
import 'package:pin/model/response/interests_response.dart';
import 'package:pin/repository/add_event_api.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/screens/addpin.dart';
import 'package:pin/utils/common.dart';
import 'package:pin/widgets/post_tile.dart';
import 'package:pin/widgets/post_tile2.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../language/translation_strings.dart';
import '../model/response/events_response.dart';

class Timeline extends StatefulWidget {
  @override
  _TimelineState createState() => _TimelineState();
}

class _TimelineState extends State<Timeline> implements restateInterface{
  List<Interests> listOfInterests = [];
  Interests _currentInterest = null;
  List<TextEditingController> commentControllerList = [];
  TextEditingController search_controller = TextEditingController();

  final ScrollController listScrollController = new ScrollController();
  bool isBlue = false;
  SharedPreferences pref;

  String userImage = "", searchText ="";

  List<DropdownMenuItem<Interests>> getDropDownInterestsItems() {
    List<DropdownMenuItem<Interests>> items = new List();
    for (Interests myFeeling in listOfInterests) {
      items.add(new DropdownMenuItem(value: myFeeling, child:
      Container(
        margin: EdgeInsets.only(top: 2, bottom: 2, left: 0),
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width * .15,
        height: MediaQuery.of(context).size.height * .030,
        child: AutoSizeText(
          myFeeling.interest.title,
            maxFontSize: 16,
            minFontSize: 5,
            maxLines: 1,

          ),
      ),

      ),);
    }
    return items;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserData();
  }

  @override
  Widget build(BuildContext context) {
//    page =1;
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getTimelineEventsResponse("1", _currentInterest!=null?_currentInterest.interest.id:-1, searchText),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => getListView(data),
    );
    var _asyncLoaderInterst = new AsyncLoader(
      key: _asyncLoaderStateInterst,
      initState: () async => await getInterestsResponse(),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => Container(),
      renderSuccess: ({data}) => getListInterestView(data),
    );
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    Translations.of(context).Timeline,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: Container(
        child: SmartRefresher(
          enablePullDown: true,
          header: ClassicHeader(
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 30, left: 10, right: 10),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 8,
                        child: Container(
                          padding: EdgeInsets.only(left: 10),
                          width: MediaQuery.of(context).size.width * .78,
                          height: MediaQuery.of(context).size.height * .06,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 3,
                                )
                              ]),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[

                              Expanded(
                                child: TypeAheadField(
                                  textFieldConfiguration:
                                  TextFieldConfiguration(
                                    onSubmitted: (term) {

                                      searchText = search_controller.text;
                                      _asyncLoaderState.currentState.reloadState();
                                    },
                                    controller: search_controller,
                                    keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.search,
                                    //inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                                    decoration: new InputDecoration(
                                        hintText: Translations.of(context)
                                            .Searchhere,
                                        labelStyle: TextStyle(
                                            fontWeight: FontWeight.bold,),
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            setState(() {
//                                              print(search_controller.text);

                                              searchText = search_controller.text;
                                              _asyncLoaderState.currentState.reloadState();
                                            });
                                          },

                                          child:Padding(
                                        padding: const EdgeInsets.all(4.0),
                                      child: InkWell(
                                        child: Container(
                                          padding: EdgeInsets.all(2),
                                          width: 35,
                                          height: 35,
                                          decoration: BoxDecoration(
                                              color: Color(0xffFDAF17), shape: BoxShape.circle),
                                          child: Image.asset(
                                            "asset/search.png",
                                            scale: 5.5,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                        ),
                                        prefixText: ' ',
//                                        suffixStyle:
//                                        TextStyle(color: Colors.blue)
                                    ),
                                  ),

                                  suggestionsCallback: (pattern) async {
                                    List<DataEvent> suggistions = new List();
                                    if(response!=null)
                                    response.data.events.data.forEach((row) =>
                                    {
                                      if (row.name.contains(pattern))
                                        {suggistions.add(row)}
                                    });
                                    return suggistions;
                                  },

                                  hideOnLoading: true,
                                  hideOnError: true,
                                  hideOnEmpty: true,
                                  getImmediateSuggestions: false,
                                  itemBuilder: (context,
                                       suggestion) {
                                    return ListTile(
                                      title: Text(suggestion.name),
                                    );
                                  },
                                  hideSuggestionsOnKeyboardHide: false,
                                  keepSuggestionsOnLoading: false,
                                  keepSuggestionsOnSuggestionSelected: false,
                                  onSuggestionSelected: ( DataEvent suggestion) async {
                                    searchText = suggestion.name;
                                    _asyncLoaderState.currentState.reloadState();
                                  },

                                  ),

                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          flex: 2,
                          child: Container(
                            height: MediaQuery.of(context).size.height * .06,
                            width: MediaQuery.of(context).size.width * .12,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 3,
                                  )
                                ]),
                            child:
                            _asyncLoaderInterst
                          ))
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Addpin()));
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 3, left: 10, right: 10),
                    width: MediaQuery.of(context).size.width,
                    //height: MediaQuery.of(context).size.height * .2,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(7),
                        boxShadow: [
                          BoxShadow(color: Colors.grey, blurRadius: 2)
                        ]),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Row(
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * .3,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image.asset(
                                      "asset/pen.png",
                                      scale: 2.5,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      Translations.of(context).AddPin,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'quicksand'),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height:
                                      MediaQuery.of(context).size.height * .06,
                                  decoration: BoxDecoration(
                                      color: Color(0xffEBEBEB),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(5),
                                          bottomLeft: Radius.circular(5))),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(left: 10, top: 10),
                          width: MediaQuery.of(context).size.width,
                          //height: MediaQuery.of(context).size.height * .06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                backgroundImage: NetworkImage(userImage),
                                backgroundColor: Colors.transparent,
                                radius: 15,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              //Image.asset("asset/women.png"),
                              Text(
                                Translations.of(context).YourPinName,
                                style: TextStyle(
                                    color: Color(0xffCECECE),
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .02,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 1,
                          decoration: BoxDecoration(color: Colors.grey),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width * .3,
                          height: MediaQuery.of(context).size.height * .035,
                          decoration: BoxDecoration(
                              color: Color(0xffEBEBEB),
                              borderRadius: BorderRadius.circular(15)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                "asset/smile.png",
                                scale: 3,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(Translations.of(context).Feeling)
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),

                _asyncLoader,
                SizedBox(
                  height: MediaQuery.of(context).size.height * .04,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }


//swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
      new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> _asyncLoaderStateInterst =
      new GlobalKey<AsyncLoaderState>();

  void _onRefresh() async {
    // monitor network fetch
    _currentInterest = null;
    search_controller.text = "";
    searchText = "";
    _asyncLoaderState.currentState.reloadState();
    _asyncLoaderStateInterst.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
  void _onLoading() async {
    // monitor network fetch
    _currentInterest = null;
    _asyncLoaderState.currentState.reloadState();
    _asyncLoaderStateInterst.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }
  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _onRefresh())
        ],
      ),
    );
  }

  EventsResponse response;

  Future<bool> _loadMore() async {
    print("onLoadMore");
    try {
      page = response.data.events.currentPage + 1;
    } catch (E) {
      page = page + 1;
    }
    response = await getTimelineEventsResponse((page).toString(), _currentInterest!=null?_currentInterest.interest.id:-1, searchText);
//    _onRefresh();
    print("load");
    setState(() {
//      itemss.addAll(response.data.events.data);
      print("data count = ${itemss.length}");
    });

    return true;
  }

  List<DataEvent> itemss = [];

  static int page = 1;
  int index = 0;

  int get count => itemss.length;

  Widget getListView(EventsResponse data) {
    if (response == null) {
      page = 1;
      itemss.clear();
      commentControllerList.clear();
      response = data;
    }
    if (page == 1) {
      if (itemss.length != commentControllerList.length)
        commentControllerList.clear();
      itemss.clear();
//      itemss.addAll(data.data.events.data);
      response = data;
    }
    itemss.addAll(response.data.events.data);
    if (itemss.length != commentControllerList.length)
      for (int i = 0; i < response.data.events.data.length; i++) {
        commentControllerList.add(new TextEditingController());
      }
    return LoadMore(
      isFinish: response.data.events.to >= data.data.events.total,
      onLoadMore: _loadMore,
      child: new ListView.builder(
        shrinkWrap: true,
        itemCount: itemss.length,
        reverse: false,
        controller: listScrollController,
        itemBuilder: (context, index) => PostTile2(index, itemss[index],
            commentControllerList[index], _asyncLoaderState, this),
      ),
      whenEmptyLoad: false,
      delegate: DefaultLoadMoreDelegate(),
      textBuilder: DefaultLoadMoreTextBuilder.english,
    );
  }


  Future<void> getUserData() async {
    pref = await SharedPreferences.getInstance();
    setState(() {
      userImage = pref.getString(Common.USER_IMAGE) ?? "";
    });
  }

  getListInterestView(InterestsResponse data) {
    listOfInterests = data.data.interests;
    return DropdownButton(
      hint: Container(
        margin: EdgeInsets.only(left: 15),
        alignment: Alignment.center,
        child: Center(
          child: Image.asset(
            "asset/side.png",
            scale: 3,
          ),
        ),
      ),
      icon: null,
      underline: null,
      selectedItemBuilder: (BuildContext context) {
        return data.data.interests.map<Widget>((Interests item) {
          return Container(
            margin: EdgeInsets.only(left: 15),
            alignment: Alignment.center,
            child: Center(
              child: Image.asset(
                "asset/side.png",
                scale: 3,
              ),
            ),
          );
        }).toList();
      },
      value: _currentInterest,
      items: getDropDownInterestsItems(),
      onChanged: (selected){
        setState(() {
          _currentInterest = selected;
          _asyncLoaderState.currentState.reloadState();
        });

      },
    );
  }

  @override
  String restate(int id) {
   /* for(int i=0;i<itemss.length;i++){
      if (itemss[i].id == id) {
        setState(() {
          itemss.remove(itemss[i]);
        });

        return "";
      }
    }*/
  }
}
abstract class restateInterface {
  String restate(int id){}
}