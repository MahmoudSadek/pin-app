import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/API/delete_account_api.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/response/delete_account_response.dart';
import 'package:pin/screens/start.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Logout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 40,
                bottom: 30,
                left: 20,
                right: 20,
              ),
              margin: EdgeInsets.all(30),
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min, // To make the card compact
                children: <Widget>[
                  Text(
                    Translations.of(context).Areyousure+" ?",
                    style: TextStyle(
                      fontSize: 22,
                      color: Color(0xff373737),
                      fontWeight: FontWeight.bold,
                      fontFamily: 'trebuchat',
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * .09),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                            //Navigator.push(context, MaterialPageRoute(builder: (context)=>Myaccout()));
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.065,
                            decoration: BoxDecoration(
                                color: Color(0xffE1E1E1),
                                borderRadius: BorderRadius.circular(25)),
                            child: Text(
                              Translations.of(context).CANCEL,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'trebuchat',
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .03,
                        ),
                        InkWell(
                          onTap: () async {
                              SharedPreferences pref = await SharedPreferences.getInstance();
                              pref.clear();
                              Navigator.pop(context);
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Start()));
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.065,
                            decoration: BoxDecoration(
                                color: Color(0xff59A0CE),
                                borderRadius: BorderRadius.circular(25)),
                            child: Text(
                              Translations.of(context).YES,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'trebuchat',
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //...bottom card part,
            //...top circlular image part,
          ],
        ),
      ),
    );
  }
}
class DeleteAccount extends StatefulWidget {
  @override
  _DeleteAccountState createState() => _DeleteAccountState();
}

class _DeleteAccountState extends State<DeleteAccount> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 40,
                bottom: 30,
                left: 20,
                right: 20,
              ),
              margin: EdgeInsets.all(30),
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min, // To make the card compact
                children: <Widget>[
                  Text(
                    Translations.of(context).Areyousureyouwantdeleteaccount+" ?",
                    style: TextStyle(
                      fontSize: 22,
                      color: Color(0xff373737),
                      fontWeight: FontWeight.bold,
                      fontFamily: 'trebuchat',
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * .09),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                            //Navigator.push(context, MaterialPageRoute(builder: (context)=>Myaccout()));
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.065,
                            decoration: BoxDecoration(
                                color: Color(0xffE1E1E1),
                                borderRadius: BorderRadius.circular(25)),
                            child: Text(
                              Translations.of(context).CANCEL,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'trebuchat',
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .03,
                        ),
                        InkWell(
                          onTap: () async {
                            _deleteAccountPress();
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.065,
                            decoration: BoxDecoration(
                                color: Color(0xff59A0CE),
                                borderRadius: BorderRadius.circular(25)),
                            child: Text(
                              Translations.of(context).YES,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'trebuchat',
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //...bottom card part,
            //...top circlular image part,
          ],
        ),
      ),
    );
  }

  Future _deleteAccountPress() async {


    DeleteAccountResponse result;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    await pr.show();
    Future.delayed(const Duration(milliseconds: 500), );
    try {
      result = await sendDeleteAccountResponse();
      pr.hide();
//      await storeUser(result);
      SharedPreferences pref = await SharedPreferences.getInstance();
      pref.clear();
      Navigator.pop(context);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Start()));

      Fluttertoast.showToast(
          msg: Translations.of(context).DONE,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {

      pr.hide();
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
}
