import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/model/response/event_details_response.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/screens/replyes.dart';
import 'package:pin/utils/common.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../language/translation_strings.dart';
import '../model/body/add_comment_body.dart';

class CommentsNew extends StatefulWidget {
  List<Comment> comments=[];
  int id, indexFile;
  String type;
  CommentsNew(this.comments, this.id, this.type,{this.indexFile});

  @override
  _CommentsState createState() => _CommentsState(comments, id, type, indexFile);
}

class _CommentsState extends State<CommentsNew> {
  int reply;
  String type;
  int id, indexFile;
  List<Comment> comments=[];
  _CommentsState( this.comments, this.id, this.type, this.indexFile);
  List<TextEditingController> commentControllerList = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for (int i = 0; i < comments.length; i++) {
      commentControllerList.add(new TextEditingController());
    }
  }
  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getEventDtailsResponse(id.toString()),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => Container(),
      renderSuccess: ({data}) => getView(data),
    );
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: SingleChildScrollView(
        child: Container(
          color: Colors.transparent,
          child: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .27),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * .08,
                    )),
                    _asyncLoader
                    ,
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .15),
                height: 100,
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(bottom: 0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey, blurRadius: 3, spreadRadius: 2)
                      ]),
                  width: 70,
                  height: 70,
                  child: Image.asset(
                    "asset/heartlike.png",
                    scale: 3.5,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .38),
                alignment: Alignment.topCenter,
                child: Material(
                    color: Colors.transparent,
                    child: Text(
                      "Comments",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'quicksand'),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _comment(BuildContext context, Comment model, int position) {

    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * .02),
      width: MediaQuery.of(context).size.width,
      //     height: MediaQuery.of(context).size.height*.19,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.height * .15,
                    // height: MediaQuery.of(context).size.height * .15,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * .05),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.height * .06,
                          height: MediaQuery.of(context).size.height * .06,
                          margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * .01),
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                              color: Colors.grey, shape: BoxShape.circle),
                          child: CircleAvatar(
                            backgroundImage: new NetworkImage(model.user.image),
                            radius: MediaQuery.of(context).size.width * .06,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Material(
                            color: Colors.transparent,
                            child: Text(
                              model.user.name==null?"":model.user.name,
                              style: TextStyle(
                                  fontFamily: 'probapro',
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              color: Color(0xffF5F5F5),
                              width: MediaQuery.of(context).size.width * .72,
                              //height: 60,
                              margin: EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * .07,
                                  top: MediaQuery.of(context).size.height *
                                      .004),
                              child: Material(
                                color: Colors.transparent,
                                child: Text(
                                model.comment,
                                  style: TextStyle(
                                      fontSize: 13, fontFamily: 'cavior'),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * .08,
                left: MediaQuery.of(context).size.width * .2),
            alignment: Alignment.centerRight,
            child: Material(
                color: Colors.transparent,
                child: reply == model.id
                    ? Container(
                        // height: MediaQuery.of(context).size.width * .12,
                        padding: EdgeInsets.only(left: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.grey),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 6,
                              child: TextField(
                                controller: commentControllerList[position],
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: Translations.of(context).Typeacomment,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    reply = -1;
                                    commentEvent(model.id,position);
                                  });
                                },
                                child: Image.asset(
                                  "asset/sent.png",
                                  color: Colors.blue,
                                  scale: 4,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                reply = model.id;
                              });
                            },
                            child: Container(
                              child: Text(
                                  Translations.of(context).Reply,
                                style:
                                    TextStyle(color: Colors.blue, fontSize: 14),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      Replies(model.replys, model.id, Common.EVENT));
                            },
                            child: Container(
                              child: Text(
                                  Translations.of(context).Replys,
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 14),
                              ),
                            ),
                          ),
                          Text(
                            model.createdAt,
                            style: TextStyle(color: Colors.grey, fontSize: 12),
                          ),
                        ],
                      )),
          ),
        ],
      ),
    );
  }

  Future<void> commentEvent(int commentId, int position) async {
    if (commentControllerList[position].text.isEmpty) {
      return;
    }
    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      AddCommnetBody body = AddCommnetBody();
      body.comment = commentControllerList[position].text;
      var result = await addReplayEventResponse(commentId.toString(),id.toString(), type, body);
      commentControllerList[position].text = "";
      _asyncLoaderState.currentState.reloadState();
      pr.hide();
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }


  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();

  getView(EventDetailsResponse data) {
    if(type==Common.FILE){
      comments = data.data.event.files[indexFile].comments;
    }else
    comments = data.data.event.comments;
    return ListView.builder(
      shrinkWrap: true,
      itemCount: comments.length,
      reverse: false,
      itemBuilder: (context, index) => _comment(context, comments[index], index),
    );
  }

}
