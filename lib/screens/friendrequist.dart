import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/screens/Dashboard.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../API/deny_a_friend_request_api.dart';
import '../API/get_pending_friend_api.dart';
import '../API/get_pending_friend_requests_api.dart';
import '../language/translation_strings.dart';
import '../model/body/deny_a_friend_equest_body.dart';
import '../model/response/deny_a_friend_request_response.dart';
import '../model/response/pending_friend_requests_response.dart';
import '../model/response/pending_friends_response.dart';
import 'myaccount.dart';

class Friendrequest extends StatefulWidget {
  @override
  _FriendrequestState createState() => _FriendrequestState();
}

final GlobalKey<AsyncLoaderState> _asyncLoadergetPendingFriendRequest =
    new GlobalKey<AsyncLoaderState>();

final GlobalKey<AsyncLoaderState> _asyncLoadergetPending =
    new GlobalKey<AsyncLoaderState>();

class _FriendrequestState extends State<Friendrequest> {
  List<bool> isSelected;
  List<bool> isSelect;

  @override
  void initState() {
    isSelected = [true, false];
  }

  @override
  Widget build(BuildContext context) {
    var _PendingFriendRequest = new AsyncLoader(
      key: _asyncLoadergetPendingFriendRequest,
      initState: () async => await getPendingFriendRequestsResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    var _PendingFriend = new AsyncLoader(
      key: _asyncLoadergetPending,
      initState: () async => await getPendingFriendsResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget2(),
      renderSuccess: ({data}) => setWidegtDataPendingFriend(data),
    );
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 10),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Dashboard()),
                      );
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .24,
                  ),
                  Text(
                    Translations.of(context).FriendRequests,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.only(top: 10, bottom: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey, blurRadius: 5, spreadRadius: 2)
                  ]),
              child: ToggleButtons(
                borderColor: Colors.white,
                fillColor: Color(0xff59A0CE),
                borderWidth: .09,
                selectedBorderColor: Color(0xff59A0CE),
                selectedColor: Colors.white,
                borderRadius: BorderRadius.circular(10),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 25),
                    child: Text(
                    Translations.of(context).Requests,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'trebuchat'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 25),
                    child: Text(
                      Translations.of(context).Sent,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'trebuchat'),
                    ),
                  ),
                ],
                onPressed: (int index) {
                  setState(() {
                    for (int buttonIndex = 0;
                        buttonIndex < isSelected.length;
                        buttonIndex++) {
                      if (buttonIndex == index) {
                        isSelected[buttonIndex] = true;
                      } else {
                        isSelected[buttonIndex] = false;
                      }
                    }
                    print(isSelected);
                  });
                },
                isSelected: isSelected,
              ),
            ),
//            Container(
//              height: MediaQuery.of(context).size.height * .78,
//              child: ListView.builder(
//                  itemCount: 6,
//                  itemBuilder: (context, index) {
//                    return
//                      Container(
//                        child:
//                            isSelected[0] ? _PendingFriendRequest : _invite(index));
//                  }),
//            )
            Container(
                child:
                isSelected[0] ? _PendingFriendRequest : _PendingFriend)
          ],
        ),
      ),
    );
  }
  //swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    _asyncLoadergetPendingFriendRequest.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }  void _onLoading() async {
    // monitor network fetch
    _asyncLoadergetPendingFriendRequest.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }
  //swipe refresh
  RefreshController _refreshController2 =
  RefreshController(initialRefresh: false);

  void _onRefresh2() async {
    // monitor network fetch
    _asyncLoadergetPending.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController2.refreshCompleted();
  }  void _onLoading2() async {
    // monitor network fetch
    _asyncLoadergetPending.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController2.loadComplete();
  }

  Widget _addfriend(DataBean model,index) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 5),
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 3,
            )
          ]),
      child: Row(
//
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Container(
              margin: EdgeInsets.only(left: 10, top: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage:
                        NetworkImage(model.friends.elementAt(index).sender.image),
                    radius: 25,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          model.friends.elementAt(index).sender.name,
                          style:
                              TextStyle(fontSize: 14, fontFamily: 'trebuchat'),
                        ),
                        SizedBox(
                          height: 5,
                        ),
//                        Row(
//                          children: <Widget>[
//                            Image.asset(
//                              "asset/pin.png",
//                              scale: 3,
//                            ),
//                            SizedBox(
//                              width: 3,
//                            ),
//                            Text(
//                         "ll",
//                              style: TextStyle(
//                                  fontSize: 12, fontFamily: 'trebuchat'),
//                            )
//                          ],
//                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .05, right: 5),
              padding: EdgeInsets.all(5),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 5,
                    child: GestureDetector(
                      onTap: (){
                        _sendAcceptaFriendRequest(model,index);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .05,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xff0072BB),
                        ),
                        child: Text(
                          Translations.of(context).Confirm,
                          style: TextStyle(fontSize: 13, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    flex: 4,
                    child: GestureDetector(
                      onTap: (){
                        _sendDenyRequestPress(model,index);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .05,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffE1E1E1),
                        ),
                        child: Text(
                          Translations.of(context).Delete,
                          style: TextStyle(fontSize: 13, color: Colors.black),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _invite(Data5 model2,index) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 5),
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 3,
            )
          ]),
      child: Row(
//
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.only(left: 10, top: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage:
                        NetworkImage(model2.friends.elementAt(index).recipient.image),
                    radius: 25,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          model2.friends.elementAt(index).recipient.name,
                          style:
                              TextStyle(fontSize: 17, fontFamily: 'trebuchat'),
                        ),
                        SizedBox(
                          height: 5,
                        ),
//                        Row(
//                          children: <Widget>[
//                            Image.asset(
//                              "asset/pin.png",
//                              scale: 3,
//                            ),
//                            SizedBox(
//                              width: 3,
//                            ),
//                            Text(
//                              id == 0
//                                  ? "Cario"
//                                  : id == 1
//                                      ? "Benha"
//                                      : id == 2
//                                          ? "Naser City"
//                                          : id == 3
//                                              ? "Mansora"
//                                              : id == 4
//                                                  ? "Shebeen"
//                                                  : id == 5
//                                                      ? "Alex"
//                                                      : "pakistan",
//                              style: TextStyle(
//                                  fontSize: 12, fontFamily: 'trebuchat'),
//                            )
//                          ],
//                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .05, right: 5),
              padding: EdgeInsets.all(5),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
//                                alignment: Alignment.center,
//                                width: MediaQuery.of(context).size.width,
//                                height: MediaQuery.of(context).size.height*.05,
//                                decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(5),
//                                  color: Color(0xff0072BB),
//
//                                ),
//                                child: Text("Add Friend",style: TextStyle(fontSize: 13,color: Colors.white),),
                        ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    flex: 6,
                    child: GestureDetector(
                      onTap: () {
                        _sendRemoveFriend(model2,index);

                  },
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .05,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffE1E1E1),
                        ),
                        child: Text(
                          Translations.of(context).CancelRequest,
                          style: TextStyle(fontSize: 13, color: Colors.black),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoadergetPendingFriendRequest.currentState.reloadState())
        ],
      ),
    );
  }

  Widget getNoConnectionWidget2() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () =>
                  _asyncLoadergetPending.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtData(PendingFriendRequestsResponse data) {
    return data.data.friends.length <= 0
        ? Center(
            child: Container(
                height: MediaQuery.of(context).size.width,
                child: SmartRefresher(
                    enablePullDown: true,
                    header: ClassicHeader(
                    ),
                    controller: _refreshController,
                    onRefresh: _onRefresh,
                    onLoading: _onLoading,
                    child:
                Center(
                  child: Text(
                    Translations.of(context).Youhavenotreceivedfriendrequestsyet,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ))),
          )
        : ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: data.data.friends.length,
            itemBuilder: (context, index) {
              return _addfriend(data.data,index);
            });
  }

  setWidegtDataPendingFriend(PendingFriendsResponse data2) {
    return data2.data.friends.length <= 0
        ? Center(
            child: Container(
                height: MediaQuery.of(context).size.width,
                child: SmartRefresher(
                    enablePullDown: true,
                    header: ClassicHeader(
                    ),
                    controller: _refreshController2,
                    onRefresh: _onRefresh2,
                    onLoading: _onLoading2,
                    child:
                Center(
                  child: Text(
                    Translations.of(context).Youhavenotsentfriendrequestsyet,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ))),
          )
        : ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: data2.data.friends.length,
            itemBuilder: (context, index) {
              return _invite(data2.data,index);
            });
  }

  Future _sendDenyRequestPress(DataBean model,index) async {
    DenyAFriendEquestBody body = DenyAFriendEquestBody();

    body.friendId = model.friends.elementAt(index).sender.id;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();

    DenyAFriendRequestResponse result;
    try {
      result = await sendDenyAFriendRequestResponse(body);
//      await storeUser(result);
//      pr.dismiss();
      _asyncLoadergetPendingFriendRequest.currentState.reloadState();
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: Translations.of(context).therequesthasbeencanceled,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
  Future _sendAcceptaFriendRequest(DataBean model33,index) async {
    DenyAFriendEquestBody body = DenyAFriendEquestBody();

    body.friendId = model33.friends.elementAt(index).sender.id;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();

    DenyAFriendRequestResponse result;
    try {
      result = await sendAcceptaFriendRequest(body);
//      await storeUser(result);
      _asyncLoadergetPendingFriendRequest.currentState.reloadState();
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: Translations.of(context).Therequestwasapproved,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
  Future _sendRemoveFriend(Data5 model,index) async {
    DenyAFriendEquestBody body = DenyAFriendEquestBody();

    body.friendId = model.friends.elementAt(index).recipient.id;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();

    DenyAFriendRequestResponse result;
    try {
      result = await sendRemoveFriend(body);

      _asyncLoadergetPending.currentState.reloadState();
//      await storeUser(result);
      Navigator.pop(context);
//      Navigator.push(context,
//          MaterialPageRoute(builder: (context) => Friendrequest()));
      Fluttertoast.showToast(
          msg: Translations.of(context).Requeshasbeenrejected,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
}
