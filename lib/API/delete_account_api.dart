

import 'package:http/http.dart' as http ;
import 'package:pin/model/response/delete_account_response.dart';
import 'package:pin/model/response/error_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';




Future<DeleteAccountResponse> sendDeleteAccountResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.post(Common.MAIN_API_URL+'api/user/deleteAccount',headers: headers);

  if (response.statusCode == 200){
    return DeleteAccountResponse.fromMap(json.decode(response.body));
  } else {

    ErrorResponse res = ErrorResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}




