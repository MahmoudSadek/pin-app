import 'events_response.dart';

class EventDetailsResponse {
  bool status;
  String msg;
  Data data;

  EventDetailsResponse({this.status, this.msg, this.data});

  EventDetailsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  DataEvent event;

  Data({this.event});

  Data.fromJson(Map<String, dynamic> json) {
    event = json['event'] != null ? new DataEvent.fromJson(json['event']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.event != null) {
      data['event'] = this.event.toJson();
    }
    return data;
  }
}




