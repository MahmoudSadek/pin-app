import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../language/translation_strings.dart';
import '../model/body/add_comment_body.dart';

class Replies extends StatefulWidget {
  List<Replys> comments=[];
  int id;
  String type;
  Replies(this.comments, this.id, this.type);

  @override
  _CommentsState createState() => _CommentsState(comments, id, type);
}

class _CommentsState extends State<Replies> {
  int reply;
  String type;
  int id;
  List<Replys> comments=[];
  _CommentsState( this.comments, this.id, this.type);
  List<TextEditingController> commentControllerList = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for (int i = 0; i < comments.length; i++) {
      commentControllerList.add(new TextEditingController());
    }
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: SingleChildScrollView(
        child: Container(
          color: Colors.transparent,
          child: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .27),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * .08,
                    )),
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: comments.length,
                      reverse: false,
                      itemBuilder: (context, index) => _comment(context, comments[index], index),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .15),
                height: 100,
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(bottom: 0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey, blurRadius: 3, spreadRadius: 2)
                      ]),
                  width: 70,
                  height: 70,
                  child: Image.asset(
                    "asset/heartlike.png",
                    scale: 3.5,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .38),
                alignment: Alignment.topCenter,
                child: Material(
                    color: Colors.transparent,
                    child: Text(
                      "Comments",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'quicksand'),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _comment(BuildContext context, Replys model, int position) {

    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * .02),
      width: MediaQuery.of(context).size.width,
      //     height: MediaQuery.of(context).size.height*.19,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.height * .15,
                    // height: MediaQuery.of(context).size.height * .15,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * .05),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.height * .06,
                          height: MediaQuery.of(context).size.height * .06,
                          margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * .01),
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                              color: Colors.grey, shape: BoxShape.circle),
                          child: CircleAvatar(
                            backgroundImage: new NetworkImage(model.user.image),
                            radius: MediaQuery.of(context).size.width * .06,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Material(
                            color: Colors.transparent,
                            child: Text(
                              model.user.name==null?"":model.user.name,
                              style: TextStyle(
                                  fontFamily: 'probapro',
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              color: Color(0xffF5F5F5),
                              width: MediaQuery.of(context).size.width * .72,
                              //height: 60,
                              margin: EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * .07,
                                  top: MediaQuery.of(context).size.height *
                                      .004),
                              child: Material(
                                color: Colors.transparent,
                                child: Text(
                                model.comment,
                                  style: TextStyle(
                                      fontSize: 13, fontFamily: 'cavior'),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * .08,
                left: MediaQuery.of(context).size.width * .2),
            alignment: Alignment.centerRight,
            child: Material(
                color: Colors.transparent,
                child: reply == model.id
                    ? Container(
                        // height: MediaQuery.of(context).size.width * .12,
                        padding: EdgeInsets.only(left: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.grey),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 6,
                              child: TextField(
                                controller: commentControllerList[position],
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: Translations.of(context).Typeacomment,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    reply = -1;
                                    commentEvent(model.id,position);
                                  });
                                },
                                child: Image.asset(
                                  "asset/sent.png",
                                  color: Colors.blue,
                                  scale: 4,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            model.createdAt,
                            style: TextStyle(color: Colors.grey, fontSize: 12),
                          ),
                        ],
                      )),
          ),
        ],
      ),
    );
  }

  Future<void> commentEvent(int commentId, int position) async {
    if (commentControllerList[position].text.isEmpty) {
      return;
    }
    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      AddCommnetBody body = AddCommnetBody();
      body.comment = commentControllerList[position].text;
      var result = await addReplayEventResponse(commentId.toString(),id.toString(), type, body);
      commentControllerList[position].text = "";
      pr.hide();
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
