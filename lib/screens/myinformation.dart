import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:pin/screens/Dashboard.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../API/get_user_info_api.dart';
import 'editinformation.dart';
import '../model/response/get_user_info_response.dart';
import 'myaccount.dart';
import 'myprofile.dart';

class Myinformation extends StatefulWidget {
  @override
  _MyinformationState createState() => _MyinformationState();
}

class _MyinformationState extends State<Myinformation> {

  final GlobalKey<AsyncLoaderState> _asyncLoaderStateGetUserInfo =
  new GlobalKey<AsyncLoaderState>();

  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderStateGetUserInfo,
      initState: () async => await getGetUserInfoResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(horizontal: 14),
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Dashboard()),
                        );
                      },
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * .24,
                    ),
                    Text(
                      Translations.of(context).MyInformation,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'trebuchat'),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(60.0)),
        body: SmartRefresher(
            enablePullDown: true,
            header: ClassicHeader(
            ),
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child:
            _asyncLoader)
    );
  }

  //swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }  void _onLoading() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  Widget _widget(int id,UserBean model) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(10),

      width: MediaQuery.of(context).size.width,
      // height: MediaQuery.of(context).size.height*.17,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(7),
          boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            id == 1
                ? Translations.of(context).AboutMe
                : id == 2
                    ? Translations.of(context).Activities
                    : id == 3
                        ? Translations.of(context).Hobbies
                            : id == 4 ? Translations.of(context).Education : Translations.of(context).Work,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            id == 1
                ?
            model.aboutMe==null?"":
            model.aboutMe
                : id == 2
                ?
            model.activities ==null?"":
            model.activities
                : id == 3
                ?
            model.hobbies ==null?"":
            model.hobbies
                : id == 4
                ?
            model.education ==null?"":
            model.education
                :
            model.work ==null?"":
            model.work,
            style: TextStyle(
                fontSize: 14,
                fontFamily: 'quicksand',
                color: Color(0xffBEBEBE)),
          )
        ],
      ),
    );
  }

  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () =>
                  _asyncLoaderStateGetUserInfo.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtData(GetUserInfoResponse data) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            _widget(1,data.data.user),
            _widget(2,data.data.user),
            _widget(3,data.data.user),
            _widget(4,data.data.user),
            _widget(5,data.data.user),
            Container(
              margin: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * .12,
                  right: MediaQuery.of(context).size.width * .12,
                  top: 20),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * .065,
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Editinformation()));
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Color(0xff59A0CE)),
                  child: Text(
                    Translations.of(context).EditInformation,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'quicksand',
                        color: Colors.white),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * .03,
            )
          ],
        ),
      ),
    );
  }
}
