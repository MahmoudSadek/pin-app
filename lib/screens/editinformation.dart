import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/API/get_user_info_api.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/response/get_user_info_response.dart';
import 'package:pin/screens/customdialog.dart';
import 'package:pin/dialogboxs/savedinfo.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../API/update_info_user_api.dart';
import 'Dashboard.dart';
import '../model/body/update_info_account_body.dart';
import '../model/body/update_info_user_body.dart';
import '../model/response/update_info_user_response.dart';
import 'myaccount.dart';
import 'myinformation.dart';

class Editinformation extends StatefulWidget {
  @override
  _EditinformationState createState() => _EditinformationState();
}
final GlobalKey<AsyncLoaderState> _asyncLoaderStateGetUserInfo33 =
new GlobalKey<AsyncLoaderState>();
class _EditinformationState extends State<Editinformation> {
  var edu = ['Bachelors', 'Intermediate', 'Matrric', 'O/A level'];
  String dropdownitem;

  TextEditingController aboutMeEditingController = TextEditingController();
  TextEditingController activitiesEditingController = TextEditingController();
  TextEditingController hobbiesEditingController = TextEditingController();
  TextEditingController workEditingController = TextEditingController();
  TextEditingController educationEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderStateGetUserInfo33,
      initState: () async => await getGetUserInfoResponse(),
      renderLoad: () => Padding(
        padding: const EdgeInsets.only(top: 100),
        child: Center(child: CircularProgressIndicator()),
      ),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Myinformation()),
                      );
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .23,
                  ),
                  Text(
                    Translations.of(context).EditInformation,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: SmartRefresher(
        enablePullDown: true,
        header: ClassicHeader(
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child:
        SingleChildScrollView(
          child: Container(
            child: _asyncLoader
            ),
      ),)
    );
  }
  //swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo33.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }  void _onLoading() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo33.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }
  Widget _editinfo(int id,DataBean22 model ) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10, top: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              id == 1
                  ? Translations.of(context).AboutMe
                  : id == 2
                      ? Translations.of(context).Activities
                      : id == 3
                          ? Translations.of(context).Hobbies
                              : id == 4 ? Translations.of(context).Education : Translations.of(context).Work,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 20),
            margin: EdgeInsets.only(left: 10, right: 10, top: 5),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * .07,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)],
                color: Colors.white),
            child: TextField(
              controller:
              id == 1
                  ? aboutMeEditingController
                  : id == 2
                  ? activitiesEditingController
                  : id == 3
                  ? hobbiesEditingController
                  : id == 4 ? educationEditingController : workEditingController,
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText:

                id == 1
                    ? model.user.aboutMe
                    : id == 2
                    ? model.user.activities
                    : id == 3
                    ? model.user.hobbies
                    : id == 4 ? model.user.education : model.user.work,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 15, top: 5),
            alignment: Alignment.centerRight,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
//                Text(
//                  "0",
//                  style: TextStyle(
//                      fontSize: 10,
//                      fontFamily: 'quicksand',
//                      color: Colors.grey),
//                ),
//                Text(
//                  id == 1 ? "/200" : "/300",
//                  style: TextStyle(
//                      fontSize: 10,
//                      fontFamily: 'quicksand',
//                      color: Colors.grey),
//                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _education(int id) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10, top: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              Translations.of(context).Education,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 20),
            margin: EdgeInsets.only(left: 10, right: 10, top: 5),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * .07,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)],
                color: Colors.white),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                isExpanded: true,
                items: edu.map((String dropdownitem) {
                  return DropdownMenuItem<String>(
                    value: dropdownitem,
                    child: Text(dropdownitem),
                  );
                }).toList(),
                hint: new Text(dropdownitem==null?"Select Education":dropdownitem),
                onChanged: (String newValueSelected) {
                  setState(() {
                    dropdownitem = newValueSelected;
                  });
                },
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 15, top: 5),
            alignment: Alignment.centerRight,
          ),
        ],
      ),
    );
  }
  _saveEditInformationPress() async {
    if (aboutMeEditingController.text.isNotEmpty ||
        activitiesEditingController.text.isNotEmpty ||
        hobbiesEditingController.text.isNotEmpty ||
        workEditingController.text.isNotEmpty ||
        dropdownitem!=null) {
      UpdateInfoAccountBody body = UpdateInfoAccountBody();
      body.aboutMe = aboutMeEditingController.text;
      body.hobbies = hobbiesEditingController.text;
      body.activities = activitiesEditingController.text;
      body.work = workEditingController.text;
      body.education = dropdownitem;


      ProgressDialog pr = new ProgressDialog(context);
      pr = new ProgressDialog(context,
          type: ProgressDialogType.Normal,
          isDismissible: true,
          showLogs: true);
      await pr.show();
      UpdateInfoUserResponse result;
      try {
        result = await sendUpdateInfoAccountResponse(body);
//      pr.dismiss();

        Navigator.pop(context);


        Fluttertoast.showToast(
            msg: Translations
                .of(context)
                .Theinformationhasbeenmodified,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      } catch (e) {
        pr.hide();
        Fluttertoast.showToast(
            msg: e.message,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }
  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () =>
                  _asyncLoaderStateGetUserInfo33.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtData(GetUserInfoResponse data) {
    return Column(
      children: <Widget>[

        _editinfo(1,data.data),
        _editinfo(2,data.data),
        _editinfo(3,data.data),
//            _editinfo(4),
        _education(1),
        _editinfo(5,data.data),
        Container(
          margin: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * .12,
              right: MediaQuery.of(context).size.width * .12,
              top: 20),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * .065,
          child: InkWell(
            onTap: () {
              _saveEditInformationPress().
              whenComplete(() {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => Savedinfo(),
                );
                Navigator.pop(context);
              });

            },
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Color(0xff59A0CE)),
              child: Text(
                Translations.of(context).Save,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'quicksand',
                    color: Colors.white),
              ),
            ),
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * .03,
        )
      ],
    );
  }
}
