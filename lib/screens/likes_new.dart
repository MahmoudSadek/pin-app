import 'package:flutter/material.dart';
import 'package:pin/model/response/events_response.dart';

class LikesNew extends StatelessWidget {
  List<Favorites> favorites =[];
  LikesNew(this.favorites);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          color: Colors.transparent,
          child: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .28),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * .08,
                    )),
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: favorites.length,
                      reverse: false,
                      itemBuilder: (context, index) => _likes(context, favorites[index]),
                    ),
//                    _likes(context, 1),

                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .15),
                height: 100,
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(bottom: 0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey, blurRadius: 3, spreadRadius: 2)
                      ]),
                  width: 70,
                  height: 70,
                  child: Image.asset(
                    "asset/heartlike.png",
                    scale: 3.5,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * .38),
                alignment: Alignment.topCenter,
                child: Material(
                    color: Colors.transparent,
                    child: Text(
                      "Likes",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'quicksand'),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _likes(BuildContext context, Favorites model) {
    return Container(
      margin: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * .01,
          left: MediaQuery.of(context).size.height * .04,
          right: MediaQuery.of(context).size.height * .04),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width * .18,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.height * .06,
                      height: MediaQuery.of(context).size.height * .06,
                      padding: EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          color: Colors.grey, shape: BoxShape.circle),
                      child: CircleAvatar(
                        backgroundImage:  new NetworkImage(model.user.image),
                        radius: MediaQuery.of(context).size.width * .06,
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * .02,
                    ),
                    Material(
                        color: Colors.transparent,
                        child: Text(
                          model.user.name,
                          style:
                              TextStyle(fontSize: 18, fontFamily: 'quicksand'),
                        ))
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xffC5C5C5),
                              blurRadius: 3,
                              spreadRadius: 2)
                        ]),
                    width: 40,
                    height: 40,
                    child: Image.asset(
                      "asset/heartlike.png",
                      scale: 6,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * .02,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 1,
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
