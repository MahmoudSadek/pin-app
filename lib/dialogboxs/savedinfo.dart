import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pin/constents/context.dart';
import 'package:pin/screens/myinformation.dart';

class Savedinfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => Myinformation(),
            )));
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 40,
                bottom: 30,
                left: 20,
                right: 20,
              ),
              margin: EdgeInsets.all(30),
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min, // To make the card compact
                children: <Widget>[
                  Container(
                    height: 100,
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(top: 0),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xff00FF80),
                      ),
                      width: 80,
                      height: 80,
                      child: Image.asset(
                        "asset/save.png",
                        scale: 3,
                      ),
                    ),
                  ),
                  Text(
                    "Congratulations",
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'quicksand',
                    ),
                  ),
                  SizedBox(height: 16.0),
                  SizedBox(height: 24.0),
                  Container(
                    child: Text(
                      "You have successfully saved your information.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),

            //...bottom card part,
            //...top circlular image part,
          ],
        ),
      ),
    );
  }
}
