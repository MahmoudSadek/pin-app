

class GetMyFriendsResponse {
  bool status;
  String msg;
  DataBean90 data;
  List<dynamic> errors;

  static GetMyFriendsResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GetMyFriendsResponse getMyFriendsResponseBean = GetMyFriendsResponse();
    getMyFriendsResponseBean.status = map['status'];
    getMyFriendsResponseBean.msg = map['msg'];
    getMyFriendsResponseBean.data = DataBean90.fromMap(map['data']);
    getMyFriendsResponseBean.errors = map['errors'];
    return getMyFriendsResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean90 {
  List<FriendsBean> friends;

  static DataBean90 fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean90 dataBean = DataBean90();
    dataBean.friends = List()..addAll(
      (map['friends'] as List ?? []).map((o) => FriendsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "friends": friends,
  };
}



class FriendsBean {
  int id;
  String name;
  String email;
  String phone;
  dynamic gender;
  String image;
  String cover;
  dynamic aboutMe;
  dynamic activities;
  dynamic hobbies;
  dynamic education;
  dynamic work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  String createdAt;
  String updatedAt;

  static FriendsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FriendsBean friendsBean = FriendsBean();
    friendsBean.id = map['id'];
    friendsBean.name = map['name'];
    friendsBean.email = map['email'];
    friendsBean.phone = map['phone'];
    friendsBean.gender = map['gender'];
    friendsBean.image = map['image'];
    friendsBean.cover = map['cover'];
    friendsBean.aboutMe = map['about_me'];
    friendsBean.activities = map['activities'];
    friendsBean.hobbies = map['hobbies'];
    friendsBean.education = map['education'];
    friendsBean.work = map['work'];
    friendsBean.notification = map['notification'];
    friendsBean.eventsPublic = map['events_public'];
    friendsBean.friendsPublic = map['friends_public'];
    friendsBean.gcm = map['gcm'];
    friendsBean.deletedAt = map['deleted_at'];
    friendsBean.createdAt = map['created_at'];
    friendsBean.updatedAt = map['updated_at'];
    return friendsBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}