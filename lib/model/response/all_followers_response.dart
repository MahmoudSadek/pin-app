

class AllFollowersResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static AllFollowersResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AllFollowersResponse allFollowersResponseBean = AllFollowersResponse();
    allFollowersResponseBean.status = map['status'];
    allFollowersResponseBean.msg = map['msg'];
    allFollowersResponseBean.data = DataBean.fromMap(map['data']);
    allFollowersResponseBean.errors = map['errors'];
    return allFollowersResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  List<FollowersBean> followers;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.followers = List()..addAll(
      (map['followers'] as List ?? []).map((o) => FollowersBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "followers": followers,
  };
}

class FollowersBean {
  int id;
  String name;
  String email;
  String phone;
  dynamic gender;
  String image;
  String cover;
  dynamic aboutMe;
  dynamic activities;
  dynamic hobbies;
  dynamic education;
  dynamic work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  String createdAt;
  String updatedAt;
  PivotBean pivot;

  static FollowersBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FollowersBean followersBean = FollowersBean();
    followersBean.id = map['id'];
    followersBean.name = map['name'];
    followersBean.email = map['email'];
    followersBean.phone = map['phone'];
    followersBean.gender = map['gender'];
    followersBean.image = map['image'];
    followersBean.cover = map['cover'];
    followersBean.aboutMe = map['about_me'];
    followersBean.activities = map['activities'];
    followersBean.hobbies = map['hobbies'];
    followersBean.education = map['education'];
    followersBean.work = map['work'];
    followersBean.notification = map['notification'];
    followersBean.eventsPublic = map['events_public'];
    followersBean.friendsPublic = map['friends_public'];
    followersBean.gcm = map['gcm'];
    followersBean.deletedAt = map['deleted_at'];
    followersBean.createdAt = map['created_at'];
    followersBean.updatedAt = map['updated_at'];
    followersBean.pivot = PivotBean.fromMap(map['pivot']);
    return followersBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "pivot": pivot,
  };
}



class PivotBean {
  String followingId;
  String followerId;
  String acceptedAt;
  String createdAt;
  String updatedAt;

  static PivotBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PivotBean pivotBean = PivotBean();
    pivotBean.followingId = map['following_id'];
    pivotBean.followerId = map['follower_id'];
    pivotBean.acceptedAt = map['accepted_at'];
    pivotBean.createdAt = map['created_at'];
    pivotBean.updatedAt = map['updated_at'];
    return pivotBean;
  }

  Map toJson() => {
    "following_id": followingId,
    "follower_id": followerId,
    "accepted_at": acceptedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}