

import 'package:http/http.dart' as http ;
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/response/all_followers_response.dart';
import 'package:pin/utils/common.dart';







Future<AllFollowersResponse> getAllFollowersResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/followers/followers',headers: headers);

  if (response.statusCode == 200){
    return AllFollowersResponse.fromMap(json.decode(response.body));
  } else {

    AllFollowersResponse res = AllFollowersResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors[1]);
  }
}


