// To parse this JSON data, do
//
//     final allFriendsOfFriendsResponse = allFriendsOfFriendsResponseFromJson(jsonString);

import 'dart:convert';

AllFriendsOfFriendsResponse allFriendsOfFriendsResponseFromJson(String str) => AllFriendsOfFriendsResponse.fromJson(json.decode(str));

String allFriendsOfFriendsResponseToJson(AllFriendsOfFriendsResponse data) => json.encode(data.toJson());

class AllFriendsOfFriendsResponse {
  AllFriendsOfFriendsResponse({
    this.status,
    this.msg,
    this.data,
    this.errors,
  });

  bool status;
  String msg;
  Data data;
  List<dynamic> errors;

  factory AllFriendsOfFriendsResponse.fromJson(Map<String, dynamic> json) => AllFriendsOfFriendsResponse(
    status: json["status"],
    msg: json["msg"],
    data: Data.fromJson(json["data"]),
    errors: List<dynamic>.from(json["errors"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "msg": msg,
    "data": data.toJson(),
    "errors": List<dynamic>.from(errors.map((x) => x)),
  };
}

class Data {
  Data({
    this.friends,
  });

  Friends friends;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    friends: Friends.fromJson(json["friends"]),
  );

  Map<String, dynamic> toJson() => {
    "friends": friends.toJson(),
  };
}

class Friends {
  Friends({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int currentPage;
  List<Datum> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  dynamic nextPageUrl;
  String path;
  int perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  factory Friends.fromJson(Map<String, dynamic> json) => Friends(
    currentPage: json["current_page"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    firstPageUrl: json["first_page_url"],
    from: json["from"],
    lastPage: json["last_page"],
    lastPageUrl: json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    path: json["path"],
    perPage: json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "first_page_url": firstPageUrl,
    "from": from,
    "last_page": lastPage,
    "last_page_url": lastPageUrl,
    "next_page_url": nextPageUrl,
    "path": path,
    "per_page": perPage,
    "prev_page_url": prevPageUrl,
    "to": to,
    "total": total,
  };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.gender,
    this.image,
    this.cover,
    this.aboutMe,
    this.activities,
    this.hobbies,
    this.education,
    this.work,
    this.notification,
    this.eventsPublic,
    this.friendsPublic,
    this.gcm,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.isFollowing,
    this.hasRequestedToFollow,
    this.hasFriendRequestFrom,
    this.hasSentFriendRequestTo,
  });

  int id;
  String name;
  String email;
  String phone;
  dynamic gender;
  String image;
  String cover;
  dynamic aboutMe;
  dynamic activities;
  dynamic hobbies;
  dynamic education;
  dynamic work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  DateTime createdAt;
  DateTime updatedAt;
  bool isFollowing;
  bool hasRequestedToFollow;
  bool hasFriendRequestFrom;
  bool hasSentFriendRequestTo;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    gender: json["gender"],
    image: json["image"],
    cover: json["cover"],
    aboutMe: json["about_me"],
    activities: json["activities"],
    hobbies: json["hobbies"],
    education: json["education"],
    work: json["work"],
    notification: json["notification"],
    eventsPublic: json["events_public"],
    friendsPublic: json["friends_public"],
    gcm: json["gcm"],
    deletedAt: json["deleted_at"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    isFollowing: json["isFollowing"],
    hasRequestedToFollow: json["hasRequestedToFollow"],
    hasFriendRequestFrom: json["hasFriendRequestFrom"],
    hasSentFriendRequestTo: json["hasSentFriendRequestTo"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "isFollowing": isFollowing,
    "hasRequestedToFollow": hasRequestedToFollow,
    "hasFriendRequestFrom": hasFriendRequestFrom,
    "hasSentFriendRequestTo": hasSentFriendRequestTo,
  };
}
