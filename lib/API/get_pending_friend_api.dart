

import 'package:http/http.dart' as http ;
import 'package:pin/model/response/pending_friends_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';




Future<PendingFriendsResponse> getPendingFriendsResponse() async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/friends/getPendingFriendships',headers: headers);

  if (response.statusCode == 200){
    return PendingFriendsResponse.fromJson(json.decode(response.body));
  } else {

    PendingFriendsResponse res = PendingFriendsResponse.fromJson(json.decode(response.body));
    throw Exception(res.errors);
  }
}




