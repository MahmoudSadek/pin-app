

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/update_info_account_body.dart';
import 'package:pin/model/body/update_info_user_body.dart';
import 'package:pin/model/response/update_info_user_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';









Future<UpdateInfoUserResponse> sendUpdateInfoUserResponse (UpdateInfoUserBody updateInfoUserBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(updateInfoUserBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/updateInfo',body: body,headers: headers);

  if (response.statusCode == 200){
    return UpdateInfoUserResponse.fromMap(json.decode(response.body));
  } else {

    UpdateInfoUserResponse res = UpdateInfoUserResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}


Future<UpdateInfoUserResponse> sendUpdateInfoAccountResponse (UpdateInfoAccountBody updateInfoUserBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(updateInfoUserBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/updateInfo',body: body,headers: headers);

  if (response.statusCode == 200){
    return UpdateInfoUserResponse.fromMap(json.decode(response.body));
  } else {

    UpdateInfoUserResponse res = UpdateInfoUserResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}




