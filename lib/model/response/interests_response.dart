class InterestsResponse {
  bool status;
  String msg;
  Data data;

  InterestsResponse({this.status, this.msg, this.data});

  InterestsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Interests> interests;

  Data({this.interests});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['interests'] != null) {
      interests = new List<Interests>();
      json['interests'].forEach((v) {
        interests.add(new Interests.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.interests != null) {
      data['interests'] = this.interests.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Interests {
  int id;
  String userId;
  String interestId;
  Null createdAt;
  Null updatedAt;
  Interest interest;
  List<Locations> locations;

  Interests(
      {this.id,
        this.userId,
        this.interestId,
        this.createdAt,
        this.updatedAt,
        this.interest,
        this.locations});

  Interests.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    interestId = json['interest_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    interest = json['interest'] != null
        ? new Interest.fromJson(json['interest'])
        : null;
    if (json['locations'] != null) {
      locations = new List<Locations>();
      json['locations'].forEach((v) {
        locations.add(new Locations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['interest_id'] = this.interestId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.interest != null) {
      data['interest'] = this.interest.toJson();
    }
    if (this.locations != null) {
      data['locations'] = this.locations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Interest {
  int id;
  String createdAt;
  String updatedAt;
  String title;
  String image;

  Interest({this.id, this.createdAt, this.updatedAt, this.title, this.image});

  Interest.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    title = json['title'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['title'] = this.title;
    data['image'] = this.image;
    return data;
  }
}

class Locations {
  int id;
  String userInterestId;
  String lat;
  String lon;
  String km;
  String createdAt;
  String updatedAt;

  Locations(
      {this.id,
        this.userInterestId,
        this.lat,
        this.lon,
        this.km,
        this.createdAt,
        this.updatedAt});

  Locations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userInterestId = json['user_interest_id'];
    lat = json['lat'];
    lon = json['lon'];
    km = json['km'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_interest_id'] = this.userInterestId;
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    data['km'] = this.km;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
