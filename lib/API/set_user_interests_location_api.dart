

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/set_user_interest_location_body.dart';
import 'package:pin/model/response/set_user_interest_location_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';





Future<SetUserInterestLocationResponse> sendSetUserInterestLocationResponse (SetUserInterestLocationBody setUserInterestLocationBody) async {

  var headers= await Common.getHeaders();

  var body = json.encode(setUserInterestLocationBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/interest/location',body: body,headers: headers);

  if (response.statusCode == 200){
    return SetUserInterestLocationResponse.fromMap(json.decode(response.body));
  } else {

    SetUserInterestLocationResponse res = SetUserInterestLocationResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}




