

class GetMyFriendNfoResponse {
  bool status;
  String msg;
  DataBean00 data;
  List<dynamic> errors;

  static GetMyFriendNfoResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GetMyFriendNfoResponse getMyFriendNfoResponseBean = GetMyFriendNfoResponse();
    getMyFriendNfoResponseBean.status = map['status'];
    getMyFriendNfoResponseBean.msg = map['msg'];
    getMyFriendNfoResponseBean.data = DataBean00.fromMap(map['data']);
    getMyFriendNfoResponseBean.errors = map['errors'];
    return getMyFriendNfoResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean00 {
  UserBean user;

  static DataBean00 fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean00 dataBean = DataBean00();
    dataBean.user = UserBean.fromMap(map['user']);
    return dataBean;
  }

  Map toJson() => {
    "user": user,
  };
}

class UserBean {
  int id;
  String name;
  String email;
  String phone;
  dynamic gender;
  String image;
  String cover;
  dynamic aboutMe;
  dynamic activities;
  dynamic hobbies;
  dynamic education;
  dynamic work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  String createdAt;
  String updatedAt;
  String eventsCount;
  String followersCount;
  String followingsCount;
  bool isFollowing;
  bool hasRequestedToFollow;
  bool isFriendWith;
  bool hasSentFriendRequestTo;
  bool hasFriendRequestFrom;
  bool hasBlocked;
  bool isBlockedBy;

  static UserBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserBean userBean = UserBean();
    userBean.id = map['id'];
    userBean.name = map['name'];
    userBean.email = map['email'];
    userBean.phone = map['phone'];
    userBean.gender = map['gender'];
    userBean.image = map['image'];
    userBean.cover = map['cover'];
    userBean.aboutMe = map['about_me'];
    userBean.activities = map['activities'];
    userBean.hobbies = map['hobbies'];
    userBean.education = map['education'];
    userBean.work = map['work'];
    userBean.notification = map['notification'];
    userBean.eventsPublic = map['events_public'];
    userBean.friendsPublic = map['friends_public'];
    userBean.gcm = map['gcm'];
    userBean.deletedAt = map['deleted_at'];
    userBean.createdAt = map['created_at'];
    userBean.updatedAt = map['updated_at'];
    userBean.eventsCount = map['events_count'];
    userBean.followersCount = map['followers_count'];
    userBean.followingsCount = map['followings_count'];
    userBean.isFollowing = map['isFollowing'];
    userBean.hasRequestedToFollow = map['hasRequestedToFollow'];
    userBean.isFriendWith = map['isFriendWith'];
    userBean.hasSentFriendRequestTo = map['hasSentFriendRequestTo'];
    userBean.hasFriendRequestFrom = map['hasFriendRequestFrom'];
    userBean.hasBlocked = map['hasBlocked'];
    userBean.isBlockedBy = map['isBlockedBy'];
    return userBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "events_count": eventsCount,
    "followers_count": followersCount,
    "followings_count": followingsCount,
    "isFollowing": isFollowing,
    "hasRequestedToFollow": hasRequestedToFollow,
    "isFriendWith": isFriendWith,
    "hasSentFriendRequestTo": hasSentFriendRequestTo,
    "hasFriendRequestFrom": hasFriendRequestFrom,
    "hasBlocked": hasBlocked,
    "isBlockedBy": isBlockedBy,
  };
}