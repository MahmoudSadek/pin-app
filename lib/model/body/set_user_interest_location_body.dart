
import 'package:google_maps_flutter/google_maps_flutter.dart';

class SetUserInterestLocationBody {
  List<LocationBean> locations;

  static SetUserInterestLocationBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SetUserInterestLocationBody setUserInterestLocationBodyBean = SetUserInterestLocationBody();
    setUserInterestLocationBodyBean.locations = List()..addAll(
      (map['locations'] as List ?? []).map((o) => LocationBean.fromMap(o))
    );
    return setUserInterestLocationBodyBean;
  }

  Map toJson() => {
    "locations": locations,
  };
}


class LocationBean {
  int interestid;
  String interestName;
  String locationName;
  double lat;
  Marker marker;
  double lon;
  int km;

  static LocationBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LocationBean locationsBean = LocationBean();
    locationsBean.interestid = map['interest_id'];
    locationsBean.interestName = map['interestName'];
    locationsBean.locationName = map['locationName'];
    locationsBean.marker = map['marker'];
    locationsBean.lat = map['lat'];
    locationsBean.lon = map['lon'];
    locationsBean.km = map['km'];
    return locationsBean;
  }

  Map toJson() => {
    "interest_id": interestid,
    "interestName": interestName,
    "locationName": locationName,
    "marker": marker,
    "lat": lat,
    "lon": lon,
    "km": km,
  };
}