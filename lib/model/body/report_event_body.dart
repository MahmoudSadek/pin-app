class ReportEventBody {
  String eventid;
  String reason;

  ReportEventBody({this.eventid, this.reason});

  ReportEventBody.fromJson(Map<String, dynamic> json) {
    eventid = json['event_id'];
    reason = json['reason'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['event_id'] = this.eventid;
    data['reason'] = this.reason;
    return data;
  }
}
