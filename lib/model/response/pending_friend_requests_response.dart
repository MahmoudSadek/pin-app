

class PendingFriendRequestsResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static PendingFriendRequestsResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PendingFriendRequestsResponse pendingFriendRequestsResponseBean = PendingFriendRequestsResponse();
    pendingFriendRequestsResponseBean.status = map['status'];
    pendingFriendRequestsResponseBean.msg = map['msg'];
    pendingFriendRequestsResponseBean.data = DataBean.fromMap(map['data']);
    pendingFriendRequestsResponseBean.errors = map['errors'];
    return pendingFriendRequestsResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  List<FriendsBean> friends;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.friends = List()..addAll(
      (map['friends'] as List ?? []).map((o) => FriendsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "friends": friends,
  };
}

class FriendsBean {
  int id;
  String senderType;
  String senderId;
  String recipientType;
  String recipientId;
  String status;
  String createdAt;
  String updatedAt;
  SenderBean sender;
  RecipientBean recipient;

  static FriendsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FriendsBean friendsBean = FriendsBean();
    friendsBean.id = map['id'];
    friendsBean.senderType = map['sender_type'];
    friendsBean.senderId = map['sender_id'];
    friendsBean.recipientType = map['recipient_type'];
    friendsBean.recipientId = map['recipient_id'];
    friendsBean.status = map['status'];
    friendsBean.createdAt = map['created_at'];
    friendsBean.updatedAt = map['updated_at'];
    friendsBean.sender = SenderBean.fromMap(map['sender']);
    friendsBean.recipient = RecipientBean.fromMap(map['recipient']);
    return friendsBean;
  }

  Map toJson() => {
    "id": id,
    "sender_type": senderType,
    "sender_id": senderId,
    "recipient_type": recipientType,
    "recipient_id": recipientId,
    "status": status,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "sender": sender,
    "recipient": recipient,
  };
}

class RecipientBean {
  int id;
  String name;
  String email;
  String phone;
  String gender;
  String image;
  String cover;
  String aboutMe;
  String activities;
  String hobbies;
  String education;
  String work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  String createdAt;
  String updatedAt;

  static RecipientBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RecipientBean recipientBean = RecipientBean();
    recipientBean.id = map['id'];
    recipientBean.name = map['name'];
    recipientBean.email = map['email'];
    recipientBean.phone = map['phone'];
    recipientBean.gender = map['gender'];
    recipientBean.image = map['image'];
    recipientBean.cover = map['cover'];
    recipientBean.aboutMe = map['about_me'];
    recipientBean.activities = map['activities'];
    recipientBean.hobbies = map['hobbies'];
    recipientBean.education = map['education'];
    recipientBean.work = map['work'];
    recipientBean.notification = map['notification'];
    recipientBean.eventsPublic = map['events_public'];
    recipientBean.friendsPublic = map['friends_public'];
    recipientBean.gcm = map['gcm'];
    recipientBean.deletedAt = map['deleted_at'];
    recipientBean.createdAt = map['created_at'];
    recipientBean.updatedAt = map['updated_at'];
    return recipientBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}


class SenderBean {
  int id;
  String name;
  String email;
  String phone;
  String gender;
  String image;
  String cover;
  String aboutMe;
  String activities;
  String hobbies;
  String education;
  String work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  String createdAt;
  String updatedAt;

  static SenderBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SenderBean senderBean = SenderBean();
    senderBean.id = map['id'];
    senderBean.name = map['name'];
    senderBean.email = map['email'];
    senderBean.phone = map['phone'];
    senderBean.gender = map['gender'];
    senderBean.image = map['image'];
    senderBean.cover = map['cover'];
    senderBean.aboutMe = map['about_me'];
    senderBean.activities = map['activities'];
    senderBean.hobbies = map['hobbies'];
    senderBean.education = map['education'];
    senderBean.work = map['work'];
    senderBean.notification = map['notification'];
    senderBean.eventsPublic = map['events_public'];
    senderBean.friendsPublic = map['friends_public'];
    senderBean.gcm = map['gcm'];
    senderBean.deletedAt = map['deleted_at'];
    senderBean.createdAt = map['created_at'];
    senderBean.updatedAt = map['updated_at'];
    return senderBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}