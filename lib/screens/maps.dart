import 'dart:async';
import 'dart:typed_data';

import 'package:async_loader/async_loader.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/response/events_maps_response.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/model/response/interests_response.dart';
import 'package:pin/repository/add_event_api.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/screens/pindetail.dart';
import 'package:pin/states/app_state.dart';
import 'package:pin/utils/common.dart';
import 'package:pin/widgets/mark_generator.dart';

import 'addpin.dart';

class Maps extends StatefulWidget {
  @override
  _MapsState createState() => _MapsState();
}
class _MapsState extends State<Maps> {
  Completer<GoogleMapController> _controller = Completer();
  static CameraPosition lahore = CameraPosition(
    target: AppState.initialPositions,
    zoom: 12.4746,
  );

  List<Uint8List> markerImage = [];

  EventsMapsResponse reponse, _eventsResponse;
  bool enter = false, gettingData = false;
  List<Widget> widgetList = new List();

  List<Interests> listOfInterests = [];
  Interests _currentInterest = null;

  List<DropdownMenuItem<Interests>> getDropDownInterestsItems() {
    List<DropdownMenuItem<Interests>> items = new List();
    for (Interests myFeeling in listOfInterests) {
      items.add(new DropdownMenuItem(value: myFeeling, child:
      Container(
        margin: EdgeInsets.only(top: 2, bottom: 2, left: 0),
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width * .15,
        height: MediaQuery.of(context).size.height * .030,
        child: AutoSizeText(
          myFeeling.interest.title,
          maxFontSize: 16,
          minFontSize: 5,
          maxLines: 1,

        ),
      ),

      ),);
    }
    return items;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
    AppState().addListener(() async {


      final GoogleMapController controller = await _controller.future;
      setState(() {
        lahore = CameraPosition(
          target: AppState.initialPositions,
          zoom: 12.4746,
        );
        controller.animateCamera(CameraUpdate.newCameraPosition(lahore));
        getData();
      });
    });
//    itemView();
  }

  List<Marker> mMarker = [];

  @override
  Widget build(BuildContext context) {
    var _asyncLoaderInterst = new AsyncLoader(
      key: _asyncLoaderStateInterst,
      initState: () async => await getInterestsResponse(),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => Container(),
      renderSuccess: ({data}) => getListInterestView(data),
    );

    if (_eventsResponse != null  ) {
      if (_eventsResponse.data.events != null || enter)  {
        if (_eventsResponse == null) enter = false;
        for (int i = 0; i < reponse.data.events.length; i++) {
          widgetList.add(
            Container(
              width: 150,
              height: 150,
              /* decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(color: Color(0xff77189F)),
                color: Colors.white),*/
              alignment: Alignment.center,
              child: Stack(
                children: <Widget>[
                  Transform.translate(
                    offset: Offset(0.0, 0.0),
                    child:
                    // Adobe XD layer: 'gps' (group)
                    Image.asset(
                      "asset/logopin.png",
                      scale: 6,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Transform.translate(
                      offset: Offset(-167.0, -188.3),
                      child: Stack(
                        children: <Widget>[
                          Transform.translate(
                            offset: Offset(205.0, 204.93),
                            child: SizedBox(
                              width: 70.0,
                              child: Column(
                                children: [
                                  Text(
                                    reponse.data.events[i].name,
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      fontSize: 12,
                                      color: const Color(0xffffffff),
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    '',
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      fontSize: 18,
                                      color: const Color(0xffffffff),
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Transform.translate(
                              offset: Offset(161.0, 194.0),
                              child:
                              // Adobe XD layer: 'NoPath - Copy (43)' (shape)
                              Container(
                                width: 44,
                                height: 48,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    image: DecorationImage(
                                        image: NetworkImage(reponse
                                            .data.events[i].userBean.image),
                                        fit: BoxFit.fill)),
                                child: Container(),
                              )),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }
        MarkerGenerator(widgetList, (bitmaps) {
//      setState(() {
          markerImage = bitmaps;

          completeData();
//      });
        }).generate(context);
      }
    }
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              padding: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Center(
                child: Text(
                  Translations.of(context).Home,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'trebuchat'),
                ),
              ),
            ),
            preferredSize: Size.fromHeight(60.0)),
        floatingActionButton: GestureDetector(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Addpin()));
          },
          child: Container(
            height: 50.00,
            width: 50.00,
            decoration: BoxDecoration(
              color: Color(0xffffffff),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0.00, 0.00),
                  color: Color(0xff000000).withOpacity(0.45),
                  blurRadius: 40,
                ),
              ],
              borderRadius: BorderRadius.circular(44.00),
            ),
            child: Center(
              child: Image.asset(
                "asset/addlocation.png",
                scale: 4,
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              zoomGesturesEnabled: true,
              scrollGesturesEnabled: true,
              mapType: MapType.terrain,
              markers: mMarker.toSet(),
              initialCameraPosition: lahore,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              onCameraMove: ((_position) => _updatePosition(_position)),
            ),
            Positioned(
              top: 10,
              right: 20,
              left: 20,
              child: Container(
                padding:
                    EdgeInsets.only(top: 15, bottom: 15, left: 230, right: 20),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width * .14,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    image: DecorationImage(
                        image: AssetImage("asset/edd.png"), fit: BoxFit.cover)),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: Color(0xff77189F)),
                      color: Colors.white),
                  alignment: Alignment.center,
                  child: Text(
                    "Order Now",
                    style: TextStyle(fontSize: 12, color: Color(0xff77189F)),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 0,
              bottom: 0,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: gettingData?CircularProgressIndicator():Container(),
              ),

            ),
            Positioned(
              right: 0,
              bottom: 110,
              child: Container(
                  height: MediaQuery.of(context).size.height * .06,
                  width: MediaQuery.of(context).size.width * .18,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 3,
                        )
                      ]),
                  child: _asyncLoaderInterst
              )

            ),
          ],
        ));
  }

  final GlobalKey<AsyncLoaderState> _asyncLoaderStateInterst =
  new GlobalKey<AsyncLoaderState>();


  getListInterestView(InterestsResponse data) {
    listOfInterests = data.data.interests;
    return DropdownButton(
      hint: Container(
        margin: EdgeInsets.only(left: 15),
        alignment: Alignment.center,
        child: Center(
          child: Image.asset(
            "asset/side.png",
            scale: 3,
          ),
        ),
      ),
      icon: null,
      underline: null,
      selectedItemBuilder: (BuildContext context) {
        return data.data.interests.map<Widget>((Interests item) {
          return Container(
            margin: EdgeInsets.only(left: 15),
            alignment: Alignment.center,
            child: Center(
              child: Image.asset(
                "asset/side.png",
                scale: 3,
              ),
            ),
          );
        }).toList();
      },
      value: _currentInterest,
      items: getDropDownInterestsItems(),
      onChanged: (selected){
        setState(() {
          _currentInterest = selected;
          getData();
        });

      },
    );
  }

  Future<void> getData() async {
    setState(() {
      gettingData = true;
    });
    reponse = await getMapEventsResponse(lahore.target.latitude.toString(), lahore.target.longitude.toString());
    _eventsResponse = reponse;
    enter = true;
    setState(() {
      gettingData = false;
    });
  }

  Future<void> completeData() async {
    for (int i = 0; i < reponse.data.events.length; i++) {
//      await _capturePng();
      mMarker.add(Marker(
        markerId: MarkerId(reponse.data.events[i].id.toString()),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Pindetail(reponse.data.events[i].id.toString()),
              ));
        },
        icon: BitmapDescriptor.fromBytes(markerImage[i]),
        position: LatLng(double.parse(reponse.data.events[i].lat),
            double.parse(reponse.data.events[i].lon)),
      ));
    }
    setState(() {
      _eventsResponse = null;
    });
  }

  Future<void> _updatePosition(CameraPosition _position) async {
    int distance = await Common.updatePosition(
        _position.target.latitude,
        _position.target.longitude,
        lahore.target.latitude,
        lahore.target.longitude);
    if (distance > 20) {
      lahore = CameraPosition(
        target: _position.target,
        zoom: 10.4746,
      );
      getData();
    }
  }
}
