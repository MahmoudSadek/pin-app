import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'lang/messages_all.dart';

class Translations {
  static Future<Translations> load(Locale locale) {
    final String name =
    (locale.countryCode != null && locale.countryCode.isEmpty)
        ? locale.languageCode
        : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((dynamic _) {
      Intl.defaultLocale = localeName;
      return new Translations();
    });
  }

  static Translations of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations);
  }
  String get SIGNIN {
    return Intl.message(
      'SIGN IN',
      name: 'SIGN IN',
    );
  }

  String get SIGNUP {
    return Intl.message(
      'SIGN UP',
      name: 'SIGN UP',
    );
  }
  String get SKIP {
    return Intl.message(
      'SKIP',
      name: 'SKIP',
    );
  }
  String get Creat_new_Accont {
    return Intl.message(
      'Create new Accont',
      name: 'Create new Accont',
    );
  }
  String get SignupwithFacebook {
    return Intl.message(
      'Signup with Facebook',
      name: 'Signup with Facebook',
    );
  }

  String get SignupwithGoogle {
    return Intl.message(
      'Sign up with Google',
      name: 'Sign up with Google',
    );
  }


  String get Next {
    return Intl.message(
      'Next',
      name: 'Next',
    );
  }
  String get SuggestInterests {
    return Intl.message(
      'Suggest Interests',
      name: 'Suggest Interests',
    );
  }
  String get SuggestInterest {
    return Intl.message(
      'Suggest Interest',
      name: 'Suggest Interest',
    );
  }
  String get Pleasechooseyourinterestsbelow {
    return Intl.message(
      'Please choose your interests below',
      name: 'Please choose your interests below',
    );
  }
  String get Suggestion {
    return Intl.message(
      'Suggestion',
      name: 'Suggestion',
    );
  }
  String get Reason {
    return Intl.message(
      'Reason',
      name: 'Reason',
    );
  }
  String get Submit {
    return Intl.message(
      'Submit',
      name: 'Submit',
    );
  }
  String get DONE {
    return Intl.message(
      'DONE',
      name: 'DONE',
    );
  }
  String get Yoursuggestionswillbereviewedandapprovedifitmeetsourstandards {
    return Intl.message(
      'Your suggestions will be reviewed and approved if it meets our standards',
      name: 'Your suggestions will be reviewed and approved if it meets our standards',
    );
  }
  String get Thanks {
    return Intl.message(
      'Thanks',
      name: 'Thanks',
    );
  }
  String get InterestsLocation {
    return Intl.message(
      'Interests Location',
      name: 'Interests Location',
    );
  }
  String get Pleasechooseyourinterestslocation {
    return Intl.message(
      'Please choose your interests location',
      name: 'Please choose your interests location',
    );
  }
  String get SelectInterest {
    return Intl.message(
      'Select Interest',
      name: 'Select Interest',
    );
  }
  String get Add {
    return Intl.message(
      'Add',
      name: 'Add',
    );
  }
  String get ConnectPintoyourFacebook {
    return Intl.message(
      'Connect Pin to your Facebook account to invite friends and share your moments with them',
      name: 'Connect Pin to your Facebook account to invite friends and share your moments with them',
    );
  }
  String get Connect {
    return Intl.message(
      'Connect',
      name: 'Connect',
    );
  }
  String get NoThanks {
    return Intl.message(
      'No Thanks',
      name: 'No Thanks',
    );
  }
  String get CreatnewAccont {
    return Intl.message(
      'Creat new Accont',
      name: 'Creat new Accont',
    );
  }
  String get SigninwithFacebook {
    return Intl.message(
      'Sign in with Facebook',
      name: 'Sign in with Facebook',
    );
  }
  String get SigninwithGoogle {
    return Intl.message(
      'Sign in with Google',
      name: 'Sign in with Google',
    );
  }
  String get MyInterests {
    return Intl.message(
      'My Interests',
      name: 'My Interests',
    );
  }
  String get Timeline {
    return Intl.message(
      'Timeline',
      name: 'Timeline',
    );
  }
  String get MyFriends {
    return Intl.message(
      'My Friends',
      name: 'My Friends',
    );
  }
  String get Maps {
    return Intl.message(
      'Maps',
      name: 'Maps',
    );
  }

  String get Chats {
    return Intl.message(
      'Chats',
      name: 'Chats',
    );
  }
  String get Chat {
    return Intl.message(
      'Chat',
      name: 'Chat',
    );
  }

  String get MyAccount {
    return Intl.message(
      'My Account',
      name: 'My Account',
    );
  }
  String get Searchhere {
    return Intl.message(
      'Search here',
      name: 'Search here',
    );
  }

  String get AddPin {
    return Intl.message(
      'Add Pin',
      name: 'Add Pin',
    );
  }
  String get YourPinName {
    return Intl.message(
      'Your Pin Name',
      name: 'Your Pin Name',
    );
  }
  String get Feeling {
    return Intl.message(
      'Feeling',
      name: 'Feeling',
    );
  }
  String get Typeacomment {
    return Intl.message(
      'Type a comment',
      name: 'Type a comment',
    );
  }
  String get Description {
    return Intl.message(
      'Description',
      name: 'Description',
    );
  }
  String get StartDate {
    return Intl.message(
      'Start Date',
      name: 'Start Date',
    );
  }

  String get EndDate {
    return Intl.message(
      'End Date',
      name: 'End Date',
    );
  }

  String get StartTime {
    return Intl.message(
      'Start Time',
      name: 'Start Time',
    );
  }
  String get EndTime {
    return Intl.message(
      'End Time',
      name: 'End Time',
    );
  }

  String get AddLocation {
    return Intl.message(
      'Add Location',
      name: 'Add Location',
    );
  }

  String get TagYourFriend {
    return Intl.message(
      'Tag Your Friend',
      name: 'Tag Your Friend',
    );
  }
  String get Selectyourfriend {
    return Intl.message(
      'Select your friend',
      name: 'Select your friend',
    );
  }
  String get Categories {
    return Intl.message(
      'Categories',
      name: 'Categories',
    );
  }
  String get UploadPicture {
    return Intl.message(
      'Upload Picture',
      name: 'Upload Picture',
    );
  }
  String get Draft {
    return Intl.message(
      'Draft',
      name: 'Draft',
    );
  }
  String get Publish {
    return Intl.message(
      'Publish',
      name: 'Publish',
    );
  }
  String get noInternetConnection {
    return Intl.message(
      'No Internet Connection',
      name: 'No Internet Connection',
    );
  }
  String get retry {
    return Intl.message(
      'Retry',
      name: 'Retry',
    );
  }
  String get UploadVideo {
    return Intl.message(
      'Upload Video',
      name: 'Upload Video',
    );
  }
  String get enter {
    return Intl.message(
      'enter',
      name: 'enter',
    );
  }
  String get error {
    return Intl.message(
      'error',
      name: 'error',
    );
  }
  String get Schedule {
    return Intl.message(
      'Schedule',
      name: 'Schedule',
    );
  }
  String get SchedulePublish {
    return Intl.message(
      'Schedule Publish',
      name: 'Schedule Publish',
    );
  }
  String get Reply {
    return Intl.message(
      'Reply',
      name: 'Reply',
    );
  }
  String get Replys {
    return Intl.message(
      'Replies',
      name: 'Replies',
    );
  }
  String get Follow {
    return Intl.message(
      'Follow',
      name: 'Follow',
    );
  }
  String get AddFriend {
    return Intl.message(
      'Add Friend',
      name: 'Add Friend',
    );
  }
  String get Home {
    return Intl.message(
      'Home',
      name: 'Home',
    );
  }
  String get NoChatyet {
    return Intl.message(
      'No Chat yet',
      name: 'No Chat yet',
    );
  }
  String get Seeyourprofile {
    return Intl.message(
      'See your profile',
      name: 'See your profile',
    );
  }
  String get Deleteaccount {
    return Intl.message(
      'Delete account',
      name: 'Delete account',
    );
  }
  String get SignOut {
    return Intl.message(
      'Sign Out',
      name: 'Sign Out',
    );
  }
  String get MyInformation {
    return Intl.message(
      'My Information',
      name: 'My Information',
    );
  }
  String get FriendRequests {
    return Intl.message(
      'Friend Requests',
      name: 'Friend Requests',
    );
  }
  String get Notification {
    return Intl.message(
      'Notification',
      name: 'Notification',
    );
  }
  String get Showeventsforallusers {
    return Intl.message(
      'Show events for all users',
      name: 'Show events for all users',
    );
  }

  String get Makemyfriendspublic {
    return Intl.message(
      'Make my friends public',
      name: 'Make my friends public',
    );
  }
  String get AboutMe {
    return Intl.message(
      'About Me',
      name: 'About Me',
    );
  }
  String get Activities {
    return Intl.message(
      'Activities',
      name: 'Activities',
    );
  }
  String get Hobbies {
    return Intl.message(
      'Hobbies',
      name: 'Hobbies',
    );
  }
  String get Education {
    return Intl.message(
      'Education',
      name: 'Education',
    );
  }

  String get Work {
    return Intl.message(
      'Work',
      name: 'Work',
    );
  }
  String get EditInformation {
    return Intl.message(
      'Edit Information',
      name: 'Edit Information',
    );
  }
  String get YouMsutEnterInfoAboutYou {
    return Intl.message(
      'You Msut Enter Info About You',
      name: 'You Msut Enter Info About You',
    );
  }
  String get YouMsutEnterActivities {
    return Intl.message(
      'You Msut Enter Activities',
      name: 'You Msut Enter Activities',
    );
  }
  String get YouMsutEnterHobbies {
    return Intl.message(
      'You Msut Enter Hobbies',
      name: 'You Msut Enter Hobbies',
    );
  }
  String get YouMsutEnterYourWork {
    return Intl.message(
      'You Msut Enter Your Work',
      name: 'You Msut Enter Your Work',
    );
  }
  String get YouMsutChooseEducation {
    return Intl.message(
      'You Msut Choose Education',
      name: 'You Msut Choose Education',
    );
  }
  String get Theinformationhasbeenmodified {
    return Intl.message(
      'The information has been modified',
      name: 'The information has been modified',
    );
  }
  String get Save {
    return Intl.message(
      'Save',
      name: 'Save',
    );
  }
  String get Requests {
    return Intl.message(
      ' Requests ',
      name: ' Requests ',
    );
  }
  String get Sent {
    return Intl.message(
      '      Sent     ',
      name: '      Sent     ',
    );
  }
  String get Confirm {
    return Intl.message(
      'Confirm',
      name: 'Confirm',
    );
  }
  String get Delete {
    return Intl.message(
      'Delete',
      name: 'Delete',
    );
  }
  String get CancelRequest {
    return Intl.message(
      'Cancel Request',
      name: 'Cancel Request',
    );
  }
  String get Youhavenotreceivedfriendrequestsyet {
    return Intl.message(
      'You have not received friend requests yet',
      name: 'You have not received friend requests yet',
    );
  }
  String get Youhavenotsentfriendrequestsyet {
    return Intl.message(
      'You have not sent friend requests yet',
      name: 'You have not sent friend requests yet',
    );
  }
  String get therequesthasbeencanceled {
    return Intl.message(
      'the request has been canceled',
      name: 'the request has been canceled',
    );
  }
  String get Therequestwasapproved {
    return Intl.message(
      'The request was approved',
      name: 'The request was approved',
    );
  }
  String get Requeshasbeenrejected {
    return Intl.message(
      'Reques has been rejected',
      name: 'Reques has been rejected',
    );
  }
  String get MyProfile {
    return Intl.message(
      'My Profile',
      name: 'My Profile',
    );
  }
  String get MyEvents {
    return Intl.message(
      'My Events',
      name: 'My Events',
    );
  }
  String get Events {
    return Intl.message(
      'Events',
      name: 'Events',
    );
  }
  String get Followers {
    return Intl.message(
      'Followers',
      name: 'Followers',
    );
  }
  String get Following {
    return Intl.message(
      'Following',
      name: 'Following',
    );
  }
  String get Following2 {
    return Intl.message(
      'Following2',
      name: 'Following2',
    );
  }
  String get Therearenointerestsyet {
    return Intl.message(
      'There are no interests yet',
      name: 'There are no interests yet',
    );
  }
  String get Edit {
    return Intl.message(
      'Edit',
      name: 'Edit',
    );
  }
  String get Younothavefriendsyet {
    return Intl.message(
      'You not have friends yet',
      name: 'You not have friends yet',
    );
  }
  String get Changeddone {
    return Intl.message(
      'Changed done',
      name: 'Changed done',
    );
  }
  String get Therearenofollowersyet {
    return Intl.message(
      'There are no followers yet',
      name: 'There are no followers yet',
    );
  }
  String get Therearenofollowingyet {
    return Intl.message(
      'There are no following yet',
      name: 'There are no following yet',
    );
  }
  String get Requestsent {
    return Intl.message(
      'Request sent',
      name: 'Request sent',
    );
  }
  String get Unfollow {
    return Intl.message(
      'Unfollow',
      name: 'Unfollow',
    );
  }
  String get Friendprofile {
    return Intl.message(
      'Friend profile',
      name: 'Friend profile',
    );
  }
  String get Friends {
    return Intl.message(
      'Friends',
      name: 'Friends',
    );
  }
  String get Interests {
    return Intl.message(
      'Interests',
      name: 'Interests',
    );
  }
  String get Therearenofriendsyet {
    return Intl.message(
      'There are no friends yet',
      name: 'There are no friends yet',
    );
  }
  String get Suggestedfriends {
    return Intl.message(
      'Suggested friends',
      name: 'Suggested friends',
    );
  }
  String get Therearenofriendshipsuggestionsyet {
    return Intl.message(
      'There are no friendship suggestions yet',
      name: 'There are no friendship suggestions yet',
    );
  }
  String get YES {
    return Intl.message(
      'YES',
      name: 'YES',
    );
  }
  String get CANCEL {
    return Intl.message(
      'CANCEL',
      name: 'CANCEL',
    );
  }
  String get Areyousureyouwantdeleteaccount {
    return Intl.message(
      'Are you sure you want delete account',
      name: 'Are you sure you want delete account',
    );
  }
  String get Areyousure {
    return Intl.message(
      'Are you sure',
      name: 'Are you sure',
    );
  }
  String get Interestsidentified {
    return Intl.message(
      'Interests identified',
      name: 'Interests identified',
    );
  }
  String get youMsutChooseanyfrominterests {
    return Intl.message(
      'you Msut Choose any from interests',
      name: 'you Msut Choose any from interests',
    );
  }
  String get youmustselectyourinteres {
    return Intl.message(
      'you must select your interes',
      name: 'you must select your interes',
    );
  }
  String get Yourinterestsarelocated {
    return Intl.message(
      'Your interests are located',
      name: 'Your interests are located',
    );
  }
  String get Youmustregisterfirst {
    return Intl.message(
      'You must register first',
      name: 'You must register first',
    );
  }
  String get Signinsuccessful {
    return Intl.message(
      'Sign-in successful',
      name: 'Sign-in successful',
    );
  }
  String get Accountsuccessfullycreated {
    return Intl.message(
      'Account successfully created',
      name: 'Account successfully created',
    );
  }
  String get EnableGPS {
    return Intl.message(
      'Enable GPS',
      name: 'EnableGPS',
    );
  }
  String get DeletePin {
    return Intl.message(
      'Delete Pin',
      name: 'Delete Pin',
    );
  }

  String get EditPin {
    return Intl.message(
      'Edit Pin',
      name: 'Edit Pin',
    );
  }
  String get ReportPin {
    return Intl.message(
      'Report Pin',
      name: 'Report Pin',
    );
  }

  String get LanguageCode {
    return Intl.message(
      'LanguageCode',
      name: 'LanguageCode',
    );
  }








}
