import 'dart:async';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:pin/dialogboxs/gps_dialog.dart';
import 'package:pin/screens/start.dart';
import 'package:pin/states/app_state.dart';
import 'package:pin/utils/common.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Dashboard.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  Animation _animation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 4));
    _animation = Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(parent: _animationController, curve: Curves.ease));

    _animation.addListener(() {
      setState(() {});
    });
    _animation.addStatusListener((state) {
      if (state == AnimationStatus.completed) {
        navigationPage();
      }
    });

    _animationController.forward();

    this.initDynamicLinks();
  }

  void initDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;

    if (deepLink != null) {
      Navigator.pushNamed(context, deepLink.path);
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;

      if (deepLink != null) {
        Navigator.pushNamed(context, deepLink.path);
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  Future navigationPage() async {

    bool isLocationEnabled = await Geolocator().isLocationServiceEnabled();
    if(isLocationEnabled) {
      AppState();
     Common.CheckUser(context);
    }else{
      showDialog(
        context: context,
        builder: (BuildContext context) => GPSEnableDialog(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
        child: Opacity(
            opacity: _animation.value,
            child: new Stack(
              children: <Widget>[
                Opacity(
                  opacity: 0,
                  child: Container(
                    //margin: EdgeInsets.only( top:160),
                    child: Image.asset(
                      "asset/logopin.png",
                      scale: 4,
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 1,
                  width: MediaQuery.of(context).size.width * 1,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("asset/start.png"),
                          fit: BoxFit.cover)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        //margin: EdgeInsets.only( top:160),
                        child: Image.asset(
                          "asset/logo_app.png",
                          scale: 3,
                        ),
                      ),
                      Container(
                        //margin: EdgeInsets.only(left:85),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
//                            Image.asset(
//                              "asset/pin_logo.png",
//                              scale: 5,
//                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .035,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * .01,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * .025,
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
