

import 'package:http/http.dart' as http ;
import 'package:pin/model/response/get_user_info_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';







Future<GetUserInfoResponse> getGetUserInfoResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/info',headers: headers);

  if (response.statusCode == 200){
    return GetUserInfoResponse.fromMap(json.decode(response.body));
  } else {

    GetUserInfoResponse res = GetUserInfoResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}




