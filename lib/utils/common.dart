
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:pin/screens/Dashboard.dart';
import 'package:pin/screens/start.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Common {

  static const String MAIN_API_URL = "https://pin.sweven.net/";
  static const String TOKEN = "TOKEN";
  static const String publish = "publish";
  static const String draft = "draft";
  static const String scheduled_publish = "scheduled_publish";
  static const String scheduled = "scheduled";
  static const String EVENT = "event";
  static const String FILE = "file";
  static const String USER_TOKEN = "USER_TOKEN";
  static const String USER_NAME = "USER_NAME";
  static const String USER_ID = "USER_ID";
  static const String USER_IMAGE = "USER_IMAGE";
  static bool GOOGLE = false;


  static  String USER_ID_ = "0";

  static getHeaders() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String language = _prefs.getString('language') ?? 'ar';

    String getTokengmail = _prefs.getString(USER_TOKEN);
    String getTokengmais = _prefs.getString(USER_ID);
    getTokengmail == null ? getTokengmail = "" : getTokengmail =
        _prefs.getString(USER_TOKEN);
print(getTokengmail);
    var tokenData = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
//      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzcyNmI2M2JlYjk0Yjk0OTdmMWU1MThiMmI3NjJjZWJlYWFkNjgyYzE5YWU0YzM2ZmM5M2VjZTBhYTFhZWJjNmZiYWM5MTEzMDI1NTNlOGYiLCJpYXQiOjE1ODk5NjExMzMsIm5iZiI6MTU4OTk2MTEzMywiZXhwIjoxNjIxNDk3MTMzLCJzdWIiOiI0NSIsInNjb3BlcyI6W119.iyJRqtAjOEJM4TBQqP8lvsrd3vzCJeABPvEhgSZyMlklC9qkKxzZfYlHFTQF3EYb7mcyXVR3rZt8GopmZznj7mx-gxySqyGyNqPDiWv6IrjM7WgYJoWyN94IWv41eEJVjEeXtZesgePkeGJVqGm_uPc8045A-6EKk6Vjp0bB3P8Xib-ZqOuw0m4megZx2hToSLrCrvnyupXglh-6s2ktaIxTjcr1FJcuKXsrWQsGjJOjlgEKxv9Mz12lTAC_M25uJ42Ixjbmlsa58yxWuhQMSJRERbrjsV7twPcskUc4FxZKT1BnzxBS7wqrJSoDjJb8nsYvJYeKmTGQAyPf3_-CASCSG2v0owBhvau0l_6fPehZNhWYCZSnmfJUFSonfntDp4VFoZ6SlZQoH3DV8-1lmPUAY0L_0f5GwKL_rfEQ57O2m1-iKYoxddoEH03yxS7GZQ6Cy28qD7uzY0TGKPmQVz1Gv_bQX130IS7hHK2dvDY-apfMYaXeqJvNLSBBhRY_qdrUcEbwNG6bWfQDuYy3ANt8V9yqfkfvc-oCM6jzX7vvn0pa4SOOF25XteRH-ib72RWc2-XUWvcaXKF9FogJRSLLP2z_xUiB2uQlW33_fZsoJzK8hnh7wpxwRb6V7jIFIWb2Oqzc03aL86jwqk2FtZ3MuvUlBuh2IWDNyoeg2tM',
      'Authorization': "Bearer " + getTokengmail,
      'locale': language
    };
    return tokenData;
  }


  static getToken() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String token = _prefs.getString(TOKEN) ?? '';
    return token;
  }

  static getTokengmail() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String getTokengmail = _prefs.getString(USER_TOKEN);
    return getTokengmail;
  }

  static Future<int> updatePosition(double _position1_latitude,
      double _position1_longtude, double _position2_latitude,
      double _position2_longtude) async {
    final double distance = await Geolocator().distanceBetween(
        _position1_latitude, _position1_longtude, _position2_latitude,
        _position2_longtude);
    return (distance / 1000).toInt();
  }

  static Future<void> CheckUser(BuildContext context) async {
    SharedPreferences Tokengmail = await SharedPreferences.getInstance();
    String ss = Tokengmail.getString(Common.USER_ID);
    USER_ID_ = ss;
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) =>
        Tokengmail.containsKey(Common.USER_TOKEN)
            ? Dashboard()
            : Start()));
  }

}
