import 'dart:io';

import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loadmore/loadmore.dart';
import 'package:pin/API/get_user_info_api.dart';
import 'package:pin/constents/menuitem.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/screens/pindetail.dart';
import 'package:pin/utils/common.dart';
import 'package:pin/widgets/post_tile.dart';
import 'package:pin/widgets/post_tile2.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../API/get_my_friends_api.dart';
import '../API/get_user_interests_api.dart';
import '../API/update_mage_api.dart';
import 'Dashboard.dart';
import 'comments.dart';
import 'customdialog.dart';
import '../dialogboxs/spam.dart';
import '../dialogboxs/viewpin.dart';
import 'follower.dart';
import 'following.dart';
import '../language/translation_strings.dart';
import 'interest_edit.dart';
import 'likes.dart';
import '../model/body/update_cover_body.dart';
import '../model/body/update_image_body.dart';
import '../model/response/get_my_friends_response.dart';
import '../model/response/get_user_info_response.dart';
import '../model/response/get_user_interests_response.dart';
import '../model/response/update_image_response.dart';
import 'myaccount.dart';
import 'myprofilefriend.dart';

class Myprofile extends StatefulWidget {

  static GlobalKey<AsyncLoaderState> getUserInterestss =
  new GlobalKey<AsyncLoaderState>();
  @override
  _MyprofileState createState() => _MyprofileState();
}

final GlobalKey<AsyncLoaderState> _asyncLoaderStateGetUserInfo =
new GlobalKey<AsyncLoaderState>();
final GlobalKey<AsyncLoaderState> _asyncLoaderStateGetMyFriends =
new GlobalKey<AsyncLoaderState>();
final GlobalKey<AsyncLoaderState> _asyncLoaderStatePosts =
new GlobalKey<AsyncLoaderState>();

class _MyprofileState extends State<Myprofile> {
  File _updateImage;
  File _updateCoverImage;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _updateImage = image;
    });
    _sendUpdateImage();
  }
  Future getCoverImage() async {
    var image2 = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _updateCoverImage = image2;
    });
    _sendUpdateCoverImage();
  }

  var selected = [];
  String imageUrl;
  @override
  Widget build(BuildContext context) {
    var _asyncLoaderPosts = new AsyncLoader(
      key: _asyncLoaderStatePosts,
      initState: () async => await getEventsResponse("1"),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => getNoConnectionWidget4(),
      renderSuccess: ({data}) => getListView(data),
    );
    var _asyncLoadergetuserData = new AsyncLoader(
      key: _asyncLoaderStateGetUserInfo,
      initState: () async => await getGetUserInfoResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget2(),
      renderSuccess: ({data}) => setWidegtgetuserData(data),
    );
    var _asyncLoader = new AsyncLoader(
      key: Myprofile.getUserInterestss,
      initState: () async => await getGetUserInterestsResponse(),
      renderLoad: () =>  Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    var _asyncLoaderGetMyFriends = new AsyncLoader(
      key: _asyncLoaderStateGetMyFriends,
      initState: () async => await getMyFriendsResponse(),
      renderLoad: () =>  Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget3(),
      renderSuccess: ({data}) => setWidegtDataGetMyFriends(data),
    );
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2.2;
    final double itemWidth = size.width / 2;
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .3,
                  ),
                  Text(
                    Translations.of(context).MyProfile,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: SmartRefresher(
        enablePullDown: true,
        header: ClassicHeader(
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child:
        SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
              Container(
                //color: Colors.yellowAccent,
                margin: EdgeInsets.only(left: 10, right: 10, top: 10),
//              color: Colors.blue,
                width: MediaQuery.of(context).size.width,
                //    height: MediaQuery.of(context).size.width * .6,
                child: _asyncLoadergetuserData
              ),
              Container(
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.width * .221,
                child: _asyncLoader
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 15, top: 10),
                  child: Text(
                    Translations.of(context).Friends,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'quicksand'),
                  )),
              Container(
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.width * .260,
                child: _asyncLoaderGetMyFriends

              ),
//              Container(
//                  alignment: Alignment.centerLeft,
//                  margin: EdgeInsets.only(left: 15, top: 10),
//                  child: Text(
//                    "Upcoming Events",
//                    style: TextStyle(
//                        fontSize: 16,
//                        fontWeight: FontWeight.bold,
//                        fontFamily: 'quicksand'),
//                  )),
//              Container(
//                  alignment: Alignment.center,
//                  height: MediaQuery.of(context).size.width * .33,
//                  child: ListView.builder(
//                    scrollDirection: Axis.horizontal,
//                    itemCount: 6,
//                    itemBuilder: (context, index) {
//                      return Container(
//                        child: _events(index),
//                      );
//                    },
//                  )),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 15, top: 10),
                  child: Text(
                    Translations.of(context).MyEvents,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'quicksand'),
                  )),
                _asyncLoaderPosts,
              SizedBox(
                height: MediaQuery.of(context).size.height * .04,
              ),
            ],
          ),
        ),
      ),)
    );
  }
//swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo.currentState.reloadState();
    _asyncLoaderStateGetMyFriends.currentState.reloadState();
    Myprofile.getUserInterestss.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
  void _onLoading() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo.currentState.reloadState();
    _asyncLoaderStateGetMyFriends.currentState.reloadState();
    Myprofile.getUserInterestss.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  Widget _intereest(InterestsBean model) {
    return GestureDetector(
//      onTap: () {
//        setState(() {
//          selected.contains(id) ? selected.remove(id) : selected.add(id);
//        });
//      },
      child: Container(
        margin: EdgeInsets.only(
          left: 10,
          top: 5,
        ),
        alignment: Alignment.bottomCenter,
        width: MediaQuery.of(context).size.width * .16,
        height: MediaQuery.of(context).size.height * .05,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              width: 1,
//              color: selected.contains(id) ? Color(0xff086EBA) : Colors.grey
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 30,
              height: 30,
              alignment: Alignment.center,
              decoration: BoxDecoration(
//                color: selected.contains(id) ? Color(0xff086EBA) : Colors.grey,
                shape: BoxShape.circle,
              ),
              child: Icon(
                Icons.done_all,
//                color: Colors.white,
                  color: Color(0xff59A0CE)
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              model.interest.title,
//              id == 0
//                  ? "Marriage"
//                  : id == 1 ? "Jobs" : id == 2 ? "Friends" : "Jobs",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            )
          ],
        ),
      ),
    );
  }

  Widget _friends(FriendsBean model6) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 10),
      alignment: Alignment.bottomCenter,
      // width: MediaQuery.of(context).size.width * .217,
      //height: MediaQuery.of(context).size.height * .11,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(width: 1, color: Color(0xffBCBCBC).withOpacity(.64)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            //width: 40,
            //height: 40,
            decoration: BoxDecoration(
                color: Color(0xff2C4752),
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 3,
                  )
                ]),
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Myprofilefriend(model6)),
                );
              },
              child: Container(
                padding: EdgeInsets.all(1),
                child: CircleAvatar(
                  backgroundImage:
                  NetworkImage(model6.image),
//                  radius: 2,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            model6.name,
            style: TextStyle(
                color: Color(0xff2C4752),
                fontSize: 10,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          )
        ],
      ),
    );
  }

  Widget _events(int id) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 10),
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width * .3,
      //   height: MediaQuery.of(context).size.height * .15,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
            image: AssetImage(id == 0
                ? "asset/logo.png"
                : id == 1 ? "asset/logo.png" : "asset/logo.png"),
            fit: BoxFit.cover),
        color: Colors.black,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            id == 0 ? "SPORTS" : id == 1 ? "FRIENDS" : "BRLINE",
            style: TextStyle(
                fontSize: 18, fontFamily: 'quicksand', color: Colors.white),
          ),
          Text("17 DEC",
              style: TextStyle(
                  fontSize: 12, fontFamily: 'quicksand', color: Colors.white))
        ],
      ),
    );
  }


  Widget _social(int id,DataBean22 model) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            id == 1
                ? print("pin")
                : id == 2
                    ? showDialog(
                        context: context,
                        builder: (BuildContext context) => Follower())
                    : showDialog(
                        context: context,
                        builder: (BuildContext context) => Following());
          },
          child: Container(
            width: MediaQuery.of(context).size.width * .09,
            height: MediaQuery.of(context).size.width * .09,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5,
                  )
                ]),
            child: Text(
              id == 1 ? model.user.eventsCount : id == 2 ? model.user.followersCount : model.user.followingsCount,
              style: TextStyle(fontSize: 12, color: Color(0xff59A0CE)),
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          id == 1 ? Translations.of(context).Events : id == 2 ? Translations.of(context).Followers : Translations.of(context).Following,
          style: TextStyle(
              color: Colors.black, fontSize: 8, fontWeight: FontWeight.bold),
        )
      ],
    );
  }



  Widget getNoConnectionWidget() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          SizedBox(
//            height: 90.0,
//            child: new Container(
//              decoration: new BoxDecoration(
//                image: new DecorationImage(
//                  image: new AssetImage('assets/wifi.png'),
//                  fit: BoxFit.contain,
//                ),
//              ),
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => Myprofile.getUserInterestss.currentState.reloadState())
        ],
      ),
    );
  }
  Widget getNoConnectionWidget2() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          SizedBox(
//            height: 90.0,
//            child: new Container(
//              decoration: new BoxDecoration(
//                image: new DecorationImage(
//                  image: new AssetImage('assets/wifi.png'),
//                  fit: BoxFit.contain,
//                ),
//              ),
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderStateGetUserInfo.currentState.reloadState())
        ],
      ),
    );
  }
  Widget getNoConnectionWidget3() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderStateGetMyFriends.currentState.reloadState())
        ],
      ),
    );
  }
  Widget getNoConnectionWidget4() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderStatePosts.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtData(GetUserInterestsResponse data) {
    return
      data.data.interests.length<=0?Container(
          child: Text(
            Translations.of(context).Therearenointerestsyet,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          )
      ):
    GridView.builder(
        itemCount: data.data.interests.length,
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.0,
            crossAxisSpacing: 10,
            mainAxisSpacing: 12,
            crossAxisCount: 1),
        itemBuilder: (context, index) {
          return _intereest(data.data.interests[index]);
        });
  }
  setWidegtDataGetMyFriends(GetMyFriendsResponse data6) {
    return data6.data.friends.length<=0?Container(
        child: Text(
          Translations.of(context).Younothavefriendsyet,
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontFamily: 'quicksand'),
        )
    ):
      GridView.builder(
        itemCount: data6.data.friends.length,
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.0,
            crossAxisSpacing: 10,
            mainAxisSpacing: 12,
            crossAxisCount: 1),
        itemBuilder: (context, index) {
          return _friends(data6.data.friends[index]);
        });
  }

  setWidegtgetuserData(GetUserInfoResponse data11) {
    imageUrl = data11.data.user.image;
    storeImage(data11);
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.width * .40,
          // height: MediaQuery.of(context).size.height * 0.23,
          margin: EdgeInsets.only(bottom: 100),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
              image: DecorationImage(image: NetworkImage(data11.data.user.cover),
                  fit: BoxFit.cover)
          ),
        ),
        GestureDetector(
          onTap: () {
            getCoverImage();
          },
          child: Container(
              width: MediaQuery.of(context).size.width,
              // height: MediaQuery.of(context).size.height * 0.05,
              alignment: Alignment.centerRight,
              margin:
              EdgeInsets.only(right: 10, bottom: 20, top: 10),
              child: Image.asset(
                "asset/camra.png",
                scale: 4,
              )),
        ),
        Container(
          margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.width * .30,
              left: 20),
          child: Row(
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.bottomCenter,
                      width:
                      MediaQuery.of(context).size.width * .17,
                      height:
                      MediaQuery.of(context).size.width * .17,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                            width: 2, color: Colors.white),
                          image: DecorationImage(image: NetworkImage(data11.data.user.image),
                              fit: BoxFit.cover)
                      ),
                      child: Stack(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
//
                              getImage();

                            },
                            child: Container(
                              margin: EdgeInsets.only(left: 40),
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                border: Border.all(
                                    width: 2, color: Colors.white),
                                image: DecorationImage(
                                    image: AssetImage(
                                        "asset/camra.png")),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * .04,
              ),
              Container(
                width: 100,
                margin: EdgeInsets.only(bottom: 20),
                child: Text(
                  data11.data.user.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                      color: Colors.white,
                      fontFamily: 'quicksand'),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * .03,
                    top: MediaQuery.of(context).size.width * .055),
                child: Row(
                  children: <Widget>[
                    _social(1,data11.data),
                    SizedBox(
                      width: 5,
                    ),
                    _social(2,data11.data),
                    SizedBox(
                      width: 5,
                    ),
                    _social(3,data11.data),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
                left: 5,
                top: MediaQuery.of(context).size.width * .58),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  Translations.of(context).Interests,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'quicksand'),
                ),
                InkWell(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => InterestEdit()),
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      Translations.of(context).Edit,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                      color: Colors.blue),
                    ),
                  ),
                ),
              ],
            )),
      ],
    );
  }

  Future storeImage(GetUserInfoResponse data ) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString(Common.USER_IMAGE, data.data.user.image);
  }

   _sendUpdateImage() async {
    UpdateImageBody body = UpdateImageBody();
    body.image = _updateImage;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    UpdateImageResponse result;
    try {
      result = await sendUpdateImageResponse(body);
//      await storeImage(result);

      _asyncLoaderStateGetUserInfo.currentState.reloadState();

      Navigator.pop(context);

      Fluttertoast.showToast(
          msg: Translations.of(context).Changeddone,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
  _sendUpdateCoverImage() async {
    UpdateCoverBody body = UpdateCoverBody();
    body.cover = _updateCoverImage;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    UpdateImageResponse result;
    try {
      result = await sendUpdateImageCover(body);
//      await storeUser(result);

      _asyncLoaderStateGetUserInfo.currentState.reloadState();

      Navigator.pop(context);

      Fluttertoast.showToast(
          msg: Translations.of(context).Changeddone,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }


  EventsResponse response;

  Future<bool> _loadMore() async {
    print("onLoadMore");
    try {
      page = response.data.events.currentPage + 1;
    } catch (E) {
      page = page + 1;
    }
    response = await getEventsResponse((page).toString());
//    _onRefresh();
    print("load");
    setState(() {
//      itemss.addAll(response.data.events.data);
      print("data count = ${itemss.length}");
    });

    return true;
  }

  List<DataEvent> itemss = [];

  List<TextEditingController> commentControllerList = [];
  static int page = 1;
  int index = 0;

  final ScrollController listScrollController = new ScrollController();
  int get count => itemss.length;

  Widget getListView(EventsResponse data) {
    if (response == null) {
      page = 1;
      itemss.clear();
      commentControllerList.clear();
      response = data;
    }
    if (page == 1) {
      if (itemss.length != commentControllerList.length)
        commentControllerList.clear();
      itemss.clear();
//      itemss.addAll(data.data.events.data);
      response = data;
    }
    itemss.addAll(response.data.events.data);
    if (itemss.length != commentControllerList.length)
      for (int i = 0; i < response.data.events.data.length; i++) {
        commentControllerList.add(new TextEditingController());
      }
    return LoadMore(
      isFinish: response.data.events.to >= data.data.events.total,
      onLoadMore: _loadMore,
      child: new ListView.builder(
        shrinkWrap: true,
        itemCount: itemss.length,
        reverse: false,
        controller: listScrollController,
        itemBuilder: (context, index) => PostTile2(index, itemss[index],
            commentControllerList[index], _asyncLoaderStatePosts,null),
      ),
      whenEmptyLoad: false,
      delegate: DefaultLoadMoreDelegate(),
      textBuilder: DefaultLoadMoreTextBuilder.english,
    );
  }

}
