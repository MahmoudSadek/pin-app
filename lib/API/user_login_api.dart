

import 'package:http/http.dart' as http ;
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/body/user_login_body.dart';
import 'package:pin/model/response/error_response.dart';
import 'package:pin/model/response/user_login_response.dart';
import 'package:pin/utils/common.dart';

Future<UserLoginResponse> getUserLoginResponsebyfacebook (UserLoginBody userLoginBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(userLoginBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/social/facebook',body: body,headers: headers);

  if (response.statusCode == 200){
    return UserLoginResponse.fromJson(json.decode(response.body));
  } else {

    ErrorResponse res = ErrorResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}


Future<UserLoginResponse> getUserLoginResponsebygoogle (UserLoginBody userLoginBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(userLoginBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/social/google',body: body,headers: headers);

  if (response.statusCode == 200){
    return UserLoginResponse.fromJson(json.decode(response.body));
  } else {

    ErrorResponse res = ErrorResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}
