import 'dart:io';

import 'package:async_loader/async_loader.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loadmore/loadmore.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/body/message_body.dart';
import 'package:pin/model/response/chat_thread_response.dart';
import 'package:pin/model/response/messages_response.dart';
import 'package:pin/repository/chat_api.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Perchat extends StatefulWidget {
  Threads item;
  Perchat(this.item);

  @override
  _PerchatState createState() => _PerchatState(item);
}

class _PerchatState extends State<Perchat> {
  File _image;
  Threads item;

  bool isLoading = false;
  bool isLoadingData = false;
  bool blockFirebase = false;
  TextEditingController _ControlerMessage=new TextEditingController();
  _PerchatState(this.item);
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sendSeenChatResponse(item.withUser.id.toString());

    getData();
  }
  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getMessagesResponse(page.toString(), item.withUser.id),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => getListView(data),
    );
    return WillPopScope(
      child: Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(horizontal: 14),
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                          alignment: Alignment.centerRight,
                          child: Icon(Icons.arrow_back_ios)),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Container(
                        alignment: Alignment.center,
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(1),
                              child: CircleAvatar(
                                backgroundImage: NetworkImage(item.withUser.image),
                                radius: 15,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              item.withUser.name,
                              style: TextStyle(fontSize: 18),
                            )
                          ],
                        )),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        alignment: Alignment.centerRight,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(70.0)),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              reverse: true,
              child: Column(
                children: <Widget>[
                  StreamBuilder<DocumentSnapshot>(
                    stream: Firestore.instance.collection("conversations").document("${item.conversationId}").snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) return LinearProgressIndicator();
//            _asyncLoaderState.currentState.reloadState();\

                      if(!blockFirebase)
//                    if(snapshot.data.data!=null)
                        getData();
//                    else  Fluttertoast.showToast(
//                        msg:Translations.of(context).enter,
//                        toastLength: Toast.LENGTH_SHORT,
//                        gravity: ToastGravity.CENTER,
//                        timeInSecForIosWeb: 1,
//                        backgroundColor: Colors.black,
//                        textColor: Colors.amber,
//                        fontSize: 16.0);

                      return Container(
                        height: MediaQuery.of(context).size.height * .78,
                        child: getListViewWidget(),
                      );
                    },
                  ),
//                _asyncLoader,
//                Container(
//                  height: MediaQuery.of(context).size.height * .78,
//                  child: getListViewWidget(),
//                ),
//                _asyncLoader,

                  // Loading
                  Container(

                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.width * .18,
                  )
                ],
              ),
            ),
            buildInput(),
          ],
        ),
        /*SmartRefresher(
          enablePullDown: true,
          header: ClassicHeader(
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child:  Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * .75,
                child: _asyncLoader,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width * .14,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Color(0xff0072BB).withOpacity(.16), blurRadius: 2)
                ]),
                child: Row(
                  children: <Widget>[
                    Expanded(
//                       flex: 1,
//
//                      child: Center(
//                        child: Center(
//                          child: _image == null
//                              ? Text('No image selected.')
//                              : Image.file(_image),
//                        ),
////                          floatingActionButton: FloatingActionButton(
////                            onPressed: getImage,
////                            tooltip: 'Pick Image',
////                            child: Icon(Icons.add_a_photo),
////                          ),
//                      ),
                      child: GestureDetector(
                        onTap: () {
                          //  Image.file(_image);
                          getImage();
                        },
                        child: Container(
                          child: Image.asset(
                            "asset/chatcamra.png",
                            scale: 2.5,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      flex: 5,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 4,
                              child: TextField(
                                controller: _ControlerMessage,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Type a message.',
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    sendMessage();
                                  });
                                },
                                child: Image.asset(
                                  "asset/sent.png",
                                  scale: 3,
                                  color: Colors.blue,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),*/
      ),
      onWillPop: ()  {

         sendSeenChatResponse(item.withUser.id.toString());

        return new Future(() => true);
      },
    );
  }
  Widget buildLoading() {
//    isLoading = true;
    return Positioned(
      child:
        isLoading
          ? Container(
        height: 20,
        width: 20,
        child: Center(
          child: CircularProgressIndicator(),
        ),
        color: Colors.white.withOpacity(0.8),
      )
          : Container(),
    );
  }

  Widget _receive1(int id, Messages messag) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(left: 80, right: 10),
              decoration: BoxDecoration(
                  color: Color(0xffF3F3F3),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                      topLeft: Radius.circular(15))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(15),
                    child: Text(
                      messag.message,
                      style: TextStyle(fontSize: 13),
                    ),
                  ),
                  messag.isSeen!="0"?
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    alignment: Alignment.centerRight,
                    child: Icon(
                      Icons.done_all,
                      color: Colors.blue,
                    ),
                  ):Container()
                ],
              ))
        ],
      ),
    );
  }


  Widget _my1(int id, Messages messag) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 10),
      child: Row(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * .09,
            alignment: Alignment.bottomCenter,
            padding: EdgeInsets.all(1),
            child: Container(
              child: CircleAvatar(
                backgroundImage:  NetworkImage(item.withUser.image),
//                  radius: 2,
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    topLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15)),
                boxShadow: [BoxShadow(color: Colors.grey)]),
            child: Container(
              width: MediaQuery.of(context).size.width * .7,
              padding: EdgeInsets.all(10),
              child: Text(
                messag.message,
                style: TextStyle(fontSize: 13),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _my2(int id) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 3),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width * .133,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15),
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                ),
                boxShadow: [BoxShadow(color: Colors.grey)]),
            child: Container(
              width: MediaQuery.of(context).size.width * .7,
              padding: EdgeInsets.all(10),
              child: Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                style: TextStyle(fontSize: 13),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _deliver() {
    return Container(
      margin: EdgeInsets.only(right: 10, top: 5),
      alignment: Alignment.centerRight,
      child: Text(
        "Delivered",
        style: TextStyle(
            color: Color(0xffB0AFB8),
            fontFamily: 'proba',
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _receive2(int id) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(left: 80, right: 10),
              decoration: BoxDecoration(
                  color: Color(0xffF3F3F3),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                      topLeft: Radius.circular(15))),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                      style: TextStyle(fontSize: 13),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    alignment: Alignment.centerRight,
                    child: Icon(
                      Icons.done_all,
                      color: Colors.blue,
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }


  Future<void> sendMessage() async {
    if (_ControlerMessage.text.isEmpty) {
      return;
    }
//
//    ProgressDialog pr = new ProgressDialog(context);
//    pr = new ProgressDialog(context,
//        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
//    await pr.show();
    setState(() {
      isLoading = true;
    });
    try{
      MessageBody body = MessageBody();
      body.chatId = item.withUser.id;
      body.message = _ControlerMessage.text;
      _ControlerMessage.text= "";
      var reponse = await sendChatResponse(body);

      setState(() {
        isLoading = false;
      });
      getData();
//      pr.hide();
    }catch(e){
//      pr.hide();
      setState(() {
        isLoading = false;
      });
    }
  }


//swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();

  void _onRefresh() async {
    // monitor network fetch
    page=20;
    _asyncLoaderState.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }  void _onLoading() async {
    // monitor network fetch
    _asyncLoaderState.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }
  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderState.currentState.reloadState())
        ],
      ),
    );
  }
  List<Messages> itemss = [];
  Widget getListView(MessagesResponse data) {
    setState(() {
      if(page==20) {
        itemss.clear();
        itemss.addAll( data.data.messages);
      }
    });
//    await Future.delayed(Duration(seconds:  10));
    if(blockFirebase)
      blockFirebase = false;
    return Container();
  }
  Widget getListViewWidget() {
//    if(page==20) {
//      itemss.clear();
//      itemss.addAll( data.data.messages);
//    }
    return Stack(children:[
    itemss.length<=0?Container(
        child: Center(
          child: isLoadingData?Center(child: new CircularProgressIndicator()):Text(
            Translations.of(context).NoChatyet,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          ),
        )
    ):LoadMore(
      isFinish:  itemss.length < page,
      onLoadMore: _loadMore,
      child: new ListView.builder(
//        shrinkWrap: true,
//        physics: NeverScrollableScrollPhysics(),
        itemCount:  itemss.length,
        controller: listScrollController,
        reverse: true,
        itemBuilder: (context, index) => _ChatItem(index,itemss[index]),
      ),
      whenEmptyLoad: false,
      delegate: DefaultLoadMoreDelegate(),
      textBuilder: DefaultLoadMoreTextBuilder.english,
    ),Positioned(
        bottom: 0,
    left: 200,
    right: 200,
    child:
      isLoadingData
          ? Container(
        width: 20,
        height: 20,
        child: Center(
          child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),),
        ),
        color: Colors.white.withOpacity(0.8),
      ):
      Container()),]);
  }
   int page = 20;
  final ScrollController listScrollController = new ScrollController();
  Future<bool> _loadMore() async {
    print("onLoadMore");
    page = page + 20;
    MessagesResponse response =  await getMessagesResponse(page.toString(), item.withUser.id);
    itemss.clear();
    itemss.addAll(response.data.messages);
//    _onRefresh();
    print("load");
    setState(() {
//      itemss.addAll(response.data.events.data);
      print("data count = ${itemss.length}");
    });

    return true;
  }

  Future<void> getData()  async {
    blockFirebase = true;
    isLoadingData= true;

    print("onLoadMore");
    MessagesResponse response =  await getMessagesResponse(page.toString(), item.withUser.id);
    print("blockFirebase = $blockFirebase");
    setState(() {
      itemss.clear();
      itemss.addAll(response.data.messages);
      print("data count = ${itemss.length}");

      isLoadingData= false;
    });

    await Future.delayed(Duration(seconds:  2));
    if(blockFirebase)
      blockFirebase = false;
  }
  /*
  Widget getListView(MessagesResponse data) {
    return data.data.messages.length<=0?Container(
        child: Center(
          child: Text(
            Translations.of(context).NoChatyet,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          ),
        )
    ):ListView.builder(
      shrinkWrap: true,
      itemCount: data.data.messages.length,
      reverse: false,
      itemBuilder: (context, index) => _ChatItem(index,data.data.messages[index]),
    );
  }*/
//  _receive2(1),
//  _deliver(),


  _ChatItem(int index, Messages messag) {
    return
      messag.senderId!=item.withUser.id.toString()?
      _receive1(index,messag):_my1(index,messag);
  }

  buildInput() {
    return Positioned(
      bottom: 0,
      child: Column(
        children: [
          buildLoading(),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.width * .14,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  color: Color(0xff0072BB).withOpacity(.16), blurRadius: 2)
            ]),
            child: Row(
              children: <Widget>[
                Expanded(
//                       flex: 1,
//
//                      child: Center(
//                        child: Center(
//                          child: _image == null
//                              ? Text('No image selected.')
//                              : Image.file(_image),
//                        ),
////                          floatingActionButton: FloatingActionButton(
////                            onPressed: getImage,
////                            tooltip: 'Pick Image',
////                            child: Icon(Icons.add_a_photo),
////                          ),
//                      ),
                  child: GestureDetector(
                    onTap: () {
                      //  Image.file(_image);
                      getImage();
                    },
                    child: Container(
                      child: Image.asset(
                        "asset/chatcamra.png",
                        scale: 2.5,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 5,
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 4,
                          child: TextField(
                            controller: _ControlerMessage,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Type a message.',
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                sendMessage();
                              });
                            },
                            child: Image.asset(
                              "asset/sent.png",
                              scale: 3,
                              color: Colors.blue,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }


}
