import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/map/map.dart';
import 'package:pin/map/map_location.dart';
import 'package:pin/screens/pindetail.dart';

import 'addpin.dart';


class ViewMapMark extends StatefulWidget {
  LatLng currentLatLng ;
  ViewMapMark(this.currentLatLng);

  @override
  _ViewMapMarkState createState() => _ViewMapMarkState(currentLatLng);
}

class _ViewMapMarkState extends State<ViewMapMark> {
  var black = TextStyle(color: Colors.black);
  Completer<GoogleMapController> _controller = Completer();
  static final CameraPosition lahore = CameraPosition(
    target: LatLng(31.5204, 74.3587),
    zoom: 14.4746,
  );
  LatLng currentLatLng ;
  _ViewMapMarkState(this.currentLatLng);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(horizontal: 14),
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      Translations.of(context).AddLocation,
                      style: TextStyle(
                          fontFamily: 'trebuchat',
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(60.0)),


      body: Column(
          children: <Widget>[MapLocationWidegt(currentLatLng),

            GestureDetector(
              onTap: () {
                Navigator.pop(
                  context
                );
              },
              child: Container(
                height: MediaQuery.of(context).size.width * .12,
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * .09,
                    right: MediaQuery.of(context).size.width * .09,
                    top: MediaQuery.of(context).size.height * .02),
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height*.07,

                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Color(0xff59A0CE)),
                child: Text(
                  "Done",
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'quicksand',
                      color: Colors.white),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * .02,
            )
          ],
      )/*GoogleMap(
        mapType: MapType.terrain,
        initialCameraPosition: lahore,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),*/
    );
  }

  /*appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image(
                    height: MediaQuery.of(context).size.height * 0.03,
                    image: AssetImage("asset/direction.png"),
                  ),
                  Text(
                    "900 m",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ],
              ),
              Text(" 30 street , Maddie ",
                  style: TextStyle(fontSize: 22, color: Colors.black)),
              Image(
                height: MediaQuery.of(context).size.height * 0.05,
                image: AssetImage(
                  "asset/mic.png",
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        //margin: EdgeInsets.only(bottom: 40),
        height: MediaQuery.of(context).size.height * 0.08,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          boxShadow: [
            BoxShadow(
              offset: Offset(0.00, 0.00),
              color: Color(0xff000000).withOpacity(0.34),
              blurRadius: 6,
            ),
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(14.00),
            topRight: Radius.circular(14.00),
          ),
        ),
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
//                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Pindetail()));
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 15),
                      padding: EdgeInsets.all(10),
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 1, color: Colors.black)),
                      child: Image.asset(
                        "asset/cross.png",
                        color: Color(0xff949494),
                      ),
                    ),
                  ),
                  Container(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "3 min",
                          style:
                              TextStyle(fontSize: 20, color: Color(0xff0A7506)),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "4.3 KM",
                              style: TextStyle(
                                  fontSize: 11, color: Color(0xff404040)),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "12:19 am",
                              style: TextStyle(
                                  fontSize: 11, color: Color(0xff404040)),
                            ),
                          ],
                        ),
                      )
                    ],
                  )),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    padding: EdgeInsets.all(10),
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(width: 1, color: Colors.black)),
                    child: Image.asset(
                      "asset/direction.png",
                      color: Colors.black,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    padding: EdgeInsets.all(10),
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                        color: Color(0xff086EBA),
                        shape: BoxShape.circle,
                        border: Border.all(width: 1, color: Color(0xff086EBA))),
                    child: Image.asset(
                      "asset/updic.png",
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),*/
}
