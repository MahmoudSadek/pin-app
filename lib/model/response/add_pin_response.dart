class AddPinResponse {
  bool status;
  String msg;
  Data data;

  AddPinResponse({this.status, this.msg, this.data});

  AddPinResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Created created;

  Data({this.created});

  Data.fromJson(Map<String, dynamic> json) {
    created =
    json['created'] != null ? new Created.fromJson(json['created']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.created != null) {
      data['created'] = this.created.toJson();
    }
    return data;
  }
}

class Created {
  String name;
  String feelingId;
  String description;
  String startDate;
  String endDate;
  String startTime;
  String endTime;
  String lat;
  String lon;
  String type;
  int userId;
  String updatedAt;
  String createdAt;
  int id;
  bool isFavorited;

  Created(
      {this.name,
        this.feelingId,
        this.description,
        this.startDate,
        this.endDate,
        this.startTime,
        this.endTime,
        this.lat,
        this.lon,
        this.type,
        this.userId,
        this.updatedAt,
        this.createdAt,
        this.id,
        this.isFavorited});

  Created.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    feelingId = json['feeling_id'];
    description = json['description'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    lat = json['lat'];
    lon = json['lon'];
    type = json['type'];
    userId = json['user_id'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
    isFavorited = json['isFavorited'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['feeling_id'] = this.feelingId;
    data['description'] = this.description;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    data['type'] = this.type;
    data['user_id'] = this.userId;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    data['isFavorited'] = this.isFavorited;
    return data;
  }
}
