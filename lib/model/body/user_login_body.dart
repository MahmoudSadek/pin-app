

class UserLoginBody {
  String providerId;
  String name;
  String email;
  String phone;
  String image;
  String gcm;

  static UserLoginBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserLoginBody userLoginBodyBean = UserLoginBody();
    userLoginBodyBean.providerId = map['provider_id'];
    userLoginBodyBean.name = map['name'];
    userLoginBodyBean.email = map['email'];
    userLoginBodyBean.phone = map['phone'];
    userLoginBodyBean.image = map['image'];
    userLoginBodyBean.gcm = map['gcm'];
    return userLoginBodyBean;
  }

  Map toJson() => {
    "provider_id": providerId,
    "name": name,
    "email": email,
    "phone": phone,
    "image": image,
    "gcm": gcm,
  };
}