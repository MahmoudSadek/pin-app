
class SendAFriendRequestBody {
  int friendId;

  static SendAFriendRequestBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SendAFriendRequestBody sendAFriendRequestBodyBean = SendAFriendRequestBody();
    sendAFriendRequestBodyBean.friendId = map['friend_id'];
    return sendAFriendRequestBodyBean;
  }

  Map toJson() => {
    "friend_id": friendId,
  };
}