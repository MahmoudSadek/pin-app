class Consts {
  Consts._();

  static const double padding = 30.0;
  static const double avatarRadius = 56.0;
}
