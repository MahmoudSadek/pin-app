
class UserLoginResponse {
  bool status;
  String msg;
  Data data;

  UserLoginResponse({this.status, this.msg, this.data});

  UserLoginResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  User user;

  Data({this.user});

  Data.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  int id;
  String name;
  String email;
  String phone;
  String gender;
  String image;
  String cover;
  String aboutMe;
  String activities;
  String hobbies;
  String education;
  String work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  String deletedAt;
  String interestsCount;
  String createdAt;
  String updatedAt;
  String token;

  User(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.gender,
        this.image,
        this.cover,
        this.aboutMe,
        this.activities,
        this.hobbies,
        this.education,
        this.work,
        this.notification,
        this.eventsPublic,
        this.friendsPublic,
        this.gcm,
        this.deletedAt,
        this.interestsCount,
        this.createdAt,
        this.updatedAt,
        this.token});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    gender = json['gender'];
    image = json['image'];
    cover = json['cover'];
    aboutMe = json['about_me'];
    activities = json['activities'];
    hobbies = json['hobbies'];
    education = json['education'];
    work = json['work'];
    notification = json['notification'];
    eventsPublic = json['events_public'];
    friendsPublic = json['friends_public'];
    gcm = json['gcm'];
    deletedAt = json['deleted_at'];
    interestsCount = json['interests_count'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['image'] = this.image;
    data['cover'] = this.cover;
    data['about_me'] = this.aboutMe;
    data['activities'] = this.activities;
    data['hobbies'] = this.hobbies;
    data['education'] = this.education;
    data['work'] = this.work;
    data['notification'] = this.notification;
    data['events_public'] = this.eventsPublic;
    data['friends_public'] = this.friendsPublic;
    data['gcm'] = this.gcm;
    data['deleted_at'] = this.deletedAt;
    data['interests_count'] = this.interestsCount;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['token'] = this.token;
    return data;
  }
}










