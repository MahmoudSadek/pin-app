import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

String gmailName;
String gmailEmail;
String gmailImageUrl;
String gmailPhoneNumber;
String gmailUserId;

Future<String> signInWithGoogle() async {
  try {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    // Checking if email and name is null
    assert(user.email != null);
    assert(user.displayName != null);
    assert(user.photoUrl != null);
    assert(user.phoneNumber == null);

    gmailName = user.displayName;
    gmailEmail = user.email;
    gmailImageUrl = user.photoUrl;
    gmailPhoneNumber = user.phoneNumber;

//TODO   for get ,,,,,,,user_id,,,,,,,,,,
    FirebaseAuth.instance.currentUser().then((user) {
      if (user != null) {
        user.getIdToken().then((token) {
          Map<dynamic, dynamic> tokenMap = token.claims;
//        tokenId = tokenMap['sub'];
          gmailUserId = tokenMap['user_id'];
//        print(tokenMap['sub']);
          print(tokenMap['user_id']);
        });
      }
    });
    // Only taking the first part of the name, i.e., First Name
    if (gmailName.contains(" ")) {
      gmailName = gmailName.substring(0, gmailName.indexOf(" "));
    }

    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return 'signInWithGoogle succeeded: $user';
  }catch(e){
    print(e.message);
  }

}

void signOutGoogle() async {
  await googleSignIn.signOut();

  print("User Sign Out");
}

