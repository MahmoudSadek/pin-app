import 'package:async_loader/async_loader.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/constents/context.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/body/add_comment_body.dart';
import 'package:pin/model/response/event_details_response.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/utils/common.dart';
import 'package:pin/widgets/dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:video_player/video_player.dart';

import '../screens/comments_new.dart';
import '../screens/customdialog.dart';
import '../screens/likes_new.dart';

class Viewpin extends StatefulWidget {
  DataEvent item;

  Viewpin(this.item);

  @override
  _ViewpinState createState() => _ViewpinState(item);
}

class _ViewpinState extends State<Viewpin> {
  int dotIndex = 0;
  DataEvent item;
  TextEditingController commentController = new TextEditingController();

  List<VideoPlayerController> _controllerList=[];
  List<ChewieController> chewieControllerList=[];
  _ViewpinState(this.item);


  @override
  void initState() {
    super.initState();
    video();
  }
  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getEventDtailsResponse(item.id.toString()),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => Container(),
      renderSuccess: ({data}) => getView(data),
    );
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              alignment: Alignment.topCenter,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      bottom: 10,
                    ),
                    margin: EdgeInsets.only(top: Consts.avatarRadius),
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(7),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10.0,
                          offset: const Offset(0.0, 10.0),
                        ),
                      ],
                    ),
                    child: _asyncLoader,
                  ),

                  Positioned(
                    top: MediaQuery.of(context).size.height * .015,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        height: MediaQuery.of(context).size.height * .14,
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 3,
                                    spreadRadius: 2)
                              ]),
                          width: MediaQuery.of(context).size.width * .07,
                          height: MediaQuery.of(context).size.width * .07,
                          child: Image.asset(
                            "asset/cross.png",
                            scale: 3,
                          ),
                        ),
                      ),
                    ),
                  ),

                  //...bottom card part,
                  //...top circlular image part,
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget _social(int id, DataEvent item) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: () {
            id == 1
                ? showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(item),
            )
                : id == 2
                ? showDialog(
                context: context,
                builder: (BuildContext context) =>
                    CommentsNew(item.files[dotIndex].comments, item.id, Common.FILE, indexFile: dotIndex ,))
                :favoriteEvent(item.id);
          },
          child: Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5,
                  )
                ]),
            child: Image.asset(
              id == 1
                  ? "asset/share.png"
                  : id == 2 ? "asset/comment.png" : "asset/heart.png",
              scale: 4,
              color: id==3&&item.files[dotIndex].isFavorited?Colors.red:Colors.grey,
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        InkWell(
          onTap: () {
            id == 1
                ? showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(item),
            )
                : id == 2
                ? showDialog(
                context: context,
                builder: (BuildContext context) =>
                    CommentsNew(item.files[dotIndex].comments, item.id, Common.FILE, indexFile: dotIndex ,))
                : showDialog(
                context: context,
                builder: (BuildContext context) =>
                    LikesNew(item.files[dotIndex].favorites));
          },
          child: Text(
            id == 3
                ? item.files[dotIndex].favorites.length.toString()
                : id == 2 ? item.files[dotIndex].comments.length.toString() : "",
            style: TextStyle(
                color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  Future<void> favoriteEvent(int id) async {

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      var result = await addFavoriteEventResponse(item.files[dotIndex].id.toString(), Common.FILE, item.files[dotIndex].isFavorited?"unfavorite":"favorite");
      _asyncLoaderState.currentState.reloadState();
      pr.hide();
//      asyncLoaderState.currentState.reloadState();
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
  Future<void> commentEvent() async {
    if (commentController.text.isEmpty) {
      return;
    }
    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
    try {
      AddCommnetBody body = AddCommnetBody();
      body.comment = commentController.text;
      var result = await addCommentEventResponse(
          item.files[dotIndex].id.toString(), Common.FILE, body);
      _asyncLoaderState.currentState.reloadState();
      commentController.text = "";
      pr.hide();
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
//          msg:  Translations.of(context).text("error"),
          msg: Translations.of(context).error,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
  @override
  void dispose() {
    for(int i=0;i<item.files.length;i++){
      if(item.files[i].mime.contains("video")) {
        _controllerList[i].dispose();
      }}
    super.dispose();
  }
 /* void video() {
    try{
    for(int i=0;i<item.files.length;i++){
      _controllerList.add(VideoPlayerController.network(item.files[1].name));
      if(item.files[i].mime.contains("video")){
        _controllerList[i] = VideoPlayerController.network(item.files[i].name,
        );
        _controllerList[i].addListener(() {
          setState(() {});
        });
        _controllerList[i].setLooping(true);
        _controllerList[i].initialize().then((_) => setState(() {}));
        _controllerList[i].pause();
      }

    }

    }catch(e){

    }
  }*/

  void video() {
    try {
      for (int i = 0; i < item.files.length; i++) {
        _controllerList.add(VideoPlayerController.network(item.files[1].name));
        chewieControllerList.add(ChewieController(
          videoPlayerController: _controllerList[i],
          aspectRatio: 3 / 2,
          autoPlay: true,
          looping: true,
        ));
        if (item.files[i].mime.contains("video")) {
          _controllerList[i] = VideoPlayerController.network(item.files[i].name,
          );
          _controllerList[i].addListener(() {
            setState(() {});
          });
          _controllerList[i].setLooping(false);
          _controllerList[i].initialize().then((_) => setState(() {}));
          _controllerList[i].pause();

           chewieControllerList[i] = ChewieController(
            videoPlayerController: _controllerList[i],
            aspectRatio: 3 / 2,
            autoPlay: true,
            looping: true,
          );
        }
      }/*for (int i = 0; i < item.files.length; i++) {
        _controllerList.add(VideoPlayerController.network(item.files[1].name));
        if (item.files[i].mime.contains("video")) {
          _controllerList[i] = VideoPlayerController.network(item.files[i].name,
          );
          _controllerList[i].addListener(() {
            setState(() {});
          });
          _controllerList[i].setLooping(false);
          _controllerList[i].initialize().then((_) => setState(() {}));
          _controllerList[i].pause();
        }
      }*/
    }catch(e){

    }

  }

  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();

  getView(EventDetailsResponse data) {
    item = data.data.event;
    return Column(
      mainAxisSize: MainAxisSize.min, // To make the card compact
      children: <Widget>[
        Container(
            width: MediaQuery.of(context).size.width,
            height: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                image: DecorationImage(
                    image: AssetImage("asset/edd.png"),
                    fit: BoxFit.cover)),
            child: Container(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * .7,
                  right: 10,
                  top: 18,
                  bottom: 18),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.00),
                  border: Border.all(
                    width: 1.00,
                    color: Color(0xff77189f),
                  ),
                ),
                child: Text(
                  "Order Now",
                  style: TextStyle(
                      fontSize: 12, color: Color(0xff77189F)),
                ),
              ),
            )),
        SizedBox(
          height: 40,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.center,
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Stack(
            // alignment: AlignmentDirectional.bottomCenter,
              children: [
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: GestureDetector(
                        onTap: () {
                          print("pin");
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: 5, bottom: 30),
                          width:
                          MediaQuery.of(context).size.width,
                          height: 150,
                          decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.circular(10),
                          ),
                          child: Stack(
                            children: <Widget>[
                              new Swiper(
                                itemBuilder:
                                    (BuildContext context,
                                    int index) {
                                  return Container(
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: NetworkImage(item
                                              .files[index].name),
                                          fit: BoxFit.fill,
                                        ),
                                        borderRadius:
                                        BorderRadius.circular(
                                            10)),
                                    child: item.files[index]
                                        .mime.contains(
                                        "video")
                                        ? AspectRatio(
                                      aspectRatio: 2.0,
                                      child: Stack(
                                        alignment: Alignment.bottomCenter,
                                        children: <Widget>[
                                          VideoPlayer(_controllerList[index]),
                                          Chewie(
                                            controller: chewieControllerList[index],
                                          )
//                                          _PlayPauseOverlay(controller: _controllerList[index], item: item,),
//                                          VideoProgressIndicator(_controllerList[index], allowScrubbing: true),
                                        ],
                                      ),
                                    )
                                        : Container(),
                                  );
                                },
                                itemCount: item.files.length,
                                pagination: new SwiperPagination(
                                    builder:
                                    SwiperPagination.rect),
                                control: new SwiperControl(
                                    iconNext: null,
                                    iconPrevious: null),
                                onIndexChanged: (index) {
                                  setState(() {
                                    dotIndex = index;
                                  });
                                },
                                onTap: (pic) {
                                  showDialog(
                                      context: context,
                                      builder: (context) =>
                                          showImageDialog(item
                                              .files[dotIndex]));
                                },
                              ),
//                                            Container(
//                                              decoration: BoxDecoration(
//                                                  image: DecorationImage(
//                                                    image: AssetImage(
//                                                        "asset/img31.png"),
//                                                    fit: BoxFit.cover,
//                                                  ),
//                                                  borderRadius:
//                                                      BorderRadius.circular(
//                                                          10)),
//                                            ),
                              Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        item.name,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight:
                                            FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            item.address_name.contains("\n")?
                                            item.address_name.substring(0,item.address_name.indexOf("\n"))
                                                :"",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight:
                                                FontWeight.bold,
                                                color: Colors.white),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Image.asset(
                                                "asset/pinlocation.png",
                                                scale: 2,
                                              ),
                                              Text(
                                                item.address_name.contains("\n")?
                                                item.address_name.substring(item.address_name.indexOf("\n")+1):
                                                item.address_name,
                                                style: TextStyle(
                                                  color: Colors.white,
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                  ],
                ),
                Positioned(
                  top: 155,
                  child: Container(
                    height: 20,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: item.files.length,
                          itemBuilder: (context, index) =>
                              Container(
                                margin: EdgeInsets.only(left: 4),
                                width: dotIndex == index ? 15 : 10,
                                decoration: BoxDecoration(
                                    color: Colors.blue,
                                    shape: BoxShape.circle),
                              )),
                    ),
                  ),
                ),
                Positioned(
                  top: 135,
                  child: Container(
                    margin: EdgeInsets.only(
                      left:
                      MediaQuery.of(context).size.width * .67,
                    ),
                    child: Row(
                      children: <Widget>[
                        _social(1, item),
                        SizedBox(
                          width: 5,
                        ),
                        _social( 2, item),
                        SizedBox(
                          width: 5,
                        ),
                        _social( 3, item),
                      ],
                    ),
                  ),
                ),
              ]),
        ),
        SizedBox(height: 20),
        Container(
          margin: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * .025,
              right: MediaQuery.of(context).size.width * .025,
              bottom: MediaQuery.of(context).size.height * .018),
          width: MediaQuery.of(context).size.width,
          //height: MediaQuery.of(context).size.height * .065,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  height: MediaQuery.of(context).size.width * .12,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border:
                    Border.all(width: 1, color: Colors.grey),
                  ),
                  child: Image.asset(
                    "asset/mask.png",
                    scale: 3,
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 6,
                child: Container(
                  height: MediaQuery.of(context).size.width * .12,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border:
                    Border.all(width: 1, color: Colors.grey),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 6,
                        child: TextField(
                          controller: commentController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: Translations.of(context)
                                .Typeacomment,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              commentEvent();
                            });
                          },
                          child: Image.asset(
                            "asset/sent.png",
                            color: Colors.blue,
                            scale: 4,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );

  }

}


class _PlayPauseOverlay extends StatelessWidget {
  _PlayPauseOverlay({Key key, this.controller, this.item}) : super(key: key);

  final VideoPlayerController controller;
  DataEvent item;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
            color: Colors.black26,
            child: Center(
              child: Icon(
                Icons.play_arrow,
                color: Colors.white,
                size: 100.0,
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();

          },
        ),
      ],
    );
  }


}
