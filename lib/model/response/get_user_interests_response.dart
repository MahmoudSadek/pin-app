

class GetUserInterestsResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static GetUserInterestsResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GetUserInterestsResponse getUserInterestsResponseBean = GetUserInterestsResponse();
    getUserInterestsResponseBean.status = map['status'];
    getUserInterestsResponseBean.msg = map['msg'];
    getUserInterestsResponseBean.data = DataBean.fromMap(map['data']);
    getUserInterestsResponseBean.errors = map['errors'];
    return getUserInterestsResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  List<InterestsBean> interests;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.interests = List()..addAll(
      (map['interests'] as List ?? []).map((o) => InterestsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "interests": interests,
  };
}


class InterestsBean {
  int id;
  String userId;
  String interestId;
  dynamic createdAt;
  dynamic updatedAt;
  InterestBean interest;
  List<dynamic> locations;

  static InterestsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    InterestsBean interestsBean = InterestsBean();
    interestsBean.id = map['id'];
    interestsBean.userId = map['user_id'];
    interestsBean.interestId = map['interest_id'];
    interestsBean.createdAt = map['created_at'];
    interestsBean.updatedAt = map['updated_at'];
    interestsBean.interest = InterestBean.fromMap(map['interest']);
    interestsBean.locations = map['locations'];
    return interestsBean;
  }

  Map toJson() => {
    "id": id,
    "user_id": userId,
    "interest_id": interestId,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "interest": interest,
    "locations": locations,
  };
}


class InterestBean {
  int id;
  String createdAt;
  String updatedAt;
  String title;
  String image;

  static InterestBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    InterestBean interestBean = InterestBean();
    interestBean.id = map['id'];
    interestBean.createdAt = map['created_at'];
    interestBean.updatedAt = map['updated_at'];
    interestBean.title = map['title'];
    interestBean.image = map['image'];
    return interestBean;
  }

  Map toJson() => {
    "id": id,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "title": title,
    "image": image,
  };
}