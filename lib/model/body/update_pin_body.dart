
import 'dart:io';

class UpdatePinBody {
  String method;
  String name;
  int feelingId;
  String description;
  String startDate;
  String endDate;
  String startTime;
  String endTime;
  String lat;
  String lon;
  String type;
  List<String> categories;
  List<String> friends;
  List<File> images;
  List<File> videos;

  UpdatePinBody(
      {this.method,
      this.name,
        this.feelingId,
        this.description,
        this.startDate,
        this.endDate,
        this.startTime,
        this.endTime,
        this.lat,
        this.lon,
        this.type,
        this.categories,
        this.friends,
        this.images,
        this.videos,
      });

  UpdatePinBody.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    method = json['_method'];
    feelingId = json['feeling_id'];
    description = json['description'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    lat = json['lat'];
    lon = json['lon'];
    type = json['type'];
    categories = json['categories'];
    friends = json['friends'];
    images = json['images'];
    videos = json['videos'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['_method'] = this.method;
    data['feeling_id'] = this.feelingId;
    data['description'] = this.description;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    data['type'] = this.type;
    data['categories'] = this.categories;
    data['friends'] = this.friends;
    data['images'] = this.images;
    data['videos'] = this.videos;
    return data;
  }
}
