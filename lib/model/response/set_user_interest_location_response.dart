

class SetUserInterestLocationResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static SetUserInterestLocationResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SetUserInterestLocationResponse setUserInterestLocationResponseBean = SetUserInterestLocationResponse();
    setUserInterestLocationResponseBean.status = map['status'];
    setUserInterestLocationResponseBean.msg = map['msg'];
    setUserInterestLocationResponseBean.data = DataBean.fromMap(map['data']);
    setUserInterestLocationResponseBean.errors = map['errors'];
    return setUserInterestLocationResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  List<LocationsBean> locations;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.locations = List()..addAll(
      (map['locations'] as List ?? []).map((o) => LocationsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "locations": locations,
  };
}


class LocationsBean {
  double lat;
  double lon;
  int km;
  int userInterestId;
  String updatedAt;
  String createdAt;
  int id;

  static LocationsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LocationsBean locationsBean = LocationsBean();
    locationsBean.lat = map['lat'];
    locationsBean.lon = map['lon'];
    locationsBean.km = map['km'];
    locationsBean.userInterestId = map['user_interest_id'];
    locationsBean.updatedAt = map['updated_at'];
    locationsBean.createdAt = map['created_at'];
    locationsBean.id = map['id'];
    return locationsBean;
  }

  Map toJson() => {
    "lat": lat,
    "lon": lon,
    "km": km,
    "user_interest_id": userInterestId,
    "updated_at": updatedAt,
    "created_at": createdAt,
    "id": id,
  };
}