

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/send_a_follow_request_body.dart';
import 'package:pin/model/response/error_response.dart';
import 'package:pin/model/response/send_a_follow_request_response.dart';

import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';





Future<SendAFollowRequestResponse> sendSendAFollowRequestResponse (SendAFollowRequestBody sendAFollowRequestBody) async {

  var headers= await Common.getHeaders();

  var body = json.encode(sendAFollowRequestBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/follower/follow',body: body,headers: headers);

  if (response.statusCode == 200){
    return SendAFollowRequestResponse.fromMap(json.decode(response.body));
  } else {

    ErrorResponse res = ErrorResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}




