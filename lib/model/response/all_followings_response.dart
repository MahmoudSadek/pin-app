

class AllFollowingsResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static AllFollowingsResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AllFollowingsResponse allFollowingsResponseBean = AllFollowingsResponse();
    allFollowingsResponseBean.status = map['status'];
    allFollowingsResponseBean.msg = map['msg'];
    allFollowingsResponseBean.data = DataBean.fromMap(map['data']);
    allFollowingsResponseBean.errors = map['errors'];
    return allFollowingsResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  List<FollowingsBean> followings;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.followings = List()..addAll(
      (map['followings'] as List ?? []).map((o) => FollowingsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "followings": followings,
  };
}


class FollowingsBean {
  int id;
  String name;
  String email;
  String phone;
  dynamic gender;
  String image;
  String cover;
  dynamic aboutMe;
  dynamic activities;
  dynamic hobbies;
  dynamic education;
  dynamic work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  String createdAt;
  String updatedAt;
  PivotBean pivot;

  static FollowingsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FollowingsBean followingsBean = FollowingsBean();
    followingsBean.id = map['id'];
    followingsBean.name = map['name'];
    followingsBean.email = map['email'];
    followingsBean.phone = map['phone'];
    followingsBean.gender = map['gender'];
    followingsBean.image = map['image'];
    followingsBean.cover = map['cover'];
    followingsBean.aboutMe = map['about_me'];
    followingsBean.activities = map['activities'];
    followingsBean.hobbies = map['hobbies'];
    followingsBean.education = map['education'];
    followingsBean.work = map['work'];
    followingsBean.notification = map['notification'];
    followingsBean.eventsPublic = map['events_public'];
    followingsBean.friendsPublic = map['friends_public'];
    followingsBean.gcm = map['gcm'];
    followingsBean.deletedAt = map['deleted_at'];
    followingsBean.createdAt = map['created_at'];
    followingsBean.updatedAt = map['updated_at'];
    followingsBean.pivot = PivotBean.fromMap(map['pivot']);
    return followingsBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "pivot": pivot,
  };
}



class PivotBean {
  String followerId;
  String followingId;
  String acceptedAt;
  String createdAt;
  String updatedAt;

  static PivotBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PivotBean pivotBean = PivotBean();
    pivotBean.followerId = map['follower_id'];
    pivotBean.followingId = map['following_id'];
    pivotBean.acceptedAt = map['accepted_at'];
    pivotBean.createdAt = map['created_at'];
    pivotBean.updatedAt = map['updated_at'];
    return pivotBean;
  }

  Map toJson() => {
    "follower_id": followerId,
    "following_id": followingId,
    "accepted_at": acceptedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}