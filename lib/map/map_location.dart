

import 'dart:async';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class MapLocationWidegt extends StatefulWidget {
  LatLng currentLatLng ;
  MapLocationWidegt(this.currentLatLng);

  @override
  _MapLocationWidegtState createState() => _MapLocationWidegtState(currentLatLng);
}

class _MapLocationWidegtState extends State<MapLocationWidegt> {
  LatLng currentLatLng ;
  Completer<GoogleMapController> _controller = Completer();
  CameraPosition _kGooglePlex ;

  List<Marker>mMarker=[];

  _MapLocationWidegtState(this.currentLatLng);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _kGooglePlex =  CameraPosition(
      target: currentLatLng,
      zoom: 14.4746,
    );
    mMarker.add(
        Marker(
          markerId: MarkerId('1'),
          position: currentLatLng,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height-200,

        child: GoogleMap(gestureRecognizers: Set()
          ..add(
              Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
          ..add(
            Factory<VerticalDragGestureRecognizer>(
                    () => VerticalDragGestureRecognizer()),
          )
          ..add(
            Factory<HorizontalDragGestureRecognizer>(
                    () => HorizontalDragGestureRecognizer()),
          )
          ..add(
            Factory<ScaleGestureRecognizer>(
                    () => ScaleGestureRecognizer()),
          ),
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
          zoomGesturesEnabled: true,
          scrollGesturesEnabled: true,
          mapType: MapType.normal,
          initialCameraPosition: _kGooglePlex,
          markers: mMarker.toSet(),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),
      ),
    );
  }

}





