import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:pin/screens/signup.dart';
import 'package:pin/utils/common.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';


class Start extends StatefulWidget {
  Start({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _StartState createState() => _StartState();
}

class _StartState extends State<Start> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    super.initState();
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@drawable/ic_notification');

    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);

    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
//        _showItemDialog(message);
        displayNotification(message);
      },
//      onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
//        _navigateToItemDetail(message);
        displayNotification(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        displayNotification(message);
//        _navigateToItemDetail(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(alert: true, badge: true, sound: true));
    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      print(token);
      SharedPreferences pref = await SharedPreferences.getInstance();
      pref.setString(Common.TOKEN, token);

    });
  }

  Future displayNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'channelid', 'flutterfcm', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      message['data']['message'].toString(),
      message['notification']['body'],
      platformChannelSpecifics,
      payload: message['data']['message'],
    );
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
  }

  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();

            },
          ),
        ],
      ),
    );
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  var selected = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 1,
          width: MediaQuery.of(context).size.width * 1,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("asset/start.png"), fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                //margin: EdgeInsets.only( top:160),
                child: Image.asset(
                  "asset/logo_app.png",
                  scale: 3,
                ),
              ),
              Container(
                //margin: EdgeInsets.only(left:85),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
//                    Image.asset(
//                      "asset/pin_logo.png",
//                      scale: 5,
//                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .035,
                    ),
                    Image.asset(
                      "asset/language.png",
                      scale: 5,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * .01,
              ),
              _button(1),
              SizedBox(
                height: MediaQuery.of(context).size.height * .025,
              ),
              _button(2),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _button(int id) {
    return ScopedModelDescendant<AppModel>(
        builder: (context, child, model) => MaterialButton(
              onPressed: () {
                model.changeDirection(id == 1 ? "en" : "ar");
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Signup()));
              },
//                    height: 60.0,
//                    color: const Color.fromRGBO(119, 31, 17, 1.0),
              child:
                  /*GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Signup()));
      },
      child:*/
                  Container(
                //  margin: EdgeInsets.only(left:70),

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: id == 1 ? Colors.white : Color(0xffB3C7DC)),
                height: MediaQuery.of(context).size.height * .07,
                width: MediaQuery.of(context).size.height * .22,

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      id == 1 ? "English" : "العربية",
                      style: TextStyle(fontSize: 18),
                    ),
                    id == 1
                        ? Icon(Icons.check_circle, color: Colors.blue)
                        : Icon(Icons.check_circle, color: Colors.grey)
                  ],
                ),
              ),
            ));
  }


}
