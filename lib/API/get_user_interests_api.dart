

import 'package:http/http.dart' as http ;
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/response/get_user_interests_response.dart';
import 'package:pin/utils/common.dart';





Future<GetUserInterestsResponse> getGetUserInterestsResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/interests',headers: headers);

  if (response.statusCode == 200){
    return GetUserInterestsResponse.fromMap(json.decode(response.body));
  } else {

    GetUserInterestsResponse res = GetUserInterestsResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}






Future<GetUserInterestsResponse> getGetFriendInterestsResponse (int id) async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/interests/$id',headers: headers);

  if (response.statusCode == 200){
    return GetUserInterestsResponse.fromMap(json.decode(response.body));
  } else {

    GetUserInterestsResponse res = GetUserInterestsResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}

