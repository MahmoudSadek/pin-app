// To parse this JSON data, do
//
//     final pendingFriendsResponse = pendingFriendsResponseFromJson(jsonString);

import 'dart:convert';

PendingFriendsResponse pendingFriendsResponseFromJson(String str) => PendingFriendsResponse.fromJson(json.decode(str));

String pendingFriendsResponseToJson(PendingFriendsResponse data) => json.encode(data.toJson());

class PendingFriendsResponse {
  bool status;
  String msg;
  Data5 data;
  List<dynamic> errors;

  PendingFriendsResponse({
    this.status,
    this.msg,
    this.data,
    this.errors,
  });

  factory PendingFriendsResponse.fromJson(Map<String, dynamic> json) => PendingFriendsResponse(
    status: json["status"],
    msg: json["msg"],
    data: Data5.fromJson(json["data"]),
    errors: List<dynamic>.from(json["errors"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "msg": msg,
    "data": data.toJson(),
    "errors": List<dynamic>.from(errors.map((x) => x)),
  };
}

class Data5 {
  List<Friend> friends;

  Data5({
    this.friends,
  });

  factory Data5.fromJson(Map<String, dynamic> json) => Data5(
    friends: List<Friend>.from(json["friends"].map((x) => Friend.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "friends": List<dynamic>.from(friends.map((x) => x.toJson())),
  };
}

class Friend {
  int id;
  String senderType;
  String senderId;
  String recipientType;
  String recipientId;
  String status;
  DateTime createdAt;
  DateTime updatedAt;
  Recipient sender;
  Recipient recipient;

  Friend({
    this.id,
    this.senderType,
    this.senderId,
    this.recipientType,
    this.recipientId,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.sender,
    this.recipient,
  });

  factory Friend.fromJson(Map<String, dynamic> json) => Friend(
    id: json["id"],
    senderType: json["sender_type"],
    senderId: json["sender_id"],
    recipientType: json["recipient_type"],
    recipientId: json["recipient_id"],
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    sender: Recipient.fromJson(json["sender"]),
    recipient: Recipient.fromJson(json["recipient"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "sender_type": senderType,
    "sender_id": senderId,
    "recipient_type": recipientType,
    "recipient_id": recipientId,
    "status": status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "sender": sender.toJson(),
    "recipient": recipient.toJson(),
  };
}

class Recipient {
  int id;
  String name;
  String email;
  String phone;
  dynamic gender;
  String image;
  dynamic cover;
  dynamic aboutMe;
  dynamic activities;
  dynamic hobbies;
  dynamic education;
  dynamic work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  DateTime createdAt;
  DateTime updatedAt;

  Recipient({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.gender,
    this.image,
    this.cover,
    this.aboutMe,
    this.activities,
    this.hobbies,
    this.education,
    this.work,
    this.notification,
    this.eventsPublic,
    this.friendsPublic,
    this.gcm,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  factory Recipient.fromJson(Map<String, dynamic> json) => Recipient(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"] == null ? null : json["phone"],
    gender: json["gender"],
    image: json["image"],
    cover: json["cover"],
    aboutMe: json["about_me"],
    activities: json["activities"],
    hobbies: json["hobbies"],
    education: json["education"],
    work: json["work"],
    notification: json["notification"],
    eventsPublic: json["events_public"],
    friendsPublic: json["friends_public"],
    gcm: json["gcm"],
    deletedAt: json["deleted_at"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone == null ? null : phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
