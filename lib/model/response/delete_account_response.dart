

class DeleteAccountResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static DeleteAccountResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DeleteAccountResponse deleteAccountResponseBean = DeleteAccountResponse();
    deleteAccountResponseBean.status = map['status'];
    deleteAccountResponseBean.msg = map['msg'];
    deleteAccountResponseBean.data = DataBean.fromMap(map['data']);
    deleteAccountResponseBean.errors = map['errors'];
    return deleteAccountResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  bool deleted;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.deleted = map['deleted'];
    return dataBean;
  }

  Map toJson() => {
    "deleted": deleted,
  };
}