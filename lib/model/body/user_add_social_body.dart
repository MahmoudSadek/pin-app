
class UserAddSocialBody {
  String providerId;

  static UserAddSocialBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserAddSocialBody userAddSocialBodyBean = UserAddSocialBody();
    userAddSocialBodyBean.providerId = map['provider_id'];
    return userAddSocialBodyBean;
  }

  Map toJson() => {
    "provider_id": providerId,
  };
}