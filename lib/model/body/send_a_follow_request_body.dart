
class SendAFollowRequestBody {
  String followedId;

  static SendAFollowRequestBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SendAFollowRequestBody sendAFollowRequestBodyBean = SendAFollowRequestBody();
    sendAFollowRequestBodyBean.followedId = map['followed_id'];
    return sendAFollowRequestBodyBean;
  }

  Map toJson() => {
    "followed_id": followedId,
  };
}