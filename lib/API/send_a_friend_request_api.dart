

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/send_a_friend_request_body.dart';
import 'package:pin/model/response/send_a_friend_request_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';







Future<SendAFriendRequestResponse> sendSendAFriendRequestResponse (SendAFriendRequestBody sendAFriendRequestBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(sendAFriendRequestBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/friend/befriend',body: body,headers: headers);

  if (response.statusCode == 200){
    return SendAFriendRequestResponse.fromMap(json.decode(response.body));
  } else {

    SendAFriendRequestResponse res = SendAFriendRequestResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}




