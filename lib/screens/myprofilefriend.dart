import 'package:async_loader/async_loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loadmore/loadmore.dart';
import 'package:pin/chat/perchat.dart';
import 'package:pin/constents/menuitem.dart';
import 'package:pin/model/response/chat_thread_response.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/repository/event_api.dart';
import 'package:pin/screens/addpin.dart';
import 'package:pin/widgets/post_tile.dart';
import 'package:pin/widgets/post_tile2.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../API/get_my_friend_info_api.dart';
import '../API/get_my_friends_api.dart';
import '../API/get_user_interests_api.dart';
import '../API/send_a_follow_request_api.dart';
import 'comments.dart';
import 'customdialog.dart';
import '../dialogboxs/spam.dart';
import '../dialogboxs/viewpin.dart';
import 'follower.dart';
import 'following.dart';
import '../language/translation_strings.dart';
import 'likes.dart';
import '../model/body/send_a_follow_request_body.dart';
import '../model/response/get_my_friend_nfo_response.dart';
import '../model/response/get_my_friends_response.dart';
import '../model/response/get_user_interests_response.dart';
import '../model/response/send_a_follow_request_response.dart';
import 'myprofile.dart';

class Myprofilefriend extends StatefulWidget {
  FriendsBean friendsBean;

  Myprofilefriend(this.friendsBean);

  @override
  _MyprofilefriendState createState() => _MyprofilefriendState(friendsBean);
}

class _MyprofilefriendState extends State<Myprofilefriend> {
  var selected = [];
  FriendsBean friendsBean;

  _MyprofilefriendState(this.friendsBean);

  final GlobalKey<AsyncLoaderState> _getMyFrienfProfileinfo =
  new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> _getFriendInterestss =
  new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> _asyncLoaderStateGetMyFriends =
  new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> _asyncLoaderStatePosts =
  new GlobalKey<AsyncLoaderState>();

  @override
  Widget build(BuildContext context) {
    var _asyncLoaderPosts = new AsyncLoader(
      key: _asyncLoaderStatePosts,
      initState: () async => await getUserEventsResponse("1", friendsBean.id),
      renderLoad: () => Container(
          margin: const EdgeInsets.only(top: 50.0),
          child: Center(child: new CircularProgressIndicator())),
      renderError: ([error]) => getNoConnectionWidget4(),
      renderSuccess: ({data}) => getListView(data),
    );

    var _asyncLoadergetMyFrienfProfileinf = new AsyncLoader(
      key: _getMyFrienfProfileinfo,
      initState: () async => await getGetMyFriendNfoResponse(friendsBean.id),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget2(),
      renderSuccess: ({data}) => setWidegtgetuserData(data),
    );
    var _asyncLoaderetfriendnterestss = new AsyncLoader(
      key: _getFriendInterestss,
      initState: () async => await getGetFriendInterestsResponse(friendsBean.id),
      renderLoad: () =>  Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    var _asyncLoaderGetMyFriends = new AsyncLoader(
      key: _asyncLoaderStateGetMyFriends,
      initState: () async => await getMyFriendsinfriendResponse(friendsBean.id),
      renderLoad: () =>  Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget3(),
      renderSuccess: ({data}) => setWidegtDataGetMyFriends(data),
    );
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .3,
                  ),
                  Text(
                    Translations.of(context).Friendprofile,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: SmartRefresher(
        enablePullDown: true,
        header: ClassicHeader(
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child:
        SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, top: 10),
//              color: Colors.blue,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width * 0.7,
                child: _asyncLoadergetMyFrienfProfileinf

              ),
              Container(
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.width * .221,
                child:_asyncLoaderetfriendnterestss

              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 15, top: 10),
                  child: Text(
                    Translations.of(context).Friends,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'quicksand'),
                  )),
              Container(
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.width * .260,
                child: _asyncLoaderGetMyFriends,
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 15, top: 20),
                  child: Text(
                    Translations.of(context).Events,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'quicksand'),
                  )),
              _asyncLoaderPosts,
              SizedBox(
                height: MediaQuery.of(context).size.height * .04,
              ),
            ],
          ),
        ),
      ),)
    );
  }
//swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    _getMyFrienfProfileinfo.currentState.reloadState();
    _getFriendInterestss.currentState.reloadState();
    _asyncLoaderStateGetMyFriends.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }  void _onLoading() async {
    // monitor network fetch
    _getMyFrienfProfileinfo.currentState.reloadState();
    _getFriendInterestss.currentState.reloadState();
    _asyncLoaderStateGetMyFriends.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  Widget _social(int id,DataBean00 model) {
    return Column(
      children: <Widget>[
        GestureDetector(
//          onTap: () {
//            id == 1
//                ? print("pin")
//                : id == 2
//                    ? showDialog(
//                        context: context,
//                        builder: (BuildContext context) => Follower())
//                    : showDialog(
//                        context: context,
//                        builder: (BuildContext context) => Following());
//          },
          child: Container(
            width: MediaQuery.of(context).size.width * .09,
            height: MediaQuery.of(context).size.width * .09,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5,
                  )
                ]),
            child: Text(
              id == 1 ? model.user.eventsCount : id == 2 ? model.user.followersCount : model.user.followingsCount,
              style: TextStyle(fontSize: 12, color: Color(0xff59A0CE)),
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          id == 1 ? Translations.of(context).Events : id == 2 ? Translations.of(context).Followers : Translations.of(context).Following2,
          style: TextStyle(
              color: Colors.white, fontSize: 8, fontWeight: FontWeight.bold),
        )
      ],
    );
  }


  Widget _interest(InterestsBean model) {
    return GestureDetector(
//      onTap: () {
//        setState(() {
//          selected.contains(id) ? selected.remove(id) : selected.add(id);
//        });
//      },
      child: Container(
        margin: EdgeInsets.only(
          left: 10,
          top: 5,
        ),
        alignment: Alignment.bottomCenter,
        width: MediaQuery.of(context).size.width * .16,
        height: MediaQuery.of(context).size.height * .05,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            width: 1,
//              color: selected.contains(id) ? Color(0xff086EBA) : Colors.grey
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 30,
              height: 30,
              alignment: Alignment.center,
              decoration: BoxDecoration(
//                color: selected.contains(id) ? Color(0xff086EBA) : Colors.grey,
                shape: BoxShape.circle,
              ),
              child: Icon(
                  Icons.done_all,
//                color: Colors.white,
                  color: Color(0xff59A0CE)
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              model.interest.title,
//              id == 0
//                  ? "Marriage"
//                  : id == 1 ? "Jobs" : id == 2 ? "Friends" : "Jobs",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            )
          ],
        ),
      ),
    );
  }

  Widget _friends(FriendsBean model6) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 10),
      alignment: Alignment.bottomCenter,
//       width: MediaQuery.of(context).size.width * .217,
//      height: MediaQuery.of(context).size.height * .11,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(width: 1, color: Color(0xffBCBCBC).withOpacity(.64)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
//            width: 40,
//            height: 40,
            decoration: BoxDecoration(
                color: Color(0xff2C4752),
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 3,
                  )
                ]),
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => new  Myprofilefriend(model6)),
                );
              },
              child: Container(
                padding: EdgeInsets.all(1),
                child: CircleAvatar(
                  backgroundImage:
                  NetworkImage(model6.image),
//                  radius: 2,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 5,left: 5),
            child: Text(
              model6.name,
              style: TextStyle(
                  color: Color(0xff2C4752),
                  fontSize: 10,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            ),
          )
        ],
      ),
    );
  }



  Widget getNoConnectionWidget2() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _getMyFrienfProfileinfo.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtgetuserData(GetMyFriendNfoResponse data) {
    return  Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.width * .40,
          margin: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * .126),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            image: DecorationImage(
                image: NetworkImage(data.data.user.cover),
                fit: BoxFit.cover),
          ),
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.05,
            alignment: Alignment.centerRight,
            margin: EdgeInsets.only(right: 10, bottom: 20),
            child: Image.asset(
              "asset/camra.png",
              scale: 4,
            )),
        Container(
          margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.width * .11,
              left: MediaQuery.of(context).size.width * .04),
          child: Row(
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.bottomCenter,
                      width:
                      MediaQuery.of(context).size.width * .17,
                      height:
                      MediaQuery.of(context).size.width * .17,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                            width: 2, color: Colors.white),
                        image: DecorationImage(
                            image: NetworkImage(data.data.user.image),
                            fit: BoxFit.cover),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 40),
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  width: 2, color: Colors.white),
                              image: DecorationImage(
                                  image: AssetImage(
                                      "asset/camra.png")),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * .04,
              ),
              Container(
                width: 70,
                margin: EdgeInsets.only(
                    bottom:
                    MediaQuery.of(context).size.width * .09),
                child: Text(
                  data.data.user.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                      color: Colors.white,
                      fontFamily: 'quicksand'),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * .17,
                    top: MediaQuery.of(context).size.width * .13),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        _social(1,data.data),
                        SizedBox(
                          width: 5,
                        ),
                        _social(2,data.data),
                        SizedBox(
                          width: 5,
                        ),
                        _social(3,data.data),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.width * .22,
                left: MediaQuery.of(context).size.width * .43),
            child: Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width * .22,
                  height: MediaQuery.of(context).size.width * .06,
//                  decoration: BoxDecoration(
//                      color: Color(0xff59A0CE),
//                      borderRadius: BorderRadius.circular(2)),
//                  child: Text(
//                    "Send Request",
//                    style: TextStyle(
//                        fontSize: 10,
//                        fontFamily: 'quicksand',
//                        fontWeight: FontWeight.bold,
//                        color: Colors.white),
//                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                data.data.user.hasRequestedToFollow==true?
                Container(alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width * .13,
                  height: MediaQuery.of(context).size.width * .06,)
                    :
                GestureDetector(
                  onTap: (){_sendAFollowRequestPress();},
                  child: Container(
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width * .13,
                    height: MediaQuery.of(context).size.width * .06,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                          width: 1, color: Color(0xffFFC673)),
                      borderRadius: BorderRadius.circular(2),
                    ),
                    child: Text(
                      Translations.of(context).Follow,
                      style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'quicksand',
                          fontWeight: FontWeight.bold,
                          color: Color(0xffACACAC)),
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: (){
                    Threads threadsItem = Threads();
                    WithUser withUser= WithUser();
                    withUser.id = friendsBean.id;
                    withUser.image = friendsBean.image;
                    withUser.name = friendsBean.name;
                    threadsItem.withUser = withUser;
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => Perchat(threadsItem)));
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width * .13,
                    height: MediaQuery.of(context).size.width * .06,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                          width: 1, color: Color(0xffFFC673)),
                      borderRadius: BorderRadius.circular(2),
                    ),
                    child: Text(
                      Translations.of(context).Chat,
                      style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'quicksand',
                          fontWeight: FontWeight.bold,
                          color: Color(0xffACACAC)),
                    ),
                  ),
                )
              ],
            )),
        Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
                left: 5,
                top: MediaQuery.of(context).size.width * .6),
            child: Text(
              Translations.of(context).Interests,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            )),
      ],
    );
  }
  setWidegtData(GetUserInterestsResponse data) {
    return
      data.data.interests.length<=0?Container(
          child: Text(
            Translations.of(context).Therearenointerestsyet,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          )
      ):
      GridView.builder(
          itemCount: data.data.interests.length,
          scrollDirection: Axis.horizontal,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1.0,
              crossAxisSpacing: 10,
              mainAxisSpacing: 12,
              crossAxisCount: 1),
          itemBuilder: (context, index) {
            return _interest(data.data.interests[index]);
          });
  }
  setWidegtDataGetMyFriends(GetMyFriendsResponse data6) {
    return data6.data.friends.length<=0?Container(
        child: Text(
          Translations.of(context).Therearenofriendsyet,
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontFamily: 'quicksand'),
        )
    ):
    GridView.builder(
        itemCount: data6.data.friends.length,
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.0,
            crossAxisSpacing: 10,
            mainAxisSpacing: 12,
            crossAxisCount: 1),
        itemBuilder: (context, index) {
          return _friends(data6.data.friends[index]);
        });
  }

  Widget getNoConnectionWidget() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          SizedBox(
//            height: 90.0,
//            child: new Container(
//              decoration: new BoxDecoration(
//                image: new DecorationImage(
//                  image: new AssetImage('assets/wifi.png'),
//                  fit: BoxFit.contain,
//                ),
//              ),
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _getFriendInterestss.currentState.reloadState())
        ],
      ),
    );
  }
  Widget getNoConnectionWidget3() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          SizedBox(
//            height: 90.0,
//            child: new Container(
//              decoration: new BoxDecoration(
//                image: new DecorationImage(
//                  image: new AssetImage('assets/wifi.png'),
//                  fit: BoxFit.contain,
//                ),
//              ),
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderStateGetMyFriends.currentState.reloadState())
        ],
      ),
    );
  }
  Widget getNoConnectionWidget4() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderStatePosts.currentState.reloadState())
        ],
      ),
    );
  }
  Future _sendAFollowRequestPress() async {

    SendAFollowRequestBody body = SendAFollowRequestBody();
    body.followedId = friendsBean.id.toString() ;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
//    new Future.delayed(const Duration(seconds: 1), () => "1");
    SendAFollowRequestResponse result;
    try {
      result = await sendSendAFollowRequestResponse(body);
//      ]

      _getMyFrienfProfileinfo.currentState.reloadState();
      Navigator.pop(context);

      Fluttertoast.showToast(
          msg: Translations.of(context).Requestsent,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
          msg: e.msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }


  EventsResponse response;

  Future<bool> _loadMore() async {
    print("onLoadMore");
    try {
      page = response.data.events.currentPage + 1;
    } catch (E) {
      page = page + 1;
    }
    response = await getUserEventsResponse((page).toString(), friendsBean.id);
//    _onRefresh();
    print("load");
    setState(() {
//      itemss.addAll(response.data.events.data);
      print("data count = ${itemss.length}");
    });

    return true;
  }

  List<DataEvent> itemss = [];

  List<TextEditingController> commentControllerList = [];
  static int page = 1;
  int index = 0;

  final ScrollController listScrollController = new ScrollController();
  int get count => itemss.length;

  Widget getListView(EventsResponse data) {
    if (response == null) {
      page = 1;
      itemss.clear();
      commentControllerList.clear();
      response = data;
    }
    if (page == 1) {
      if (itemss.length != commentControllerList.length)
        commentControllerList.clear();
      itemss.clear();
//      itemss.addAll(data.data.events.data);
      response = data;
    }
    itemss.addAll(response.data.events.data);
    if (itemss.length != commentControllerList.length)
      for (int i = 0; i < response.data.events.data.length; i++) {
        commentControllerList.add(new TextEditingController());
      }
    return LoadMore(
      isFinish: response.data.events.to >= data.data.events.total,
      onLoadMore: _loadMore,
      child: new ListView.builder(
        shrinkWrap: true,
        itemCount: itemss.length,
        reverse: false,
        controller: listScrollController,
        itemBuilder: (context, index) => PostTile2(index, itemss[index],
            commentControllerList[index], _asyncLoaderStatePosts,null),
      ),
      whenEmptyLoad: false,
      delegate: DefaultLoadMoreDelegate(),
      textBuilder: DefaultLoadMoreTextBuilder.english,
    );
  }

}
