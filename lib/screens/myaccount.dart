import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/dialogboxs/logout.dart';
import 'package:pin/utils/common.dart';
import 'package:pin/widgets/custom_switch.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../API/get_user_info_api.dart';
import '../API/update_info_user_api.dart';
import 'friendrequist.dart';
import '../language/translation_strings.dart';
import '../model/body/update_info_user_body.dart';
import '../model/response/get_user_info_response.dart';
import '../model/response/update_info_user_response.dart';
import 'myinformation.dart';
import 'myprofile.dart';

class Myaccout extends StatefulWidget {
  @override
  _MyaccoutState createState() => _MyaccoutState();
}

class _MyaccoutState extends State<Myaccout> {
  var notiisChecked = false;
  var showeventisChecked = false;
  var myeventisChecked = false;
  var myfriendisChecked = false;




  // TODO: for Switch press

  void initState() {

    super.initState();

    _getuserinfo();
    getSwitchValues();
  }
  getSwitchValues() async {
    notiisChecked = await genotiisCheckedState();
    myeventisChecked = await getmyeventisChecked();
    myfriendisChecked = await getmyfriendisChecked();
    setState(() {});
  }

  Future<bool> savenotiisCheckedState(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("notiisChecked", value);
    return prefs.setBool("notiisChecked", value);
  }
  Future<bool> savemyeventisChecked(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("myeventisChecked", value);
    return prefs.setBool("myeventisChecked", value);
  }
  Future<bool> savemyfriendisChecked(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("myfriendisChecked", value);
    return prefs.setBool("myfriendisChecked", value);
  }

  Future<bool> genotiisCheckedState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isSwitchedFT = prefs.getBool("notiisChecked");
    return isSwitchedFT;
  }
  Future<bool> getmyeventisChecked() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isSwitchedFT = prefs.getBool("myeventisChecked");
    return isSwitchedFT;
  }
  Future<bool> getmyfriendisChecked() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isSwitchedFT = prefs.getBool("myfriendisChecked");
    return isSwitchedFT;
  }




  String name;
  String image;
  @override
  _getuserinfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString(Common.USER_NAME)??"";
      image = prefs.getString(Common.USER_IMAGE)??"";

    });
  }

  final GlobalKey<AsyncLoaderState> _asyncLoaderStateGetUserInfo =
  new GlobalKey<AsyncLoaderState>();
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderStateGetUserInfo,
      initState: () async => await getGetUserInfoResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    Translations.of(context).MyAccount,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'trebuchat'),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body:         SmartRefresher(
    enablePullDown: true,
    header: ClassicHeader(
    ),
    controller: _refreshController,
    onRefresh: _onRefresh,
    onLoading: _onLoading,
    child:
      SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Myprofile()),
                  );
                },
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10, top: 15),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width * .2,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 3,
                        )
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
//
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(left: 30, top: 14),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              image==null?
                              CircleAvatar(
                                backgroundImage:
                                    ExactAssetImage(""),
                                radius: 25,
                              ):
                              CircleAvatar(
                                backgroundImage:
                                NetworkImage(image),
                                radius: 25,
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(left: 15, top: 5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      name==null?"":name,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontFamily: 'quicksand',
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                        Translations.of(context).Seeyourprofile,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Color(0xff8A8A8A),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                            child: Image.asset(
                          "asset/leftarrow.png",
                          scale: 3,
                        )
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _info(1),
              _info(2),
              _asyncLoader,
//              InkWell(
//                onTap: () {
//                  showDialog(
//                    context: context,
//                    builder: (BuildContext context) => DeleteAccount(),
//                  );
//                },
//                child: Container(
//                  margin: EdgeInsets.only(left: 10, right: 10, top: 10),
//                  width: MediaQuery.of(context).size.width,
//                  height: MediaQuery.of(context).size.width * .15,
//                  decoration: BoxDecoration(
//                    borderRadius: BorderRadius.circular(7),
//                    color: Color(0xff59A0CE),
//                  ),
//                  child: Row(
////
//                    children: <Widget>[
//                      Expanded(
//                        flex: 4,
//                        child: Container(
//                          margin: EdgeInsets.only(
//                            left: 30,
//                          ),
//                          child: Container(
//                            alignment: Alignment.centerLeft,
//                            margin: EdgeInsets.only(left: 10),
//                            child: Text(
//                              Translations.of(context).Deleteaccount,
//                              style: TextStyle(
//                                  fontSize: 16,
//                                  fontFamily: 'quicksand',
//                                  color: Colors.white,
//                                  fontWeight: FontWeight.bold),
//                            ),
//                          ),
//                        ),
//                      ),
//                      Expanded(
//                        flex: 1,
////                        child: Image.asset(
////                          "asset/signout.png",
////                          scale: 4,
////                        ),
//                      child: Icon(Icons.delete_forever,color: Colors.white,),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
              InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => Logout(),
                  );
                },
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width * .15,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    color: Color(0xff59A0CE),
                  ),
                  child: Row(
//
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 30,
                          ),
                          child: Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              Translations.of(context).SignOut,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'quicksand',
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Image.asset(
                          "asset/signout.png",
                          scale: 4,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      )),
    );
  }
  //swipe refresh
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo.currentState.reloadState();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }  void _onLoading() async {
    // monitor network fetch
    _asyncLoaderStateGetUserInfo.currentState.reloadState();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  Widget _info(int id) {
    return GestureDetector(
      onTap: () {
        id == 1
            ? Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Myinformation(),
                ))
            : Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Friendrequest(),
                ));
      },
      child: Container(
        margin: EdgeInsets.only(left: 10, right: 10, top: 10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.width * .15,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 3,
              )
            ]),
        child: Row(
//
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Container(
                margin: EdgeInsets.only(
                  left: 30,
                ),
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    id == 1 ? Translations.of(context).MyInformation : Translations.of(context).FriendRequests,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: 'quicksand',
                        color: Color(0xff373737),
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                child: Image.asset(
                  "asset/leftarrow.png",
                  scale: 3,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _noti(int id) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 10),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width * .15,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 3,
            )
          ]),
      child: Row(
//
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Container(
              margin: EdgeInsets.only(
                left: 30,
              ),
              child: Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  id == 1
                      ? Translations.of(context).Notification
                      : id == 2
                          ? Translations.of(context).Showeventsforallusers
                          : id == 3
                              ? "Make my events public"
                              : Translations.of(context).Makemyfriendspublic,
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'quicksand',
                      color: Color(0xff373737),
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 0,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  if (id == 1) {
                    notiisChecked == false
                        ? notiisChecked = true
                        : notiisChecked = false;
                  } else if (id == 2) {
                    showeventisChecked == false
                        ? showeventisChecked = true
                        : showeventisChecked = false;
                  } else if (id == 3) {
                    myeventisChecked == false
                        ? myeventisChecked = true
                        : myeventisChecked = false;
                  } else if (id == 4) {
                    myfriendisChecked == false
                        ? myfriendisChecked = true
                        : myfriendisChecked = false;
                  } else {}

//                    notiisChecked == false
//                        ? notiisChecked = true
//                        : notiisChecked = false;
                });

                _saveswitchedPress();
              },
              child: Container(
                margin: EdgeInsets.only(right: 20),
                child: CustomSwitchButton(
                  buttonHeight: 30,
                  buttonWidth: 50,
                  indicatorWidth: 25,
                  backgroundColor: Color(0xffBCBCBC),
                  unCheckedColor: Colors.white,
                  animationDuration: Duration(milliseconds: 400),
                  checkedColor: Color(0xff59A0CE),
                  checked: id == 1
                      ? notiisChecked
                      : id == 2
                          ? showeventisChecked
                          : id == 3 ? myeventisChecked : myfriendisChecked,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  _saveswitchedPress() async {
    UpdateInfoUserBody body = UpdateInfoUserBody();
    body.notification = notiisChecked==true?"0":"1";
    body.eventsPublic = showeventisChecked==true?"0":"1";
    body.friendsPublic = myfriendisChecked==true?"0":"1";


    /*ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal,
        isDismissible: true,
        showLogs: true);
    await pr.show();*/
    UpdateInfoUserResponse result;
    try {
      result = await sendUpdateInfoUserResponse(body);
      _asyncLoaderStateGetUserInfo.currentState.reloadState();
//      pr.hide();

//      Navigator.pop(context);

/*

      Fluttertoast.showToast(
          msg: "edit information done",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
*/

    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }


  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(
              Translations.of(context).noInternetConnection,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () =>
                  _asyncLoaderStateGetUserInfo.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtData(GetUserInfoResponse data) {
      notiisChecked=data.data.user.notification=="0";
      showeventisChecked=data.data.user.eventsPublic=="0";
      myfriendisChecked=data.data.user.friendsPublic=="0";

    return
      Column(
        children: <Widget>[
          _noti(1),
          _noti(2),
//              _noti(3),
          _noti(4),
        ],
      );
  }

}





