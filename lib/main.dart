import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pin/poc.dart';
import 'package:pin/screens/splach_page.dart';
import 'package:pin/states/app_state.dart';
import 'package:provider/provider.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:io' show Platform;
import 'bloc.dart';
import 'language/translation.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String language = _prefs.getString('language') ?? 'ar';

//  runApp(MyApp(defaultLanguage: language));
//  final FirebaseApp app = await FirebaseApp.configure(
//    name: 'pin app',
//    options: Platform.isIOS
//        ? const FirebaseOptions(
//      googleAppID: '1:645865494964:android:2d17ac46c9dcc8dd183ae1',
//      gcmSenderID: '645865494964',
//      databaseURL: 'https://pin-app-6fcba.firebaseio.com',
//    )
//        : const FirebaseOptions(
//      googleAppID: '1:645865494964:android:2d17ac46c9dcc8dd183ae1',
//      apiKey: 'AIzaSyCB0mvVpMShvU_BG5HO5KoZ-vl8v6U09Mg',
//      databaseURL: 'https://pin-app-6fcba.firebaseio.com',
//    ),
//  );
  return runApp(MultiProvider(providers: [
    ChangeNotifierProvider.value(value: AppState(),)
  ],
      child:new MyApp(defaultLanguage: language)));
}

class MyApp extends StatelessWidget {
  final String defaultLanguage;

  MyApp({
    this.defaultLanguage,
  });

  @override
  Widget build(BuildContext context) {
    AppModel.defaultLanguage = defaultLanguage;
    return ScopedModel<AppModel>(model: AppModel(), child: MyApp2());
  }
}

class AppModel extends Model {
  static String defaultLanguage;

  Locale _appLocale = Locale(defaultLanguage);

  Locale get appLocal => _appLocale ?? Locale(defaultLanguage);

  Future changeDirection(String lang) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    /*if (_appLocale == Locale("ar")) {
      _appLocale = Locale("en");
      _prefs.setString('language', 'en');
    } else {
      _appLocale = Locale("ar");
      _prefs.setString('language', 'ar');
    }*/

    _appLocale = Locale(lang);
    _prefs.setString('language', lang);
    notifyListeners();
  }
}


class MyApp2 extends StatelessWidget {

  DeepLinkBloc _bloc = DeepLinkBloc();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return ScopedModelDescendant<AppModel>(
        builder: (context, child, model) => MaterialApp(
          locale: model.appLocal,
          localizationsDelegates: [
            const TranslationsDelegate(),
            GlobalMaterialLocalizations.delegate,
//            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('ar', ''), // Arabic
            const Locale('en', ''), // English
          ],
          debugShowCheckedModeBanner: false,
          title: "PIN",
//          theme: ThemeData(
//            primarySwatch: Colors.deepPurple,
//          ),
            home: Scaffold(
                body: Provider<DeepLinkBloc>(
                    create: (context) => _bloc,
                    dispose: (context, bloc) => bloc.dispose(),
                    child: PocWidget()))
//          home: new SplashPage(),
        ));
  }
}

/*
class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Start(),
    );
  }
}*/
