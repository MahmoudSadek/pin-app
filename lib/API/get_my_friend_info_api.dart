

import 'package:http/http.dart' as http ;
import 'package:pin/model/response/error_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/response/get_my_friend_nfo_response.dart';
import 'package:pin/utils/common.dart';


Future<GetMyFriendNfoResponse> getGetMyFriendNfoResponse (int id) async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/info/$id',headers: headers);

  if (response.statusCode == 200){
    return GetMyFriendNfoResponse.fromMap(json.decode(response.body));
  } else {

    ErrorResponse res = ErrorResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}
