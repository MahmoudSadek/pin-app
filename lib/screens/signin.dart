import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/utils/common.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../API/user_login_api.dart';
import 'Dashboard.dart';
import 'gmail_signin.dart';
import 'interests.dart';
import '../language/translation_strings.dart';
import '../model/body/user_login_body.dart';
import '../model/response/user_login_response.dart';
import 'video.dart';
import 'package:http/http.dart' as http;

class Signin extends StatefulWidget {
  Signin({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  var facebookLogin = FacebookLogin();

  String facebookName;
  String facebookEmail;
  String facebookImageUrl;
  String facebookPhoneNumber;
  String facebookUserId;

   signUpWithFacebook() async {
    var facebookLoginResult =
    await facebookLogin.logInWithReadPermissions(['email']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        break;
      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.loggedIn:
        var graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult
                .accessToken.token}');

        var profile = json.decode(graphResponse.body);
        print(profile.toString());
         facebookName = profile["name"];
         facebookEmail = profile["email"];
         facebookImageUrl = profile["picture"]['data']['url'];
         facebookPhoneNumber = profile["phone"];
         facebookUserId = profile["id"];
         print(facebookImageUrl);
        break;
    }
  }
  _logout() async {
    await facebookLogin.logOut();
    print("Logged out");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          height: MediaQuery.of(context).size.height * 1,
          width: MediaQuery.of(context).size.width * 1,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("asset/start.png"), fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .5,
                //margin: EdgeInsets.only(left: 80, top:160),
                child: Image.asset(
                  "asset/logo_app.png",
                  scale: 1.5,
                ),
              ),

              SizedBox(
                height: 40,
              ),
//              Container(
//                width: MediaQuery.of(context).size.width,
//                height: MediaQuery.of(context).size.height * .18,
//                // margin: EdgeInsets.only(left:85),
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  children: <Widget>[
//                    Image.asset(
//                      "asset/pin_logo.png",
//                      scale: 5,
//                    ),
//                  ],
//                ),
//              ),
              _button(1),
              SizedBox(
                height: MediaQuery.of(context).size.height * .015,
              ),
              _button(2),
              SizedBox(
                height: MediaQuery.of(context).size.height * .09,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Video()));
                },
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("  "+
                  Translations.of(context).CreatnewAccont+"  ",
                        style:
                            TextStyle(fontSize: 14, color: Colors.black),
                      ),
                      Text(""
                          /*Translations.of(context).SIGNUP*/,
                        style:
                            TextStyle(fontSize: 15, color: Color(0xff086EBA)),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    ));
  }
  Widget _button(int id) {
    return GestureDetector(
      onTap: () {
        id == 1
            ?   signUpWithFacebook().whenComplete(() {
              _signInWithFacebookPress();
//            Navigator.push(
//                context, MaterialPageRoute(builder: (context) => Dashboard()))
        })
            :
        signInWithGoogle().whenComplete(() {
          _signInWithGmailPress();
//            Navigator.push(
//                context, MaterialPageRoute(builder: (context) => Interests()))
        });
        },
      child: Container(
        decoration: id == 1
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Color(0xff054084))
            : id == 2
                ? BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xffFF2E2E),
                    border: Border.all(color: Colors.white))
                : BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.transparent),
        height: MediaQuery.of(context).size.height * .06,
        width: MediaQuery.of(context).size.width * .75,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(id == 1 ? Translations.of(context).SigninwithFacebook : Translations.of(context).SigninwithGoogle,
                style: TextStyle(
                    fontSize: 16, color: Colors.white, fontFamily: 'Sogue')),
          ],
        ),
      ),
    );
  }
  Future _signInWithFacebookPress() async {

    Common.GOOGLE = false;
//    +"l"+"q"
    UserLoginBody body = UserLoginBody();
    body.name = facebookName;
    body.email = facebookEmail;
    body.phone = facebookPhoneNumber;
    body.image = facebookImageUrl;
    body.providerId = facebookUserId;
    body.gcm = await Common.getToken();

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();

    UserLoginResponse result;
    try {
      result = await getUserLoginResponsebyfacebook(body);

//      pr.dismiss();
      if (result.data.user.interestsCount=="0"||result.data.user.interestsCount==null) {
        Fluttertoast.showToast(
            msg: Translations.of(context).Youmustregisterfirst,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
        Navigator.pop(context);
        return;
      }else {
        await storetokengmail(result);
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return Dashboard();
            },
          ),
        );
      }
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => Dashboard()
          ),
          ModalRoute.withName("/Dashboard")
      );
      Fluttertoast.showToast(
          msg:  Translations.of(context).Signinsuccessful,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      pr.hide();
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }

  Future _signInWithGmailPress() async {


    Common.GOOGLE = true;
      UserLoginBody body = UserLoginBody();
      body.name = gmailName;
      body.email = gmailEmail+"1";
      body.phone = gmailPhoneNumber;
      body.image = gmailImageUrl;
      body.providerId = gmailUserId+"1";
      body.gcm = await Common.getToken();

      ProgressDialog pr = new ProgressDialog(context);
      pr = new ProgressDialog(context,
          type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
      await pr.show();

      UserLoginResponse result;
      try {
        result = await getUserLoginResponsebygoogle(body);
        await storetokengmail(result);
//      pr.dismiss();
        if (result.data.user.interestsCount=="0"||result.data.user.interestsCount==null) {
          Fluttertoast.showToast(
              msg: Translations.of(context).Youmustregisterfirst,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.blue,
              textColor: Colors.white,
              fontSize: 16.0);
          Navigator.pop(context);
          return;
        }else {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return Dashboard();
              },
            ),
          );
        }
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => Dashboard()
            ),
            ModalRoute.withName("/Dashboard")
        );
        Fluttertoast.showToast(
            msg: Translations.of(context).Signinsuccessful,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      } catch (e) {
        pr.hide();
        Fluttertoast.showToast(
            msg: e.message,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
        return;
      }
    }
  Future storetokengmail(UserLoginResponse data ) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString(Common.USER_TOKEN, data.data.user.token);
    pref.setString(Common.USER_NAME, data.data.user.name);
    pref.setString(Common.USER_ID, data.data.user.id.toString());
    pref.setString(Common.USER_IMAGE, data.data.user.image);

  }
}
