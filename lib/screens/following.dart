import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../API/get_all_followings_api.dart';
import '../API/send_a_unFollow_request_api.dart';
import '../language/translation_strings.dart';
import '../model/body/send_a_unFollow_request_body.dart';
import '../model/response/all_followings_response.dart';
import '../model/response/send_a_unFollow_request_response.dart';
import 'myprofile.dart';

class Following extends StatefulWidget {
  @override
  _FollowingState createState() => _FollowingState();
}
final GlobalKey<AsyncLoaderState> _getAllFollowing =
new GlobalKey<AsyncLoaderState>();

class _FollowingState extends State<Following> {
  @override
  Widget build(BuildContext context) {

    var _asyncLoadergetAllFollowing = new AsyncLoader(
      key: _getAllFollowing,
      initState: () async => await getAllFollowingsResponse(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtgetuserData(data),
    );
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff59A0CE),
                    blurRadius: 3,
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20))),
            child: Container(
              height: MediaQuery.of(context).size.height * 70,
              padding: EdgeInsets.symmetric(horizontal: 14),
              margin: EdgeInsets.only(top: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .3,
                  ),
                  Text(
                    Translations.of(context).Following,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(60.0)),
      body: _asyncLoadergetAllFollowing

    );
  }

  Widget _friends(FollowingsBean model) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width * .15,
      height: MediaQuery.of(context).size.height * .30,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10),
          // border: Border.all(width: 1, color: Color(0xff086EBA)),
          boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
                color: Color(0xff086EBA),
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 3,
                  )
                ]),
            child: InkWell(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.all(1),
                child: CircleAvatar(
                  backgroundImage:
                  NetworkImage(model.image),
//                  radius: 2,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            model.name,
            style: TextStyle(
                color: Colors.black,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                fontFamily: 'quicksand'),
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: (){
              _sendAUnFollowRequestPress(model.id);
            },
            child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width * .22,
              height: MediaQuery.of(context).size.height * .04,
              decoration: BoxDecoration(
                  color: Color(0xff59A0CE),
                  borderRadius: BorderRadius.circular(4)),
              child: Text(
                Translations.of(context).Unfollow,
                style: TextStyle(
                    fontSize: 12,
                    fontFamily: 'quicksand',
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget getNoConnectionWidget() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.black),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _getAllFollowing.currentState.reloadState())
        ],
      ),
    );
  }

  setWidegtgetuserData(AllFollowingsResponse data) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2.2;
    final double itemWidth = size.width / 2;
    return
      data.data.followings.length<=0?Center(
        child: Container(
            child: Text(
              Translations.of(context).Therearenofollowingyet,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'quicksand'),
            )
        ),
      ):
      Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(right: 10),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: (itemWidth / itemHeight),
          ),
          itemBuilder: (context, index) => _friends(data.data.followings[index]),
          itemCount: data.data.followings.length,
        ),
      );
  }
  Future _sendAUnFollowRequestPress(int id) async {

    SendAUnFollowRequestBody body = SendAUnFollowRequestBody();
    body.followedId = id.toString() ;

    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();
//    new Future.delayed(const Duration(seconds: 1), () => "1");
    SendAUnFollowRequestResponse result;
    try {
      result = await sendSendAUnFollowRequestResponse(body);
//      await storeUser(result);

      _getAllFollowing.currentState.reloadState();

//      Navigator.pop(context);

      Fluttertoast.showToast(
          msg: Translations.of(context).Requestsent,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
}
