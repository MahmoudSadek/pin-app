class ChatThreadsResponse {
  bool status;
  String msg;
  Data data;

  ChatThreadsResponse({this.status, this.msg, this.data});

  ChatThreadsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }

    return data;
  }
}

class Data {
  List<Threads> threads;

  Data({this.threads});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['threads'] != null) {
      threads = new List<Threads>();
      json['threads'].forEach((v) {
        threads.add(new Threads.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.threads != null) {
      data['threads'] = this.threads.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Threads {
  int conversationId;
  WithUser withUser;
  LastMessage lastMessage;

  Threads({this.conversationId, this.withUser, this.lastMessage});

  Threads.fromJson(Map<String, dynamic> json) {
    conversationId = json['conversationId'];
    withUser = json['withUser'] != null
        ? new WithUser.fromJson(json['withUser'])
        : null;
    lastMessage = json['lastMessage'] != null
        ? new LastMessage.fromJson(json['lastMessage'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['conversationId'] = this.conversationId;
    if (this.withUser != null) {
      data['withUser'] = this.withUser.toJson();
    }
    if (this.lastMessage != null) {
      data['lastMessage'] = this.lastMessage.toJson();
    }
    return data;
  }
}

class WithUser {
  int id;
  String name;
  String email;
  Null phone;
  Null gender;
  String image;
  String cover;
  String gcm;
  Null deletedAt;
  String createdAt;
  String updatedAt;

  WithUser(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.gender,
        this.image,
        this.cover,
        this.gcm,
        this.deletedAt,
        this.createdAt,
        this.updatedAt});

  WithUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    gender = json['gender'];
    image = json['image'];
    cover = json['cover'];
    gcm = json['gcm'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['image'] = this.image;
    data['cover'] = this.cover;
    data['gcm'] = this.gcm;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class LastMessage {
  int id;
  String conversationId;
  String senderId;
  String message;
  String isSeen;
  String deletedFromSender;
  String deletedFromReceiver;
  String createdAt;
  String updatedAt;

  LastMessage(
      {this.id,
        this.conversationId,
        this.senderId,
        this.message,
        this.isSeen,
        this.deletedFromSender,
        this.deletedFromReceiver,
        this.createdAt,
        this.updatedAt});

  LastMessage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    conversationId = json['conversation_id'];
    senderId = json['sender_id'];
    message = json['message'];
    isSeen = json['is_seen'];
    deletedFromSender = json['deleted_from_sender'];
    deletedFromReceiver = json['deleted_from_receiver'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['conversation_id'] = this.conversationId;
    data['sender_id'] = this.senderId;
    data['message'] = this.message;
    data['is_seen'] = this.isSeen;
    data['deleted_from_sender'] = this.deletedFromSender;
    data['deleted_from_receiver'] = this.deletedFromReceiver;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
