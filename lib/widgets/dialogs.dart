import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/API/user_suggest_interests_api.dart';
import 'package:pin/dialogboxs/suggest_int_dialog.dart';
import 'package:pin/language/translation_strings.dart';
import 'package:pin/model/body/user_suggest_interest_body.dart';
import 'package:pin/model/response/events_response.dart';
import 'package:pin/model/response/user_suggest_iInterest_response.dart';
import 'package:progress_dialog/progress_dialog.dart';

class SuggestInterestDialog extends StatefulWidget {
  @override
  _SuggestInterestDialogState createState() => _SuggestInterestDialogState();
}

class _SuggestInterestDialogState extends State<SuggestInterestDialog> {
  TextEditingController suggestionController = TextEditingController();
  TextEditingController reasonController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Container(
              padding:
                  EdgeInsets.all(MediaQuery.of(context).size.height * 0.02),
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(
                    MediaQuery.of(context).size.height * 0.02),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Center(
                      child: _heading(Translations.of(context).SuggestInterest, 1),
                    ),
                    _heading(Translations.of(context).Suggestion, 2),
                    _textfield(suggestionController),
                    _heading(Translations.of(context).Reason, 2),
                    _textfield(reasonController),
                    Center(
                      // margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                      child: GestureDetector(
                        onTap: () {
                          _sendSuggestInterestsPress();

                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.026,
                              bottom:
                                  MediaQuery.of(context).size.height * 0.016),
                          height: MediaQuery.of(context).size.height * .056,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Color(0xffFDAF17),
                              borderRadius: BorderRadius.circular(5)),
                          child: Stack(
                            children: <Widget>[
                              Center(
                                child: Text(
                              Translations.of(context).Submit,
                                  style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.height *
                                              0.028,
                                      color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ])),
        ),
      ),
    );
  }
  Future _sendSuggestInterestsPress() async {

    if (suggestionController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: "you Msut enter any Suggest interests",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    if (reasonController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: "you Msut enter reason",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }

    UserSuggestInterestBody body = UserSuggestInterestBody();
    body.name = suggestionController.text;
    body.reason = reasonController.text;


//    ProgressDialog pr = new ProgressDialog(context);
//    pr = new ProgressDialog(context,
//        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
//    await pr.show();

    UserSuggestIInterestResponse result;
    try {
      result = await sendUserSuggestIInterestResponse(body);
//      await storeUser(result);
//      pr.dismiss();
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (BuildContext context) =>
              Suggest_intr_dialog());


//      Fluttertoast.showToast(
//          msg: "interests ok",
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.CENTER,
//          backgroundColor: Colors.blue,
//          textColor: Colors.white,
//          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }
  Widget _textfield(TextEditingController controller) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 20),
      // margin: EdgeInsets.only(left: 10, right: 10, top: 5),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .07,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7),
          boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)],
          color: Colors.white),
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.multiline,
        decoration: InputDecoration(
          border: InputBorder.none,
        ),
      ),
    );
  }

  _heading(String text, int id) {
    return Container(
      margin: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.014,
          bottom:
              MediaQuery.of(context).size.height * (id == 1 ? 0.01 : 0.006)),
      child: Text(
        text,
        style: TextStyle(
            fontSize: id == 1
                ? MediaQuery.of(context).size.height * 0.021
                : MediaQuery.of(context).size.height * 0.016,
            color: id == 1 ? Colors.black : Color(0xff858282),
            fontWeight: id == 1 ? FontWeight.bold : FontWeight.normal),
      ),
    );
  }
}

class showImageDialog extends StatefulWidget {
  Files file;
  showImageDialog(this. file);

  @override
  _showImageDialogState createState() => _showImageDialogState(file);
}

class _showImageDialogState extends State<showImageDialog> {
  Files file;
  _showImageDialogState(this.file);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: AspectRatio(
            aspectRatio: 1,
            child: GestureDetector(
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(file.name),
                        fit: BoxFit.fill,
                      ),
                      borderRadius:
                      BorderRadius.circular(
                          10)),
                  child: file
                      .extension ==
                      "mp4"
                      ? Image.asset(
                      "asset/backimage.png")
                      : Container(),
                )),
          ),
        ),
      ),
    );
  }
}
