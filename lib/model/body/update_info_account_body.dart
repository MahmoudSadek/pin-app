

class UpdateInfoAccountBody {
  String aboutMe;
  String activities;
  String hobbies;
  String education;
  String work;

  static UpdateInfoAccountBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpdateInfoAccountBody updateInfoUserBodyBean = UpdateInfoAccountBody();
    updateInfoUserBodyBean.aboutMe = map['about_me'];
    updateInfoUserBodyBean.activities = map['activities'];
    updateInfoUserBodyBean.hobbies = map['hobbies'];
    updateInfoUserBodyBean.education = map['education'];
    updateInfoUserBodyBean.work = map['work'];
    return updateInfoUserBodyBean;
  }

  Map toJson() => {
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work
  };
}