

class UserSuggestInterestBody {
  String name;
  String reason;

  static UserSuggestInterestBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserSuggestInterestBody userSuggestInterestBodyBean = UserSuggestInterestBody();
    userSuggestInterestBodyBean.name = map['name'];
    userSuggestInterestBodyBean.reason = map['reason'];
    return userSuggestInterestBodyBean;
  }

  Map toJson() => {
    "name": name,
    "reason": reason,
  };
}