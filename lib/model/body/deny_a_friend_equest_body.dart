
class DenyAFriendEquestBody {
  int friendId;

  static DenyAFriendEquestBody fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DenyAFriendEquestBody denyAFriendEquestBodyBean = DenyAFriendEquestBody();
    denyAFriendEquestBodyBean.friendId = map['friend_id'];
    return denyAFriendEquestBodyBean;
  }

  Map toJson() => {
    "friend_id": friendId,
  };
}