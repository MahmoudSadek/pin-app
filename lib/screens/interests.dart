import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin/screens/interestlocation.dart';
import 'package:pin/widgets/dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../API/all_interests_api.dart';
import '../API/set_user_interests_api.dart';
import '../language/translation_strings.dart';
import '../model/body/set_user_interests_body.dart';
import '../model/response/all_interests_response.dart';
import '../model/response/set_user_interests_response.dart';


class Interests extends StatefulWidget {
  Interests({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _InterestsState createState() => _InterestsState();
}


class _InterestsState extends State<Interests> {
  final GlobalKey<AsyncLoaderState> _asyncLoaderStateGetUserInterests =
  new GlobalKey<AsyncLoaderState>();

  List <Interest> selected = [];
  @override
  Widget build(BuildContext context) {
    var _asyncLoader = new AsyncLoader(
      key: _asyncLoaderStateGetUserInterests,
      initState: () async => await getAllInterestsResponse(),
      renderLoad: () =>  Center(child: CircularProgressIndicator()),
      renderError: ([error]) => getNoConnectionWidget(),
      renderSuccess: ({data}) => setWidegtData(data),
    );
    return Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59A0CE),
                      blurRadius: 3,
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20))),
              child: Container(
                height: MediaQuery.of(context).size.height * 70,
                padding: EdgeInsets.symmetric(horizontal: 14),
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width * .3,
                    ),
                    Text(
                      Translations.of(context).MyInterests,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'trebuchat'),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(60.0)),
        bottomNavigationBar: Container(
          margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
          child: Container(
            height: MediaQuery.of(context).size.width * .2,
            width: MediaQuery.of(context).size.height * .8,
            alignment: Alignment.center,
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        _sendInterestsPress();},
                      child: Container(
                        height: MediaQuery.of(context).size.width * .12,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Color(0xffFDAF17),
                            borderRadius: BorderRadius.circular(5)),
                        child: Stack(
                          children: <Widget>[
                            Center(
                              child: Text(
                                Translations.of(context).Next,
                                style: TextStyle(
                                    fontSize: 24, color: Colors.white),
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                  ),
                                  margin: EdgeInsets.only(right: 10),
                                  child: Icon(
                                    Icons.navigate_next,
                                    color: Color(0xffFDAF17),
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                    top: 50,
                    left: MediaQuery.of(context).size.width * .56,
                    child: GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) => SuggestInterestDialog());
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              Translations.of(context).SuggestInterests,
                              style: TextStyle(
                                  fontSize: 14, color: Color(0xff949494)),
                            ),
                          ),
                          SizedBox(width: 30,),
                          Icon(
                            Icons.navigate_next,
                            color: Color(0xff086EBA),
                          )
                        ],
                      ),
                    ))
              ],
            ),
          ),
        ),
        body:
        _asyncLoader
      /*ListView(
            children: [
              Container(
                margin: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * .055,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height * .02,
                        ),
                        child: Text(
                          Translations.of(context).Pleasechooseyourinterestsbelow,
                          style: TextStyle(fontSize: 16, color: Color(0xff858282)),
                        )),



                    Column(
                      children: <Widget>[
                        _asyncLoader
                      ],
                    )
    ],
    ),
    ),
    ])*/
    );
  }

  Widget _card(Interest model) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected.contains(model) ? selected.remove(model) : selected.add(model);
        });
      },
      child: Container(
        margin: EdgeInsets.only(right: 5, bottom: 5,top: 5,left: 5),
        height: MediaQuery.of(context).size.width * .52,
        width: MediaQuery.of(context).size.width * .43,
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: new DecorationImage(image: new NetworkImage(model.image),
                fit: BoxFit.cover)
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.2),
            borderRadius: BorderRadius.circular(10),
          ),
          padding: EdgeInsets.only(bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                model.title,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: selected.contains(model) ? Colors.blue : Colors.grey),
                child: Icon(
                  Icons.done,
                  size: 18,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  setWidegtData (AllInterestsResponse data){
    return
      GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1.8,
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
          //childAspectRatio: MediaQuery.of(context).size.height / 4,
        ),
        itemBuilder: (context, index) => _card(data.data.interests[index]),
        itemCount: data.data.interests.length,
      );

  }

  Widget getNoConnectionWidget() {
    return Container(
//      color: Colors.black54,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 90.0,
            child: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/wifi.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text(Translations.of(context).noInternetConnection,style: TextStyle(color: Colors.white),),
          ),
          new FlatButton(
              color: Colors.red,
              child: new Text(
                Translations.of(context).retry,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _asyncLoaderStateGetUserInterests.currentState.reloadState())
        ],
      ),
    );
  }

  Future _sendInterestsPress() async {

    if (selected.isEmpty) {
      Fluttertoast.showToast(
          msg: Translations.of(context).youMsutChooseanyfrominterests,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }

    SetUserInterestsBody body = SetUserInterestsBody();
    body.interests = [];
    for(int i=0 ; i<selected.length;i++){
      body.interests.add( selected[i].id);
    }


    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    await pr.show();

    SetUserInterestsResponse result;
    try {
      result = await sendSetUserInterestsResponse(body);
//      await storeUser(result);
//      pr.dismiss();
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
            return Interestloc();
          },
        ),
      );


      Fluttertoast.showToast(
          msg: Translations.of(context).Interestsidentified,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
  }

}

