
class SetUserInterestsResponse {
  bool status;
  String msg;
  List<dynamic> data;
  List<dynamic> errors;

  static SetUserInterestsResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SetUserInterestsResponse setUserInterestsResponseBean = SetUserInterestsResponse();
    setUserInterestsResponseBean.status = map['status'];
    setUserInterestsResponseBean.msg = map['msg'];
    setUserInterestsResponseBean.data = map['data'];
    setUserInterestsResponseBean.errors = map['errors'];
    return setUserInterestsResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}