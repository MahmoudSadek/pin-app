import 'dart:async';
import 'dart:convert' show json, jsonDecode, utf8;

import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;
import 'package:pin/model/body/add_pin_body.dart';
import 'package:pin/model/body/update_pin_body.dart';
import 'package:pin/model/response/add_pin_response.dart';
import 'package:pin/model/response/error_response.dart';
import 'package:pin/model/response/feelings_response.dart';
import 'package:pin/model/response/friends_response.dart';
import 'package:pin/model/response/interests_response.dart';
import 'package:pin/utils/common.dart';

Future<FeelingsResponse> getFeelingsResponse() async {
  var headers = await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(
    Common.MAIN_API_URL + "api/data/feelings",
    headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return FeelingsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<InterestsResponse> getInterestsResponse({String type}) async {
  var headers = await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(
    Common.MAIN_API_URL + "api/${type == null ? "user" : "data"}/interests",
    headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return InterestsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<FriendsResponse> getFriendsResponse() async {
  var headers = await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(
    Common.MAIN_API_URL + "api/user/friends/getFriends",
    headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return FriendsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<AddPinResponse> sendAddPinRequest(AddPinBody body) async {
  try {
    var token = await Common.getTokengmail();
    String address_name = "";
    final coordinates =
        new Coordinates(double.parse(body.lat), double.parse(body.lon));
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print("${first.featureName} : ${first.addressLine}");

    address_name = first.adminArea + "\n" + first.countryName;

    var request = http.MultipartRequest(
        'POST', Uri.parse(Common.MAIN_API_URL + "api/events"));
    request.fields['name'] = body.name;
    request.fields['feeling_id'] = body.feelingId.toString();
    request.fields['description'] = body.description;
    request.fields['start_date'] = body.startDate;
    request.fields['end_date'] = body.endDate;
    request.fields['start_time'] = body.startTime;
    request.fields['end_time'] = body.endTime;
    request.fields['lat'] = body.lat;
    request.fields['lon'] = body.lon;
    request.fields['type'] = body.type;
    request.fields['address_name'] = address_name;
    for (int i = 0; i < body.categories.length; i++) {
      request.fields['categories[$i]'] = body.categories[i];
    }
    for (int i = 0; i < body.friends.length; i++) {
      request.fields['friends[$i]'] = body.friends[i];
    }
    for (int i = 0; i < body.images.length; i++) {
      request.files.add(
          await http.MultipartFile.fromPath('files[]', body.images[i].path));
    }
    for (int i = 0; i < body.videos.length; i++) {
      request.files.add(
          await http.MultipartFile.fromPath('files[]', body.videos[i].path));
    }
    request.headers['authorization'] = 'Bearer $token';
    var response = await request.send();
    Stream<String> resp = response.stream.transform(utf8.decoder);
    response.stream.transform(utf8.decoder).listen((value) {
      if (response.statusCode == 200) {
        return AddPinResponse.fromJson(json.decode(value));
      } else {
        ErrorResponse res = ErrorResponse.fromJson(json.decode(value));
        throw Exception(res.msg);
      }
    });
  } catch (e) {
    throw Exception("Error in Request");
  }
}

Future<AddPinResponse> sendUpdatePinRequest(UpdatePinBody body, int id) async {
  var token = await Common.getTokengmail();
  String address_name = "";
  final coordinates =
  new Coordinates(double.parse(body.lat), double.parse(body.lon));
  var addresses =
  await Geocoder.local.findAddressesFromCoordinates(coordinates);
  var first = addresses.first;
  print("${first.featureName} : ${first.addressLine}");

  address_name = first.adminArea + "\n" + first.countryName;
  var request = http.MultipartRequest(
      'POST', Uri.parse(Common.MAIN_API_URL + "api/events/$id"));
  request.fields['_method'] = body.method;
  request.fields['name'] = body.name;
  request.fields['feeling_id'] = body.feelingId.toString();
  request.fields['description'] = body.description;
  request.fields['start_date'] = body.startDate;
  request.fields['end_date'] = body.endDate;
  request.fields['start_time'] = body.startTime;
  request.fields['end_time'] = body.endTime;
  request.fields['lat'] = body.lat;
  request.fields['lon'] = body.lon;
  request.fields['type'] = body.type;
  request.fields['address_name'] = address_name;
  for (int i = 0; i < body.categories.length; i++) {
    request.fields['categories[$i]'] = body.categories[i];
  }
  for (int i = 0; i < body.friends.length; i++) {
    request.fields['friends[$i]'] = body.friends[i];
  }
  for (int i = 0; i < body.images.length; i++) {
    request.files.add(
        await http.MultipartFile.fromPath('files[]', body.images[i].path));
  }
  for (int i = 0; i < body.videos.length; i++) {
    request.files.add(
        await http.MultipartFile.fromPath('files[]', body.videos[i].path));
  }
  request.headers['authorization'] = 'Bearer $token';
  var response = await request.send();
  Stream<String> resp = response.stream.transform(utf8.decoder);
  response.stream.transform(utf8.decoder).listen((value) {
    if (response.statusCode == 200) {
      return AddPinResponse.fromJson(json.decode(value));
    } else {
      ErrorResponse res = ErrorResponse.fromJson(json.decode(value));
      throw Exception(res.msg);
    }
  });
}
