

class UserAddSocialResponse {
  bool status;
  String msg;
  DataBean data;
  List<dynamic> errors;

  static UserAddSocialResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserAddSocialResponse userAddSocialResponseBean = UserAddSocialResponse();
    userAddSocialResponseBean.status = map['status'];
    userAddSocialResponseBean.msg = map['msg'];
    userAddSocialResponseBean.data = DataBean.fromMap(map['data']);
    userAddSocialResponseBean.errors = map['errors'];
    return userAddSocialResponseBean;
  }

  Map toJson() => {
    "status": status,
    "msg": msg,
    "data": data,
    "errors": errors,
  };
}


class DataBean {
  UserBean user;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.user = UserBean.fromMap(map['user']);
    return dataBean;
  }

  Map toJson() => {
    "user": user,
  };
}


class UserBean {
  int id;
  String name;
  String email;
  String phone;
  dynamic gender;
  String image;
  String cover;
  dynamic aboutMe;
  dynamic activities;
  dynamic hobbies;
  dynamic education;
  dynamic work;
  String notification;
  String eventsPublic;
  String friendsPublic;
  String gcm;
  dynamic deletedAt;
  String createdAt;
  String updatedAt;

  static UserBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserBean userBean = UserBean();
    userBean.id = map['id'];
    userBean.name = map['name'];
    userBean.email = map['email'];
    userBean.phone = map['phone'];
    userBean.gender = map['gender'];
    userBean.image = map['image'];
    userBean.cover = map['cover'];
    userBean.aboutMe = map['about_me'];
    userBean.activities = map['activities'];
    userBean.hobbies = map['hobbies'];
    userBean.education = map['education'];
    userBean.work = map['work'];
    userBean.notification = map['notification'];
    userBean.eventsPublic = map['events_public'];
    userBean.friendsPublic = map['friends_public'];
    userBean.gcm = map['gcm'];
    userBean.deletedAt = map['deleted_at'];
    userBean.createdAt = map['created_at'];
    userBean.updatedAt = map['updated_at'];
    return userBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "gender": gender,
    "image": image,
    "cover": cover,
    "about_me": aboutMe,
    "activities": activities,
    "hobbies": hobbies,
    "education": education,
    "work": work,
    "notification": notification,
    "events_public": eventsPublic,
    "friends_public": friendsPublic,
    "gcm": gcm,
    "deleted_at": deletedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}