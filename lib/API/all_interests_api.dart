

import 'package:http/http.dart' as http ;
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/response/all_interests_response.dart';
import 'package:pin/utils/common.dart';






Future<AllInterestsResponse> getAllInterestsResponse () async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/data/interests',headers: headers);

  if (response.statusCode == 200){
    return AllInterestsResponse.fromJson(json.decode(response.body));
  } else {

    AllInterestsResponse res = AllInterestsResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}




