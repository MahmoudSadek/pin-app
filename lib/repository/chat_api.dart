import 'package:http/http.dart' as http ;
import 'package:pin/model/body/add_comment_body.dart';
import 'package:pin/model/body/add_pin_body.dart';
import 'package:pin/model/body/message_body.dart';
import 'package:pin/model/body/report_event_body.dart';
import 'package:pin/model/body/update_pin_body.dart';
import 'package:pin/model/response/add_pin_response.dart';
import 'package:pin/model/response/chat_thread_response.dart';
import 'package:pin/model/response/error_response.dart';
import 'package:pin/model/response/event_details_response.dart';
import 'package:pin/model/response/events_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/model/response/feelings_response.dart';
import 'package:pin/model/response/friends_response.dart';
import 'package:pin/model/response/interests_response.dart';
import 'package:pin/model/response/messages_response.dart';
import 'package:pin/model/response/send_message_response.dart';
import 'package:pin/utils/common.dart';

Future<ChatThreadsResponse> getChatThreadsResponse(String pagination, String type  ) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(Common.MAIN_API_URL+"api/chat/threads/$type",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return ChatThreadsResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<MessagesResponse> getMessagesResponse(String pagination , int id ) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(aboutAppBody.toJson());
  var response = await http.get(Common.MAIN_API_URL+"api/chat/more/messages/$id?take=$pagination",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return MessagesResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

Future<SendMessageResponse> sendChatResponse(MessageBody messageBody) async {
  var headers= await Common.getHeaders();

  var body = json.encode(messageBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+"api/chat/send/${messageBody.chatId}",
      headers: headers,
      body: body
  );

  if (response.statusCode == 200) {
    return SendMessageResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}


Future<ErrorResponse> sendSeenChatResponse(String userId) async {
  var headers= await Common.getHeaders();

//  var body = json.encode(messageBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+"api/chat/make-seen/$userId",
      headers: headers,
//      body: body
  );

  if (response.statusCode == 200) {
    return ErrorResponse.fromJson(json.decode(response.body));
  } else {
    throw Exception('Unable to fetch Home FeelingsReponse from the REST API');
  }
}

