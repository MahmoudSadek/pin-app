

import 'dart:convert';

UpdateImageResponse updateImageResponseFromJson(String str) => UpdateImageResponse.fromJson(json.decode(str));

String updateImageResponseToJson(UpdateImageResponse data) => json.encode(data.toJson());

class UpdateImageResponse {
  bool status;
  String msg;
  Data data;
  List<dynamic> errors;

  UpdateImageResponse({
    this.status,
    this.msg,
    this.data,
    this.errors,
  });

  factory UpdateImageResponse.fromJson(Map<String, dynamic> json) => UpdateImageResponse(
    status: json["status"],
    msg: json["msg"],
    data: Data.fromJson(json["data"]),
    errors: List<dynamic>.from(json["errors"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "msg": msg,
    "data": data.toJson(),
    "errors": List<dynamic>.from(errors.map((x) => x)),
  };
}

class Data {
  bool updated;

  Data({
    this.updated,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    updated: json["updated"],
  );

  Map<String, dynamic> toJson() => {
    "updated": updated,
  };
}
