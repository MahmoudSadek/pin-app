

import 'package:http/http.dart' as http ;
import 'package:pin/model/response/pending_friend_requests_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';




Future<PendingFriendRequestsResponse> getPendingFriendRequestsResponse() async {

  var headers= await Common.getHeaders();

  var response = await http.get(Common.MAIN_API_URL+'api/user/friends/getFriendRequests',headers: headers);

  if (response.statusCode == 200){
    return PendingFriendRequestsResponse.fromMap(json.decode(response.body));
  } else {

    PendingFriendRequestsResponse res = PendingFriendRequestsResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}




