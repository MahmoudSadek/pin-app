

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/deny_a_friend_equest_body.dart';
import 'package:pin/model/response/deny_a_friend_request_response.dart';
import 'package:pin/model/response/error_response.dart';
import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';






Future<DenyAFriendRequestResponse> sendDenyAFriendRequestResponse (DenyAFriendEquestBody denyAFriendEquestBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(denyAFriendEquestBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/friend/denyFriendRequest',body: body,headers: headers);

  if (response.statusCode == 200){
    return DenyAFriendRequestResponse.fromMap(json.decode(response.body));
  } else {

    ErrorResponse res = ErrorResponse.fromJson(json.decode(response.body));
    throw Exception(res.msg);
  }
}




Future<DenyAFriendRequestResponse> sendAcceptaFriendRequest (DenyAFriendEquestBody denyAFriendEquestBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(denyAFriendEquestBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/friend/acceptFriendRequest',body: body,headers: headers);

  if (response.statusCode == 200){
    return DenyAFriendRequestResponse.fromMap(json.decode(response.body));
  } else {

    DenyAFriendRequestResponse res = DenyAFriendRequestResponse.fromMap(json.decode(response.body));
    throw Exception(res.msg);
  }
}





Future<DenyAFriendRequestResponse> sendRemoveFriend (DenyAFriendEquestBody denyAFriendEquestBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(denyAFriendEquestBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/friend/unfriend',body: body,headers: headers);

  if (response.statusCode == 200){
    return DenyAFriendRequestResponse.fromMap(json.decode(response.body));
  } else {

    DenyAFriendRequestResponse res = DenyAFriendRequestResponse.fromMap(json.decode(response.body));
    throw Exception(res.msg);
  }
}

