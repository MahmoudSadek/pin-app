

import 'package:http/http.dart' as http ;
import 'package:pin/model/body/user_suggest_interest_body.dart';
import 'package:pin/model/response/user_suggest_iInterest_response.dart';

import 'dart:convert'show json, jsonDecode;
import 'dart:async';

import 'package:pin/utils/common.dart';




Future<UserSuggestIInterestResponse> sendUserSuggestIInterestResponse (UserSuggestInterestBody userSuggestInterestBody, ) async {

  var headers= await Common.getHeaders();

  var body = json.encode(userSuggestInterestBody.toJson());
  var response = await http.post(Common.MAIN_API_URL+'api/user/suggest/interests',body: body,headers: headers);

  if (response.statusCode == 200){
    return UserSuggestIInterestResponse.fromMap(json.decode(response.body));
  } else {

    UserSuggestIInterestResponse res = UserSuggestIInterestResponse.fromMap(json.decode(response.body));
    throw Exception(res.errors);
  }
}




