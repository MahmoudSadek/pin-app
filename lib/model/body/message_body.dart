class MessageBody {
  int chatId;
  String message;

  MessageBody({this.chatId, this.message});

  MessageBody.fromJson(Map<String, dynamic> json) {
//    withId = json['withId'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chatId'] = this.chatId;
    data['message'] = this.message;
    return data;
  }
}
